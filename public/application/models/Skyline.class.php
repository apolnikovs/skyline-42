<?php
/**

 * @package skyline
 * @todo 
 * <ul>
 *      <li>update get fn by collecting data from db</li>
 * </ul>
 * 19Apr2012
 * -some of the data from the db is not dependant of Brand whcih need to implemented when the user is not network user
 * 
 * $data = new DataObject($this->conn);
 * $result = $data->collect('manufacturer', "Status = 'Active'", 'ManufacturerID, ManufacturerName'); 
 * 
 * @version 1.02
 * 
 * Changes
 * Date        Version Author                Reason
 * 09/13/2013  1.00    Brian Etherington     Added reference to Constants.class.php
 * 06/03/2013  1.01    Brian Etherington     Replaced space with non breaking space in 
 *                                           post code in Full address in getServiceCentre Details
 *                                           to prevent postcode wrapping on Swann job booking response page.
 * 09/05/2013  1.02    Brian Etherington     Added DISTINCT to getServiceTypes selection because Network User's
 *                                           returning dozen's of duplicates.
 */

require_once('CustomModel.class.php');
require_once('DataObject.class.php');
require_once('Timeline.class.php');
//require_once('Brands.class.php');
require_once('Constants.class.php');
require_once('Functions.class.php');

/*
 * 
 */
class Skyline extends CustomModel {  
    /*
     * @var PDO     $conn
     * @var array   $general_default to be stored as an array of skyline settings
     * @var string  $where_clause
     */       
    private $conn, $defaultBrandID, 
            $general_default,
            $SkylineBrandID = 1000,
            $where_clause;
    private $table_network_client = "network_client";
    private $table_network_service_provider = "network_service_provider";
    private $table_client_branch = "client_branch";
    private $table_client = "client";
    
    public $debug = false;
    
    public function __construct($Controller) {
                  
        parent::__construct($Controller); 
        
        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );
        
        #$this->controller->log('sesssion userID'.$_SESSION['UserID']);
        
        #var_dump( $this->controller->user );
        
        /*
         * detech the BrandID via the current loaded in User
         * note: 19Apr2012 UserID generated from login is char type future UserID is int data type
         */
        
        #$this->controller->log(var_export($this->controller->user, true) . get_class( $this->controller->user ) . $_SESSION['UserID']);
        
        if( isset($this->controller->user) &&  get_class($this->controller->user) === 'stdClass' ){
            $userID = 1;
            $user_info = $this->collectByID('user', array('UserID'=> $userID));
            
            
            $this->defaultBrandID = $user_info['DefaultBrandID'];
            $this->general_default = $this->collectByID('general_default', array('BrandID' => $this->defaultBrandID ));
            
            #$this->controller->log('Skyline->construct :'.var_export($user_info, true));

            
        }else{
            $this->general_default = $this->collectByID('general_default', array('GeneralDefaultID' => 1));
            
        }    
        
    }
    
    public function __call($method, $args){
        $this->controller->log("$method method doesn't exist in Skyline Model");
    }
    
    /*
     * Autoload is a magic method which detects if a Class is missing and called
     * 
     * @author Simon Tsang <s.tsang@pccsuk.com>
     * 
     * note: currently not working
     */
    public function __autoload( $classname ){
        $filename = "{$classname}.class.php";
        if( is_file( $filename ) )
            include_once( $filename );
        else
            $this->controller->log( "Missing $filename file");
    }
    
    public function maxLoginAttempts() {
        return isset($this->general_default['NoPasswordAttempts']) ? $this->general_default['NoPasswordAttempts'] : 3;
    }
    /*
     * 
     */
//    public function getAccessRights($userType){
//        $this->collect('permission', 'WHERE');
//    }
    
    public function getAdvancedJobSearchDropdowns(){
        return array( 'ManufacturerList' => getManufacturer('alpha') );
    }
    
    /**
     * 
     * getAppointments fn
     * @param array $JobID
     * @return array
     *
     * @author Simon Tsang s.tsang@pccsuk.com	 
     ********************************************/
    public function getAppointments( $JobID ){
        
        //$this->debug = true; 
        
        
        $args = $_POST;       
        $args['where'] = "JobID = $JobID";
        $args['DISTINCT'] = true; // KLUGE alert!!!! this is to remove invalid duplicate rows in the select.
                                  // In case you are wondering, there is no attempt to resolve the cause of 
                                  // the duplicate rows in the RMA Tracker API.
       
         //TIME_FORMAT(AppointmentTime, '%h:%i %p')
        $appointments = $this->ServeDataTables(
                $this->conn, 
                'appointment AS T1 
                    LEFT JOIN service_provider_engineer AS T2 ON T1.ServiceProviderEngineerID=T2.ServiceProviderEngineerID',
                array("T1.AppointmentID", array('T1.AppointmentDate', "DATE_FORMAT(T1.AppointmentDate, '%d/%m/%Y')"),
                    array('T1.AppointmentTime', "UCASE(T1.AppointmentTime) AS AppointmentTime"), 'T1.AppointmentType', 'T1.Notes', 
                    "CONCAT_WS(' ',UCASE(T2.EngineerFirstName), UCASE(T2.EngineerLastName)) As EngineerName", 'T1.Cancelled'), 
                $args);    

        return $appointments;
        
    }
    
    public function getAccessRight($userType){
        
        $this->controller->log('test');
    }

    /**
     * getBranch
     * @todo update template
     * @return array 
     */    
 /*   public function getBranch() {
        #$result = $this->collect('branch AS t1 LEFT JOIN brand_branch AS t2 ON t1.BranchID = t2.BranchID', null, 't1.BranchID, t1.BranchName');
        #$this->controller->log('Skyline->getBranch : ' . var_export($branches, true));           
        
        $result = array(
            array( 'code' => 'N', 'branch' => 'Northampton' ),
            array( 'code' => 'B', 'branch' => 'Birmingham' ),
            array( 'code' => 'K', 'branch' => 'Kettering' ),
            array( 'code' => 'C', 'branch' => 'Coventry' )
        );
        
        return $this->collect('branch', null, 'BranchID, BranchName');
    }*/
    
   
    public function getBranches($BrandID = null, $CompanyFlag=null, $ClientID=null, $NetworkID=null){
        
        $result = array();
        if($BrandID)
        {
            
            //return $this->collect('branch AS T1 LEFT JOIN brand_branch ON T1.BranchID=T2.BranchID', "T2.BrandID='".$BrandID."'", 'T1.BranchID, T1.BranchName');
            
            $sql = "SELECT T1.BranchID, T1.BranchName FROM branch AS T1 LEFT JOIN brand_branch AS T2 ON T1.BranchID=T2.BranchID WHERE T1.Status='Active' AND T2.Status='Active' AND T2.BrandID=:BrandID ORDER BY T1.BranchName" ;
              
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':BrandID' => $BrandID));
           
            while($row = $fetchQuery->fetch()){
                $result[] = array("BranchID"=>$row['BranchID'], "BranchName"=>$row['BranchName']);
            }
        }
        else if ($CompanyFlag)
        {    
            //return $this->collect('branch', $where, 'BranchID, BranchName');
            
            $sql = "SELECT BranchID AS ID, BranchName AS CompanyName FROM branch WHERE Status='Active' ORDER BY CompanyName" ;
              
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute();
            
            while($row = $fetchQuery->fetch()){
                $result[] = array("ID"=>$row['ID'], "CompanyName"=>$row['CompanyName']);
            }
        }
        else if($ClientID && $NetworkID)
        {
            
            $sql = "SELECT T1.BranchID, T1.BranchName FROM branch AS T1 LEFT JOIN client_branch AS T2 ON T1.BranchID=T2.BranchID WHERE T1.Status='Active' AND T2.Status='Active' AND T2.ClientID=:ClientID AND T2.NetworkID=:NetworkID ORDER BY T1.BranchName" ;
              
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':NetworkID' => $NetworkID, ':ClientID' => $ClientID));
           
            while($row = $fetchQuery->fetch()){
                $result[] = array("BranchID"=>$row['BranchID'], "BranchName"=>$row['BranchName']);
            }
        }
        else
        {    
            //return $this->collect('branch', $where, 'BranchID, BranchName');
            
            $sql = "SELECT BranchID, BranchName FROM branch WHERE Status='Active' ORDER BY BranchName" ;
              
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute();
            
            
            while($row = $fetchQuery->fetch()){
                $result[] = array("BranchID"=>$row['BranchID'], "BranchName"=>$row['BranchName']);
            }
        }
        
       
        
        
        
        return $result;
    }   
    
    
    
     
    
    
     public function getUserBrandBranch($UserName){
        
        if($UserName)
        {
            $sql = "SELECT T1.DefaultBrandID AS BrandID, T1.BranchID, T1.SuperAdmin, T2.BranchType FROM user AS T1 LEFT JOIN branch AS T2 ON T1.BranchID=T2.BranchID WHERE T1.Status='Active' AND T1.Username=:Username";
              
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':Username' => $UserName));
            $result = $fetchQuery->fetch();
            
            return $result;
        }
    }
    
    
    

    public function getBrands($NetworkID=false, $ClientID=false, $BranchID=false, $ManufacturerID=false, $ServiceProviderID=false){
        
        //return $this->collect('brand');
        $result = array();
              
        if($BranchID)
        {    
            $sql = "SELECT T2.BrandID, T2.BrandName, T2.BrandLogo, T2.AutoSendEmails, T2.EmailType, T2.Acronym FROM brand_branch AS T1 LEFT JOIN brand AS T2 ON T1.BrandID=T2.BrandID WHERE T1.Status='Active' AND T2.Status='Active' AND T1.BranchID=:BranchID GROUP BY T2.BrandID ORDER BY T2.BrandName";

            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':BranchID' => $BranchID));
            $result = $fetchQuery->fetchAll(PDO::FETCH_ASSOC);
        }
        else if($ClientID)
        {    
            $sql = "SELECT BrandID, BrandName, BrandLogo, AutoSendEmails, EmailType, Acronym FROM brand WHERE Status='Active' AND ClientID=:ClientID ORDER BY BrandName";

            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':ClientID' => $ClientID));
            $result = $fetchQuery->fetchAll(PDO::FETCH_ASSOC);
        }
        else  if($NetworkID)
        {    
            $sql = "SELECT BrandID, BrandName, BrandLogo, AutoSendEmails, EmailType, Acronym  FROM brand WHERE Status='Active' AND NetworkID=:NetworkID ORDER BY BrandName";

            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':NetworkID' => $NetworkID));
            $result = $fetchQuery->fetchAll(PDO::FETCH_ASSOC);
        }
        else  if($ManufacturerID)
        {    
            $sql = "SELECT T2.BrandID, T2.BrandName, T2.BrandLogo, T2.AutoSendEmails, T2.EmailType, T2.Acronym FROM network_manufacturer AS T1 LEFT JOIN brand AS T2 ON T1.NetworkID=T2.NetworkID WHERE T1.Status='Active' AND T2.Status='Active' AND T1.ManufacturerID=:ManufacturerID GROUP BY T2.BrandID ORDER BY T2.BrandName";

            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':ManufacturerID' => $ManufacturerID));
            $result = $fetchQuery->fetchAll(PDO::FETCH_ASSOC);
        }
        else  if($ServiceProviderID)
        {    
            $sql = "SELECT T2.BrandID, T2.BrandName, T2.BrandLogo, T2.AutoSendEmails, T2.EmailType, T2.Acronym FROM network_service_provider AS T1 LEFT JOIN brand AS T2 ON T1.NetworkID=T2.NetworkID WHERE T1.Status='Active' AND T2.Status='Active' AND (T1.ServiceProviderID=:ServiceProviderID or T2.BrandID=1000) GROUP BY T2.BrandID ORDER BY T2.BrandName";

            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':ServiceProviderID' => $ServiceProviderID));
            $result = $fetchQuery->fetchAll(PDO::FETCH_ASSOC);
        }
        else  
        {    
            $sql = "SELECT BrandID, BrandName, BrandLogo, AutoSendEmails, EmailType, Acronym FROM brand WHERE Status='Active' ORDER BY BrandName";

            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute();
            $result = $fetchQuery->fetchAll(PDO::FETCH_ASSOC);
        }
        

        return $result;
        
       // $NetworkID=null, $ClientID=null
    }
    
    /**
     *
     * @todo this function should return brands/brand if $BrandID is specified. And this won't return brand which is passed as $ExcludeBrandID
     * @param type $BrandID
     * @param type $ExcludeBrandID
     * @return array 
     */
    public function getBrand($BrandID=null, $ExcludeBrandID=null) {
    
        
        if($BrandID)
        {
            
                $query = $this->conn->query( "SELECT BrandID, BrandName, BrandLogo, NetworkID, ClientID FROM brand WHERE Status='Active' AND BrandID='".$BrandID."'" );
               
                $result = array();
                while($row = $query->fetch()){
                    $result[] = array("BrandID"=>$row['BrandID'], "Name"=>$row['BrandName'], "BrandLogo"=>$row['BrandLogo'], "NetworkID"=>$row['NetworkID'], "ClientID"=>$row['ClientID']);
                }
        }
        else if(isset($this->controller->user->SuperAdmin) && $this->controller->user->SuperAdmin)
        {
            if($ExcludeBrandID)
            {
                
                $query = $this->conn->query( "SELECT BrandID, BrandName FROM brand WHERE Status='Active' AND BrandID!='".$ExcludeBrandID."' ORDER BY BrandName" );
               
                $result = array();
                while($row = $query->fetch()){
                    $result[] = array("BrandID"=>$row['BrandID'], "Name"=>$row['BrandName']);
                }
                
            }
            else {
                
                $query = $this->conn->query( "SELECT BrandID, BrandName FROM brand WHERE Status='Active' ORDER BY BrandName" );
               
                $result = array();
                while($row = $query->fetch()){
                    
                    $result[] = array("BrandID"=>$row['BrandID'], "Name"=>$row['BrandName']);
                    
                }
                
            }
        
        }
        else if(isset($this->controller->user->Brands))
        {
            $result = array();
            foreach($this->controller->user->Brands as $bKey=>$bValue)
            {
                    
                $query = $this->conn->query( "SELECT BrandID, BrandName FROM brand WHERE Status='Active' AND BrandID='".$bKey."'" );


                while($row = $query->fetch()){
                    $result[] = array("BrandID"=>$row['BrandID'], "Name"=>$row['BrandName']);
                }

            }
              
        }
        else if(isset($this->controller->user->DefaultBrandID)) 
        {
            
            $result = array();
                    
            $query = $this->conn->query( "SELECT BrandID, BrandName FROM brand WHERE Status='Active' AND BrandID='".$this->controller->user->DefaultBrandID."'" );


            while($row = $query->fetch()){
                $result[] = array("BrandID"=>$row['BrandID'], "Name"=>$row['BrandName']);
            }
        }
            
            /*
        $result = array(
            array( 'code' => 'A', 'brand' => 'Brand Alpha' ),
            array( 'code' => 'B', 'brand' => 'Brnad Beta' )
        );
        
        */
        
        return $result;
    }
    
    public function getBrandStatus(){
        $result = array(
            array('BrandStatusID'=>1, 'Name'=>'BrandStatus 1'), 
            array('BrandStatusID'=>2, 'Name'=>'BrandStatus 2')
        );
        
        return $result;          
    }
    
    public function getBookingPerson(){
        $result = array(
            array( 'code' => 'A', 'person' => 'Person Alpha' ),
            array( 'code' => 'B', 'person' => 'Person Beta' )
        );
        
        return $result;        
    }
       
    public function getCountriesCounties() {
        
        /*$countries = $this->collect('country', null, 'CountryID, Name');
        $result = array();
        
        foreach($countries as $country){
            $c = $country;
            $c['Counties'] = $this->collect('county', array('CountryCode' => $country['CountryID']), 'CountyID, Name');
            $result[] = $c;
        }
        */
        
        
        
        $result = array();

        $query = $this->conn->query( "SELECT CountryID, Name, CountryCode FROM country WHERE Status='Active' AND BrandID='".$this->SkylineBrandID."' ORDER BY Name" );

        
        $result = array();
        while($row = $query->fetch()){
            
            
            $CountryID    = $row['CountryID'];
            
            $OkayFalg = true;
            if($this->controller->user->DefaultBrandID!=$this->SkylineBrandID)
            {
                 $query_1 = $this->conn->query( "SELECT CountryID, Name, Status  FROM country WHERE CountryCode='".$row['CountryCode']."' AND BrandID='".$this->controller->user->DefaultBrandID."'" );
            
                 $row_1 = $query_1->fetch();
                 
                 if(is_array($row_1))
                 {
                     if($row_1['Status']=="Active")
                     {
                         $row['Name'] = $row_1['Name'];
                         $row['CountryID'] = $row_1['CountryID'];
                     }
                     else
                     {
                          $OkayFalg = false;
                     }
                 }    
            }
            
            if($OkayFalg)
            {   
                $OkayFalg2 = true;
                $query2 = $this->conn->query( "SELECT CountyID, Name, CountyCode FROM county WHERE Status='Active' AND CountryID='".$CountryID."' AND BrandID='".$this->SkylineBrandID."' ORDER BY Name" );
                $result2 = array();
                while($row2 = $query2->fetch()){

                    if($this->controller->user->DefaultBrandID!=$this->SkylineBrandID)
                    {
                        
                         $query2_1 = $this->conn->query( "SELECT CountyID, Name, Status FROM county WHERE CountyCode='".$row2['CountyCode']."' AND CountryID='".$CountryID."' AND BrandID='".$this->controller->user->DefaultBrandID."' ORDER BY Name" );
                   
                         $row2_1 = $query2_1->fetch();
                 
                        if(is_array($row2_1))
                        {
                            if($row2_1['Status']=="Active")
                            {
                                $row2['Name'] = $row2_1['Name'];
                                $row2['CountyID'] = $row2_1['CountyID'];
                            }
                            else
                            {
                                $OkayFalg2 = false;
                            }
                        }    
                         
                    }
                    
                    if($OkayFalg2)
                    {
                        $result2[] = array("CountyID"=>$row2['CountyID'], "Name"=>$row2['Name']);
                    }
                }

                $result[] = array("CountryID"=>$row['CountryID'], "Name"=>$row['Name'], 'Counties'=>$result2);
            }
        }
        
        
        /****************
        
         $result = array(
                        0=>array(
                            'CountryID'=>'1',
                            'Name'=>'England',
                            'Counties'=> array(
                                '0'=>array('CountyID'=>1, 'Name'=>'Bedfordshire'),
                                '1'=>array('CountyID'=>2, 'Name'=>'Berkshire'),
                                '2'=>array('CountyID'=>3, 'Name'=>'Bristol'),
                                '3'=>array('CountyID'=>4, 'Name'=>'Buckinghamshire'),
                                '4'=>array('CountyID'=>5, 'Name'=>'Cambridgeshire')
                                )
                            ),
                        1=>array(
                            'CountryID'=>'2',
                            'Name'=>'Wales',
                            'Counties'=> array(
                                0=>array('CountyID'=>6, 'Name'=>'Anglesey'),
                                1=>array('CountyID'=>7, 'Name'=>'Brecknockshire'),
                                2=>array('CountyID'=>8, 'Name'=>'Caernarfonshire'),
                                3=>array('CountyID'=>9, 'Name'=>'Carmarthenshire'),
                                4=>array('CountyID'=>10, 'Name'=>'Cardiganshire')
                                )
                            ),
                        2=>array(
                            'CountryID'=>'3',
                            'Name'=>'Scotland',
                            'Counties'=> array(
                                0=>array('CountyID'=>11, 'Name'=>'Aberdeenshire'),
                                1=>array('CountyID'=>12, 'Name'=>'Angus'),
                                2=>array('CountyID'=>13, 'Name'=>'Argyllshire'),
                                3=>array('CountyID'=>14, 'Name'=>'Ayrshire'),
                                4=>array('CountyID'=>15, 'Name'=>'Banffshire')
                                )
                            ),
                        3=>array(
                            'CountryID'=>'4',
                            'Name'=>'Northern Ireland',
                            'Counties'=> array(
                                0=>array('CountyID'=>16, 'Name'=>'Antrim'),
                                1=>array('CountyID'=>17, 'Name'=>'Armagh'),
                                2=>array('CountyID'=>18, 'Name'=>'Down'),
                                3=>array('CountyID'=>19, 'Name'=>'FFermanagh'),
                                4=>array('CountyID'=>20, 'Name'=>'Londonderry')
                                )
                            )
                    );
         
         *****************/
        
        return $result;
    }   
    
    /**
    * getClientDefaultBranc
    *  
    * Get the default branch for a given ClientID
    * 
    * @param array $cId     The client id we wish to find the default branch for
    * 
    * @return getClientDefaultBranch    ID of default client fore network
    * 
    * @author Andrew Williams <a.williams@pccsuk.com>  
    ***************************************************************************/
    public function getClientDefaultBranch($cId) {      
        $sql = "
                SELECT
			`DefaultBranchID`
		FROM
			`client`
		WHERE
			`ClientID` = '$cId'
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ($this->debug) $this->controller->log(var_export($result,true));
        
        if ( count($result) > 0 ) {
            return($result[0]['DefaultBranchID']);                              /* Requested item exists */
        } else {
            return(null);                                                       /* Not found return null */
        }
    } 
    
    /**
     * 
     * 
     * @param array $args
     * @return array
     *
     * @author Simon Tsang s.tsang@pccsuk.com	 
     ********************************************/
    public function getCustomer( $ID ){
        return $this->collectByID('customer', array('CustomerID' => $ID), '*, LocalArea AS Area, TownCity As City ');
    }
    
    /**
     * getCustomerIdByNamePostcode
     *  
     * Get a customer's ID by their full name and postcode. This allows the API
     * to find out if a customer which has been passed by the service base as
     * strings rather than an CustomerID (as ServiceBase does not know Skyline
     * CustomerID) exists and what the ID is.
     * 
     * @param string $first_name    Customer's First Name
     * @param string $last_name     Customer's Last Name
     * @param string $postcode      Customer's Post Code
     * 
     * @return integer      ID of customer or NULL if not created
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function getCustomerIdByNamePostcode ($first_name, $last_name, $postcode) {
        $sql = "
                SELECT
			`CustomerID`
		FROM
			`customer`
		WHERE
			`ContactFirstName` = '$first_name'
                        AND `ContactLastName` = '$last_name'
                        AND `PostalCode` = '$postcode'
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['CustomerID']);                                   /* Customer exists */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    /**
     * Description
     * 
     * This method is used to build array of VAT Rates.
     * 
     * 
     * @return array $result The generated array of VAT Rates.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getVATRates() {
        
          $result  = $result1 = $result2 = array();
          
         
           $query = $this->conn->query( "SELECT VatRateID, Code, VatCode FROM vat_rate WHERE Status='Active' ORDER BY Code" );#
               
            while($row = $query->fetch()){
                $result1[] = array("VatRateID"=>$row['VatRateID'], "Code"=>$row['Code'], "VatCode"=>$row['VatCode']);
            }
            
          if($this->controller->user->DefaultBrandID!=$this->SkylineBrandID)
          {  
                $query2 = $this->conn->query( "SELECT VatRateID, Code, VatCode, Status FROM vat_rate" );#WHERE  BrandID='".$this->controller->user->DefaultBrandID."'
   
                while($row2 = $query2->fetch()){
                    $result2[] = array("VatRateID"=>$row2['VatRateID'], "Code"=>$row2['Code'], "VatCode"=>$row2['VatCode'], "Status"=>$row2['Status']);
                }
          }
          
         
          
          $result1_count = count($result1);
          $result2_count = count($result2);
          for($i=0;$i<$result1_count;$i++)
          {
              $OkayFlag = true;
              for($j=0;$j<$result2_count;$j++)
              {
                  if($result1[$i]['VatCode']==$result2[$j]['VatCode'])
                  {
                       if($result2[$j]['Status']=='Active')
                       {
                           $result1[$i]['VatRateID'] = $result2[$j]['VatRateID'];
                           $result1[$i]['Code']      = $result2[$j]['Code'];
                           $OkayFlag = true;
                           break;
                       }
                       else
                       {
                           $OkayFlag = false;
                       }    
                  }    
              }
              
              if($OkayFlag)
              {
                  $result[] = array("VatRateID"=>$result1[$i]['VatRateID'], "Code"=>$result1[$i]['Code']);
              }
          }
          
                
          return $result;  
                 
    } 
    
    
     /**
     * Description
     * 
     * This method is used to build array of Networks.
     * 
     * 
     * @return array $result The generated array of Networks.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getNetworks($form=null,$User=null) { 
         
         if($User != null){
            if($User->BranchID){
                $sql = "SELECT N1.NetworkID" . ($form!=null ? ' AS ID' : '') . ", N1.CompanyName 
                    FROM 
               network as N1 
                   LEFT JOIN 
               ".$this->table_network_client." as NC on (N1.NetworkID=NC.NetworkID)
                   LEFT JOIN 
               ".$this->table_client_branch." as CB on (NC.ClientID=CB.ClientID AND NC.NetworkID=CB.NetworkID)
                   WHERE 
               N1.Status='Active' 
                   AND
               N1.NetworkID=".$User->NetworkID."
                   AND
               NC.ClientID=".$User->ClientID."
                   AND
               CB.BranchID=".$User->BranchID."
                   ORDER BY 
               N1.CompanyName";
           }
           else if($User->ClientID){
                $sql = "SELECT N1.NetworkID" . ($form!=null ? ' AS ID' : '') . ", N1.CompanyName 
                    FROM 
               network as N1 LEFT JOIN ".$this->table_network_client." as NC on (N1.NetworkID=NC.NetworkID)
                   WHERE 
               N1.Status='Active' 
                   AND
               N1.NetworkID=".$User->NetworkID."
                   AND
               NC.ClientID=".$User->ClientID."
                   ORDER BY 
               N1.CompanyName";
           }
           else if($User->NetworkID){
                $sql = "SELECT N1.NetworkID" . ($form!=null ? ' AS ID' : '') . ", N1.CompanyName 
                    FROM 
               network as N1
                   WHERE 
               N1.Status='Active' 
                   AND
               N1.NetworkID=".$User->NetworkID."
                   ORDER BY 
               N1.CompanyName";
            }
            else if($User->ServiceProviderID){
                $sql = "SELECT N1.NetworkID" . ($form!=null ? ' AS ID' : '') . ", N1.CompanyName 
                    FROM 
               network as N1 LEFT JOIN ".$this->table_network_service_provider." as NS on (N1.NetworkID=NS.NetworkID)
                   WHERE 
               N1.Status='Active' 
                   AND
               NS.ServiceProviderID=".$User->ServiceProviderID."
                   ORDER BY 
               N1.CompanyName";

            }
            else{
                $sql = "SELECT NetworkID" . ($form!=null ? ' AS ID' : '') . ", CompanyName FROM network WHERE Status='Active' ORDER BY CompanyName";
            }
         }
         else{
             $sql = "SELECT NetworkID" . ($form!=null ? ' AS ID' : '') . ", CompanyName FROM network WHERE Status='Active' ORDER BY CompanyName";
         }
        
//         echo $sql."<BR>";
         
        $query = $this->conn->query( $sql );

//        $result = array();
//        while($row = $query->fetch()){
//            $result[] = array("NetworkID"=>$row['NetworkID'], "CompanyName"=>$row['CompanyName']);
//        }

        return $query->fetchAll();  
                 
    } 
    
    
    /**
     * Description
     * 
     * This method is used to get the network name.
     * 
     * @param integer $NetworkID
     * 
     * @return string $result 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getNetworkName($NetworkID) {
        
         $result = '';
         if($NetworkID)
         {    
            $query = $this->conn->query( "SELECT NetworkID, CompanyName FROM network WHERE NetworkID='".$NetworkID."' AND Status='Active' ORDER BY CompanyName" );

                    
            $row = $query->fetch();
                    
            $result = isset($row['CompanyName'])?$row['CompanyName']:'';
         }   
         return $result;  
    }
    
    
    
    /**
     * Description
     * 
     * This method is used to get the client name.
     * 
     * @param integer $ClientID
     * 
     * @return string $result 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getClientName($ClientID) {
        
         $result = '';
         if($ClientID)
         {    
            $query = $this->conn->query( "SELECT ClientID, ClientName FROM client WHERE ClientID='".$ClientID."' AND Status='Active'" );

                    
            $row = $query->fetch();
                    
            $result = isset($row['ClientName']) ? $row['ClientName'] : '';
         }   
         return $result;  
    }
    
    
    /**
     * getClientBranches
     * 
     * Return an array of Branches for each clientg.
     * 
     * @param integer $ClientID     A specific client ID to be searched for
     *                              
     * 
     * @return array $result    Generated array of branches for the network.
     * 
     * @author Andrew Williams <Andrew.Williams@awcomputech.com>  
     *************************************************************************/
    
     public function getClientBranches($ClientID=false) {
        
          $result = array();
          
          $where = "";
          
          if($ClientID) {
              $where = " AND cb.ClientID = $ClientID ";
          }
          
          $sql = "
                  SELECT 
			cb.ClientID AS ClientID,
			b.BranchID AS BranchID,
			b.BranchName AS BranchName 
                  FROM 
			`client_branch` AS cb LEFT JOIN `branch` AS b ON cb.BranchID = b.BranchID 
                  WHERE
			b.Status = 'Active'
                        $where
                 ";
          
          $query = $this->conn->query($sql);
           
          if($ClientID)
          {    
              while($row = $query->fetch()) {
//                  $result[] = array("ClientID"=>$row['ClientID'], "ClientName"=>$row['ClientName']);
                  $result[] = array("BranchID"=>$row['BranchID'], "BranchName"=>$row['BranchName']);
              } 
          }
          else
          {
              while($row = $query->fetch()) {
                  $result[$row['ClientID']][] = array("BranchID"=>$row['BranchID'], "BranchName"=>$row['BranchName']);
              }
          }
          
                
          return $result;  
                 
    } 
    
    
    /**
     * Description
     * 
     * This method is used to build array of Clients for each network.
     * 
     * @param string $NetworkID
     * 
     * @return array $result The generated array of Clients for each network.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getNetworkClients($NetworkID=false) {
        
	$result = array();
           
	if($NetworkID) {    

	    $query = $this->conn->query( "  SELECT	T1.NetworkID AS NetworkID, 
							T2.ClientID AS ClientID, 
							T2.ClientName AS ClientName, 
							T2.DefaultBranchID 
					    FROM	network_client AS T1 
					    LEFT JOIN	client AS T2 ON T1.ClientID=T2.ClientID 
					    WHERE	T1.NetworkID='" . $NetworkID . "' AND T2.Status='Active' 
					    ORDER BY	NetworkID, ClientName" );

	    while($row = $query->fetch()){
		$result[] = array("ClientID"=>$row['ClientID'], "ClientName"=>$row['ClientName'], "DefaultBranchID"=>$row['DefaultBranchID']);
//		$result[$row['NetworkID']][] = array("ClientID"=>$row['ClientID'], "ClientName"=>$row['ClientName'], "DefaultBranchID"=>$row['DefaultBranchID']);
	    }

	} else {
	    
	    $query = $this->conn->query( "  SELECT	T1.NetworkID AS NetworkID, 
							T2.ClientID AS ClientID, 
							T2.ClientName AS ClientName, 
							T2.DefaultBranchID 
					    FROM	network_client AS T1 
					    LEFT JOIN	client AS T2 ON T1.ClientID=T2.ClientID 
					    WHERE	T2.Status='Active' 
					    ORDER BY	NetworkID, ClientName" );

	    while($row = $query->fetch()){
		$result[$row['NetworkID']][] = array("ClientID"=>$row['ClientID'], "ClientName"=>$row['ClientName'], "DefaultBranchID"=>$row['DefaultBranchID']);
	    }
	    
	}

	return $result;  
                 
    } 
    
    
    /**
    * Description
    * 
    * This method is used to build array of Brands for each client.
    * 
    * 
    * @return array $result The generated array of Brands for each client.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
    */

    public function getClientBrands() {
	 
	$query = $this->conn->query( "SELECT BrandID, ClientID, BrandName FROM brand WHERE Status='Active'" );

	$result = array();
	while($row = $query->fetch()){
	    $result[$row['ClientID']][] = array("BrandID"=>$row['BrandID'], "BrandName"=>$row['BrandName']);
	}

	return $result;  
                 
    } 
    
    
    
     /**
     * Description
     * 
     * This method is used to build array of Brands for each Branch.
     * 
     * @param string $BranchID
     * 
     * @return array $result The generated array of Brands for each client.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getBranchBrands($BranchID) {
          $result = array();
          if($BranchID)
          {
            $query = $this->conn->query( "SELECT T1.BranchID AS BranchID, T2.BrandName AS BrandName FROM brand_branch AS T1 LEFT JOIN brand AS T2 ON T1.BrandID=T2.BrandID WHERE T1.BranchID='".$BranchID."' AND T1.Status='Active' AND T2.Status='Active'" );

                    
                    while($row = $query->fetch()){

                        if(isset($result[$row['BranchID']]) && $result[$row['BranchID']]!='')
                        {
                            $result[$row['BranchID']] .= ", ".$row['BrandName'];

                        }
                        else
                        {
                            $result[$row['BranchID']] = $row['BrandName'];
                        }   
                    }

                    }

                return $result;
          }
         
                
    
    
    /**
     * Description
     * 
     * This method is used for to get status of network for given Manufacturer.
     * 
     * @param string $ManufacturerID
     * @param string $NetworkID
     * 
     * @return boolean  
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getManufacturerNetworkStatus($ManufacturerID, $NetworkID) {
        
          if($ManufacturerID)
          {
            $query = $this->conn->query( "SELECT NetworkManufacturerID  FROM network_manufacturer AS T1 LEFT JOIN network AS T2 ON T1.NetworkID=T2.NetworkID WHERE T1.ManufacturerID='".$ManufacturerID."' AND T1.NetworkID='".$NetworkID."' AND T1.Status='Active' AND T2.Status='Active'" );

                    
                    $row = $query->fetch();
                    
                    if($row['NetworkManufacturerID'])
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
    
          }
          return false;      
    }
    
    
    
    
    /**
     * Description
     * 
     * This method is used for to get status of client for given Manufacturer.
     * 
     * @param string $ManufacturerID
     * @param string $ClientID
     * 
     * @return boolean  
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getManufacturerClientStatus($ManufacturerID, $ClientID) {
        
          if($ManufacturerID)
          {
            $query = $this->conn->query( "SELECT ClientManufacturerID FROM client_manufacturer AS T1 LEFT JOIN client AS T2 ON T1.ClientID=T2.ClientID WHERE T1.ManufacturerID='".$ManufacturerID."' AND T1.ClientID='".$ClientID."' AND T1.Status='Active' AND T2.Status='Active'" );

                    
                    $row = $query->fetch();
                    
                    if($row['ClientManufacturerID'])
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
    
          }
          return false;      
      }
    
    
    
    
    
    
    /**
     * Description
     * 
     * This method is used for to get status of Service Provider for given network.
     * 
     * @param string $ServiceProviderID
     * @param string $NetworkID
     * 
     * @return boolean  
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getServiceProviderNetworkStatus($ServiceProviderID, $NetworkID) {
    
          if($ServiceProviderID)
          {
            $query = $this->conn->query( "SELECT NetworkServiceProviderID  FROM network_service_provider AS T1 LEFT JOIN network AS T2 ON T1.NetworkID=T2.NetworkID WHERE T1.ServiceProviderID='".$ServiceProviderID."' AND T1.NetworkID='".$NetworkID."' AND T1.Status='Active' AND T2.Status='Active'" );
    
                    
                    $row = $query->fetch();
                    
                    if($row['NetworkServiceProviderID'])
                    {
                        return true;
                    }  
                    else
                    {
                        return false;
                    }

          }
          return false;      
    }
    
    
    
    
    /**
     * Description
     * 
     * This method is used for to get Service Providers for given network.
     * 
     * @param string $NetworkID
     * 
     * @return boolean  
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getNetworkServiceProviders($NetworkID, $diary=false, $diaryType='all') {

            $diaryCondition = '';
            if($diary)
            {    
                $diaryCondition = " AND T1.OnlineDiary='Active' ";
            }
            
            if($diaryType=='via')
            {
                $diaryCondition .= " AND T1.DiaryType='FullViamente' ";
            }
            else  if($diaryType=='sa')
            {
                $diaryCondition .= " AND T1.DiaryType='NoViamente' ";
            }
            else  if($diaryType=='ao')
            {
                $diaryCondition .= " AND T1.DiaryType='AllocationOnly' ";
            }
         
         
           // $result = array();
         
            $sql = "SELECT	T1.ServiceProviderID, 
				T1.CompanyName,
                                T1.DiaryType
                                
		    FROM	service_provider AS T1 
		    LEFT JOIN	network_service_provider AS T2 ON T1.ServiceProviderID=T2.ServiceProviderID 
		    WHERE	T2.NetworkID='" . $NetworkID . "' AND T1.Status='Active' ".$diaryCondition." AND T2.Status='Active'";
            
	    if($NetworkID == "") {
		$sql = "SELECT	    T1.ServiceProviderID, 
				    T1.CompanyName,
                                    T1.DiaryType

			FROM	    service_provider AS T1 
			LEFT JOIN   network_service_provider AS T2 ON T1.ServiceProviderID=T2.ServiceProviderID 
			WHERE	    T1.Status='Active' ".$diaryCondition." AND T2.Status='Active'";
	    }
	    
	    $query = $this->conn->query( $sql );
    
//            while($row = $query->fetch()) {
//                $result[] = array("ServiceProviderID"=>$row['ServiceProviderID'], 'CompanyName'=>$row['CompanyName']);
//            }
            $result = $query->fetchAll();
	    
	    $result = array_map("unserialize", array_unique(array_map("serialize", $result)));

            return $result;
          
    }
    
    
    
    public function getUserServiceProviders($user) {
	
	$q = "	SELECT	    DISTINCT(job.ServiceProviderID), sp.CompanyName 
		FROM	    job
		LEFT JOIN   service_provider AS sp ON job.ServiceProviderID = sp.ServiceProviderID
		WHERE	    job.ServiceProviderID IS NOT NULL
	     ";
	
	$values = [];
	
	if($user->UserType == "Network") {
	    $q .= " AND job.NetworkID = :networkID";
	    $values = ["networkID" => $user->NetworkID];
	}
	if($user->UserType == "Client") {
	    $q .= " AND job.ClientID = :clientID";
	    $values = ["clientID" => $user->ClientID];
	}
	if($user->UserType == "Branch") {
	    $q .= " AND job.BranchID = :branchID";
	    $values = ["branchID" => $user->BranchID];
	}
	
	$result = $this->query($this->conn, $q, $values);
	
	return $result;
	
    }
    
    
   /**
    * getNetworkServiceProviderAccountNo
    *  
    * Get the details from the network_service_provider table based on the
    * account number field.
    * 
    * @param array $aNo     The account number we wish to search for,
    * 
    * @return getNetworkServiceProviderAccountNo  Array of row from table
    * 
    * @author Andrew Williams <a.williams@pccsuk.com>  
    ***************************************************************************/
    public function getNetworkServiceProviderAccountNo($aNo) {      
        $sql = "
                SELECT
			*
		FROM
			`network_service_provider`
		WHERE
			`AccountNo` = '$aNo'
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]);                                                 /* Requested item exists */
        } else {
            return(null);                                                       /* Not found return null */
        }
    } 
    
    
    /**
    * getNetworkDefaultClient
    *  
    * Get the default client for a given NetworkID
    * 
    * @param array $nId     The network id we wish to find the defaulr client for
    * 
    * @return getNetworkDefaultClient   ID of default client fore network
    * 
    * @author Andrew Williams <a.williams@pccsuk.com>  
    ***************************************************************************/
    public function getNetworkDefaultClient($nId) {      
        $sql = "
                SELECT
			`DefaultClientID`
		FROM
			`network`
		WHERE
			`NetworkID` = '$nId'
               ";
        
        $result = $this->Query($this->conn, $sql);
       
        if ($this->debug) $this->controller->log(var_export($result,true));
        
        if ( count($result) > 0 ) {
            return($result[0]['DefaultClientID']);                              /* Requested item exists */
        } else {
            return(null);                                                       /* Not found return null */
        }
    } 
    
    
    /**
     * Description
     * 
     * This method is used for to get status of client for given Unit Type.
     * 
     * @param string $UnitTypeID
     * @param string $ClientID
     * 
     * @return boolean  
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getUnitTypeClientStatus($UnitTypeID, $ClientID) {
        
          if($UnitTypeID)
          {
            $query = $this->conn->query( "SELECT UnitClientTypeID FROM unit_client_type AS T1 LEFT JOIN unit_type AS T2 ON T1.UnitTypeID=T2.UnitTypeID WHERE T1.UnitTypeID='".$UnitTypeID."' AND T1.ClientID='".$ClientID."' AND T1.Status='Active'" );

                    
                    $row = $query->fetch();
                    
                    if($row['UnitClientTypeID'])
                    {
                        return true;
                    }  
                    else
                    {
                        return false;
                    }

          }
          return false;      
    }
    
    /*
     * 
     */
    public function getClients($User=null){
        
        if($User != null){
            if($User->BranchID){ 
                $jointable = $this->table_client." as C".
                    " LEFT JOIN ".
                $this->table_client_branch." as CB on (CB.ClientID = C.ClientID)";

                $columns = "C.ClientID AS ID, C.ClientName AS CompanyName";

                $conditions = "CB.BranchID=".$User->BranchID.
                    " AND ".
                "C.Status='Active'".
                    " AND ".
                "CB.Status='Active'";

                $orderconditions = " ORDER BY CompanyName";
            }
            else if($User->ClientID){
                $jointable = $this->table_client;
                $columns = "ClientID AS ID, ClientName AS CompanyName";
                $conditions = "Status='Active' AND ClientID=".$User->ClientID;
                $orderconditions = " ORDER BY CompanyName";
            }
            else if($User->NetworkID){
                $jointable = $this->table_client." as C".
                    " LEFT JOIN ".
                $this->table_network_client." as NC on (NC.ClientID = C.ClientID)";

                $columns = "C.ClientID AS ID, C.ClientName AS CompanyName";

                $conditions = "NC.NetworkID=".$User->NetworkID.
                    " AND ".
                "C.Status='Active'".
                    " AND ".
                "NC.Status='Active'";

                $orderconditions = " ORDER BY CompanyName";
            }
            else if($User->ServiceProviderID){
                $jointable = $this->table_client." as C".
                    " LEFT JOIN ".
                $this->table_network_client." as NC on (NC.ClientID = C.ClientID)".
                    " LEFT JOIN ".
                $this->table_network_service_provider." as NS on (NS.NetworkID = NC.NetworkID)";

                $columns = "C.ClientID AS ID, C.ClientName AS CompanyName";

                $conditions = "NS.ServiceProviderID=".$User->ServiceProviderID.
                    " AND ".
                "C.Status='Active'".
                    " AND ".
                "NS.Status='Active'";

                $orderconditions = " ORDER BY CompanyName";
            }
            else{
                $jointable = $this->table_client;
                $columns = "ClientID AS ID, ClientName AS CompanyName";
                $conditions = "Status='Active'";
                $orderconditions = " ORDER BY CompanyName";
            }
        }
        else{
            $jointable = $this->table_client;
            $columns = "ClientID AS ID, ClientName AS CompanyName";
            $conditions = "Status='Active'";
            $orderconditions = " ORDER BY CompanyName";
        }
        //return $this->collect('client', " ORDER BY CompanyName", 'ClientID AS ID, ClientName AS CompanyName');
        return $this->collect($jointable, $conditions.$orderconditions, $columns);
        
    }    
    
     /**
     * Description
     * 
     * This method is used for to get status of client for given Service Type.
     * 
     * @param string $ServiceTypeID
     * @param string $NetworkID 
     * @param string $ClientID
     * 
     * @return array  
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getClientServiceTypesStatus($ServiceTypeID, $NetworkID, $ClientID) {
          
          
        $query = $this->conn->query( "SELECT T1.ServiceTypeAliasID, T2.ServiceTypeName, T1.Status FROM service_type_alias AS T1 LEFT JOIN service_type AS T2 ON T1.ServiceTypeMask=T2.ServiceTypeID WHERE T1.ServiceTypeID='".$ServiceTypeID."' AND T1.NetworkID='".$NetworkID."' AND T1.ClientID='".$ClientID."'" );


         $row = $query->fetch();

         return $row;      
    }
    
    
    
    
    
    
    /**
     * Description
     * 
     * This method is used to build array of Service Providers.
     * 
     * 
     * @return array $result The generated array of Service Providers.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getServiceProviders($admin=null, $diary=false, $diaryType='all') {
         
        $diaryCondition = '';
        if($diary)
        {    
            $diaryCondition = " AND OnlineDiary='Active' ";
        }
        
        if($diaryType=='via')
        {
            $diaryCondition .= " AND DiaryType='FullViamente' ";
        }
        else  if($diaryType=='sa')
        {
            $diaryCondition .= " AND DiaryType='NoViamente' ";
        }
        else  if($diaryType=='ao')
        {
            $diaryCondition .= " AND DiaryType='AllocationOnly' ";
        }
        
        $sql = "SELECT ServiceProviderID " . ($admin!=null ? "AS ID" : "") .", CompanyName, DiaryType FROM service_provider WHERE Status='Active' ".$diaryCondition." ORDER BY CompanyName";

        $query = $this->conn->query( $sql );

        return $query->fetchAll();            
                 
    } 
    
    
    
    /**
     * Description
     * 
     * This method is used to build array of Extended Warrantors.
     * 
     * 
     * @return array $result The generated array of Extended Warrantors.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getExtendedWarrantor($stage=null) {
         
         switch($stage)
         {
             
            case 'sysuser' :
                
			$sql = "SELECT ExtendedWarrantorID AS ID,  ExtendedWarrantorName AS CompanyName FROM extended_warrantor WHERE Status='Active' ORDER BY CompanyName";
			break;
                    
            default:
                        $sql = "SELECT ExtendedWarrantorID, ExtendedWarrantorName FROM extended_warrantor WHERE Status='Active' ORDER BY ExtendedWarrantorName";
               
         }  
        

        $query = $this->conn->query( $sql );

        return $query->fetchAll();            
                 
    }
    
    
    
    
    /**
     * Description
     * 
     * This method is used to build array of Completion Statuses.
     * 
     * 
     * @return array $result The generated array of Completion Statuses.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getCompletionStatuses($NetworkID=null) {
    
          if($NetworkID)
          {
             $query = $this->conn->query( "SELECT CompletionStatusID, CompletionStatus FROM completion_status WHERE NetworkID='".$NetworkID."' AND Status='Active' ORDER BY Sort ASC" );
          }
          else
          {
              $query = $this->conn->query( "SELECT CompletionStatusID, CompletionStatus FROM completion_status WHERE Status='Active' ORDER BY Sort ASC" );
          }
               
                $result = array();
                while($row = $query->fetch()){
                    $result[] = array("CompletionStatusID"=>$row['CompletionStatusID'], "CompletionStatusName"=>$row['CompletionStatus']);
//                    $result[$row['CompletionStatusID']] = $row['CompletionStatus'];
                }
                
          return $result;  
                 
    } 
    
    
    
    
    
    
    
     /**
     * Description
     * 
     * This method is used to build array of countries of each brand.
     * 
     * 
     * @return array $result The generated array of countries of each brand.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getBrandsCountries() {
        
         
          $query = $this->conn->query( "SELECT BrandID, CountryID, Name FROM country WHERE Status='Active' ORDER BY BrandID,Name" );
               
                $result = array();
                while($row = $query->fetch()){
                    $result[$row['BrandID']][] = array("CountryID"=>$row['CountryID'], "Name"=>$row['Name']);
                }
                
          return $result;  
                 
    } 
    
    
    
    
    /**
     * Description
     * 
     * This method is used to build array of job types of each brand.
     * 
     * @param integer $BrandID 
     * 
     * @return array $result The generated array of job types of each brand.
     */
        
     public function getBrandsJobTypes($BrandID=false) {
         
          if($BrandID)
          {
              $query = $this->conn->query( "SELECT BrandID, JobTypeID, Type FROM job_type WHERE BrandID='".$BrandID."'AND Status='Active' ORDER BY BrandID,Type" );
          }     
          else
          {
            $query = $this->conn->query( "SELECT BrandID, JobTypeID, Type FROM job_type WHERE Status='Active' ORDER BY BrandID,Type" );
               
                   
          }    
          
                $result = array();
                while($row = $query->fetch()){
                    $result[$row['BrandID']][] = array("JobTypeID"=>$row['JobTypeID'], "Type"=>$row['Type']);
                }
                
          return $result;  
    } 
    
    
  
    
    
    /**
     * Description
     * 
     * This method is used to build array of Repair Skills.
     * 
     * 
     * @return array $result The generated array of Repair Skills.
     */
    
     public function getRepairSkills() {
        
          
            $query = $this->conn->query( "SELECT RepairSkillID, RepairSkillName FROM repair_skill WHERE Status='Active' ORDER BY RepairSkillName" );

                   
            $result = array();
            while($row = $query->fetch()){
                $result[] = array("RepairSkillID"=>$row['RepairSkillID'], "RepairSkillName"=>$row['RepairSkillName']);
            }

            return $result;  
    }
    
    
    /**
     * Does a specific job exist?
     * 
     * @param integer $jId      The ID of the job for which we wish to check exisits         
     * 
     * @return getJobExists     True if job exists false otherwise
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     */
    public function getJobExists($jId) {
        
        $sql = "
                SELECT
			COUNT(*) AS `Match`
		FROM
			job
		WHERE
			`JobID` = $jId
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        return($result['Match'] > 0);
    }
    
    /**
     * Does a specific job exist under a certain user?
     * 
     * @param integer $jId      The ID of the job for which we wish to check exisits 
     *        integer $uId      The ID of the user we wish to check        
     * 
     * @return getJobExistsUser     True if job exists for the specified user,  false otherwise
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     */
    public function getJobExistsUser($jId, $uId) {
        
        $sql = "
                SELECT
			COUNT(*) AS `Match`
		FROM
			(`job` j RIGHT JOIN `user` u ON u.`NetworkID` = j.`NetworkID`					-- User joined to job relation is dependent on user
			                             OR u.`ServiceProviderID` = j.`ServiceProviderID`			-- type: hence multiple right join. Assumes only relevant
			                             OR u.`ClientID` = j.`ClientID`				-- column in user filled and others are NULL.
			                             OR u.`BranchID` = j.`BranchID`)
		WHERE
			j.`JobID` = $jId
                        AND u.`UserID` = $uId
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        return($result[0]['Match'] > 0);
    }
    
    /**
     * 
     * 
     * @param string $jId
     * @return array
     *
     * @author Simon Tsang s.tsang@pccsuk.com	 
     *
     * Changes
     * Date        Version Author                   Reason
     * ??/??/????  1.00    Simon Tsang              Initial Version
     * 04/07/2012  1.01    Nageswara Rao Kanteti    Changes to query*  
     ********************************************/  
    public function getJobTypeName($JobTypeID) {
        
        $jb_job_type_name = '';
       
        /*
        $job_type_rows = $this->getJobTypes();
        foreach ($job_type_rows as $jt)
        {
            if($jt['JobTypeID']==$jId)
            {
                $jb_job_type_name = $jt['Name'];
                break;
            }
        }*/
        
        $sql = 'SELECT Type FROM  job_type  WHERE JobTypeID=:JobTypeID AND Status=:Status';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
      
        
        $fetchQuery->execute(array(':JobTypeID' => $JobTypeID, ':Status' => 'Active'));
        $result = $fetchQuery->fetch();
        
        return isset($result[0])?$result[0]:'';
    }
    
    
    
     /**
     * Part first order date for given job
     * 
     * @param string $JobID
     * @return string
     *
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>	 
     *
     ********************************************/  
    public function getFirstPartOrderDate($JobID) {
        
        $order_date = '';
        
        $sql = 'SELECT OrderDate FROM  part  WHERE JobID=:JobID ORDER BY OrderDate LIMIT 0,1';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
      
        
        $fetchQuery->execute(array(':JobID' => $JobID));
        $result = $fetchQuery->fetch();
        
        if(isset($result[0]))
        {    
            $statusID = $this->getStatusID('12 PARTS ORDERED');

            $status_history_model   = $this->controller->loadModel( 'StatusHistory' );
            $statusHitoryDetails    = $status_history_model->getStatusHistory($JobID, $statusID, "ORDER BY Date");
            
            return isset($statusHitoryDetails['Date'])?$statusHitoryDetails['Date']:'';
        }
        else
        {
            return '';
        }
       
    }
    
    
    
    /**
     * Part last due date for given job
     * 
     * @param string $JobID
     * @return string
     *
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>	 
     *
     ********************************************/  
    public function getLastPartDueDate($JobID) {
        
        $receivedDate = $this->getLastPartReceivedDate($JobID);
        
        if(!$receivedDate || $receivedDate=='0000-00-00')
        {
            $sql = 'SELECT DueDate FROM  part  WHERE JobID=:JobID AND DueDate IS NOT NULL ORDER BY DueDate DESC LIMIT 0,1';
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));


            $fetchQuery->execute(array(':JobID' => $JobID));
            $result = $fetchQuery->fetch();

            return isset($result[0])?$result[0]:false;
        }
        else
        {
            return false;
        }
    }
    
    
    
    /**
     * Part last ReceivedDate for given job
     * 
     * @param string $JobID
     * @return string
     *
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>	 
     *
     ********************************************/  
    public function getLastPartReceivedDate($JobID) {
        
        $order_date = '';
        
        $allPartsReceived = false;
        
        
        $sql1 = 'SELECT PartID FROM  part  WHERE JobID=:JobID AND OrderDate IS NOT NULL AND ReceivedDate IS NULL LIMIT 0,1';
        $fetchQuery1 = $this->conn->prepare($sql1, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery1->execute(array(':JobID' => $JobID));
        $result1 = $fetchQuery1->fetch();
        $allPartsReceived = isset($result1[0])?false:true;
        
        
       
        
        if($allPartsReceived)
        {
            
            $statusID = $this->getStatusID('13 PARTS RECEIVED');

            $status_history_model   = $this->controller->loadModel( 'StatusHistory' );
            $statusHitoryDetails    = $status_history_model->getStatusHistory($JobID, $statusID, "ORDER BY Date DESC");
            
            if(isset($statusHitoryDetails['Date']))
            {    
                return $statusHitoryDetails['Date'];
            }
            else
            {
                $sql = 'SELECT ReceivedDate FROM  part  WHERE JobID=:JobID ORDER BY ReceivedDate DESC LIMIT 0,1';
                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute(array(':JobID' => $JobID));
                $result = $fetchQuery->fetch();
                return isset($result[0])?$result[0]:'0000-00-00';
            }
            
        }
        else
        {
            return false;
        }
        
    }
    
    
    
    
    public function getJobStatus(){
        
        
           $sql = "SELECT StatusID, StatusName FROM status WHERE Status='Active' ORDER BY StatusName";
              
           $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
           $fetchQuery->execute();
           $result = $fetchQuery->fetchAll(PDO::FETCH_ASSOC);
        
        
        
        
        //$result = $this->collect('status', null, 'StatusID AS JobTypeID, StatusName AS Name');  
//        $result = array(
//            array('JobTypeID'=>1, 'Name'=>'Test Job Type1'), 
//            array('JobTypeID'=>2, 'Name'=>'Test Job Type2')
//        );
        
        return $result;
    }
    
    
    
    /**
     * Description
     * 
     * This method is used for to fetch list of active appointment types.
     *
     * @param array $args
     
     * @return array 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function getAppointmentTypes(){
        
        
           $sql = "SELECT AppointmentTypeID, `Type` FROM appointment_type WHERE Status='Active' ORDER BY `Type`";
              
           $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
           $fetchQuery->execute();
           
           $result = $fetchQuery->fetchAll(PDO::FETCH_ASSOC);
        
           return $result;
    }
    
    
     /**
     * Description
     * 
     * This method is used for to fetch job booking buttons.
     *
     * @param int $BrandID default null.
     
     * @return array 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function getJobBookingButtons($BrandID=null) {
        
        $JobTypesList = $this->getJobTypes($BrandID);
        
        $ButtonsList = array();
        
        $ButtonsList = $JobTypesList;
        
        return $ButtonsList;
    }
    
    public function getJobTypes($BrandID=null, $JobSettingsFlag=false) {
        
        if(!$BrandID)
        {
            $BrandID = $this->SkylineBrandID;
        }
        
        //joe change 03/05/2013 Andris
        //hide jobtype 1 for some users
      $where="";
        $hideJobType1=[937,938,939];//user id array
        if(in_array($this->controller->user->UserID,$hideJobType1)){
            $where.="and JobTypeID!='1'";
        }
        //end
        
        
        //Commented by Nag..  on 02/06/2013--for booking setup page
       // $result = $this->collect('job_type', "Status='Active'  $where  AND BrandID='".$BrandID."' ORDER BY Priority", 'JobTypeID, Type AS Name');  
        
        
        /*if($this->controller->user->BranchID)
        {
            $where .= " AND T2.NetworkID='".$this->controller->user->NetworkID."' AND T2.ClientID='".$this->controller->user->ClientID."' AND T2.BranchID='".$this->controller->user->BranchID."' ";
        }
        else*/
        
       /* if($this->controller->user->ClientID)
        {
            $where .= " AND T2.NetworkID='".$this->controller->user->NetworkID."' AND T2.ClientID='".$this->controller->user->ClientID."' ";
        }
        else if($this->controller->user->NetworkID)
        {
            $where .= " AND T2.NetworkID='".$this->controller->user->NetworkID."' ";
        }
        */
        
        
//        if($JobSettingsFlag)
//        {
//            $where .= " AND T2.JobSettingsID IS NOT NULL ";
//        }    
        
       //$result = $this->collect('job_type AS T1 LEFT JOIN job_settings AS T2 ON T1.JobTypeID=T2.JobTypeID', "T1.Status='Active'  $where  AND T1.BrandID='".$BrandID."' ORDER BY T1.Priority", 'T1.JobTypeID, T1.Type AS Name');   
        
        
        $result = $this->collect('job_type', "Status='Active'  $where  AND BrandID='".$BrandID."' ORDER BY Priority", 'JobTypeID, Type AS Name');   
        
      //  $this->log("Status='Active'  $where  AND BrandID='".$BrandID."' ORDER BY Priority");
      //  $this->log($result);
        
//        $result = array(
//            array('JobTypeID'=>1, 'Name'=>'Test Job Type1'), 
//            array('JobTypeID'=>2, 'Name'=>'Test Job Type2'),
//            array('JobTypeID'=>3, 'Name'=>'CAMERA')
//        );
        
        
        return $result;
    }
    
    
    public function getJobSites() {
        
        $result = array(
            0=>array('JobSiteCode'=>'field call', 'JobSiteName'=>'Field call'), 1=>array('JobSiteCode'=>'workshop', 'JobSiteName'=>'Workshop')
            );
        
        return $result;
    }
    
    public function getPaymentRoutes() {
        
        $result = array(
               
                0=>array('Code'=>'Claim Back', 'Name'=>'Claim Back'), 1=>array('Code'=>'Invoice', 'Name'=>'Invoice')
            
            );
        
        return $result;
    }
    
    
    public function getManufacturer($stage='', $NetworkID=null, $ClientID=null) {
        switch($stage){
            case 'alpha':
                $query = $this->conn->query( 'SELECT * FROM manufacturer WHERE ManufacturerName IS NOT NULL' );
                # $query->setFetchMode(PDO::FETCH_ASSOC);     
                #return $query->fetchAll(); 
                #$this->log('[message] :'.var_export($query->fetchAll(), true));
                
                $dropdown_list = array();
                while($row = $query->fetch()){
                    $dropdown_list[$row['ManufacturerName']] = $row['ManufacturerName'];//ManufacturerID
                }
                
                return $dropdown_list;                
                break;
            default:
               // $result = $this->collect('manufacturer', null, 'ManufacturerID, ManufacturerName');  
                # this would get data and return similar array
                #$query = $this->conn->query( 'SELECT * FROM manufacturer' );
                #$query->setFetchMode(PDO::FETCH_ASSOC);     
                #return $query->fetchAll();         
                
                //$result =  array(0=>array('ManufacturerID'=>1, 'ManufacturerName'=>'Test Manufacturer1'), 1=>array('ManufacturerID'=>2, 'ManufacturerName'=>'Test Manufacturer2'));
               
               
                if($ClientID)
                {
                    $query = $this->conn->query( "SELECT T1.ManufacturerID, T1.ManufacturerName, T1.ManufacturerLogo, T1.Acronym FROM manufacturer AS T1 LEFT JOIN client_manufacturer AS T2 ON T1.ManufacturerID=T2.ManufacturerID WHERE T1.Status='Active' AND T2.ClientID='".$ClientID."' AND T2.Status='Active' ORDER BY T1.ManufacturerName" );
                   
                }
                else if($NetworkID) 
                {
                    $query = $this->conn->query( "SELECT T1.ManufacturerID, T1.ManufacturerName, T1.ManufacturerLogo, T1.Acronym FROM manufacturer AS T1 LEFT JOIN network_manufacturer AS T2 ON T1.ManufacturerID=T2.ManufacturerID WHERE T1.Status='Active' AND T2.NetworkID='".$NetworkID."' AND T2.Status='Active' ORDER BY T1.ManufacturerName" );
                   
                }
                else
                {
                     if($stage=="sysuser")
                     {
                         $query = $this->conn->query( "SELECT ManufacturerID AS ID, ManufacturerName AS CompanyName, ManufacturerLogo, Acronym FROM manufacturer WHERE Status='Active' ORDER BY CompanyName" );
                     }
                     else
                     {
                         $query = $this->conn->query( "SELECT ManufacturerID, ManufacturerName, ManufacturerLogo, Acronym FROM manufacturer WHERE Status='Active' ORDER BY ManufacturerName" );
                     }
                     
                }    
                
                $result = $query->fetchAll();
        
                               
                return $result;                
        }

    }
    
    public function getModelNo(){
        $result = array(0=>array('JobTypeID'=>1, 'Name'=>'Test Job Type1'), 1=>array('JobTypeID'=>2, 'Name'=>'Test Job Type2'));
        
        return $result;           
    }
    
    /**
     * 
     * @return Array
     ********************************************/
    public function getPaymentType() {
        $result = $this->collect('payment_type', null, 'PaymentTypeID, Type AS PaymentTypeName');  
//        $result = array(
//            array('PaymentTypeID' => 'Cash', 'PaymentTypeName' => 'Cash'),
//            array('PaymentTypeID' => 'Cheque', 'PaymentTypeName' => 'Cheque'),
//            array('PaymentTypeID' => 'CreditCard', 'PaymentTypeName' => 'Credit Card'),
//            array('PaymentTypeID' => 'DebitCard', 'PaymentTypeName' => 'Debit Card'),
//            array('PaymentTypeID' => 'Account', 'PaymentTypeName' => 'Account'),
//            array('PaymentTypeID' => 'Goodwill', 'PaymentTypeName' => 'Goodwill')
//        );
//        
        return $result;
    }
    
    /*
     * 
     */
    public function getPermissions( $args, $RoleID ){# AND t2.PermissionID IS NULL
        $where_clause = ($RoleID != '') ? "t2.RoleID = $RoleID" : 't2.RoleID IS NULL';
        
        
        $tables = "permission AS t1 LEFT JOIN assigned_permission AS t2 ON t1.PermissionID = t2.PermissionID AND $where_clause
            LEFT JOIN role AS t3 ON t2.RoleID = t3.RoleID";
        
        $args['where'] = "t1.Status='Active'";
        
        if(isset($args['statusFlag']) && $args['statusFlag'])
        {
            if($args['statusFlag']=="Active")
            {
                $args['where'] = $args['where']." AND t2.RoleID IS NOT NULL";
            }
            else if($args['statusFlag']=="In-active")
            {
                $args['where'] = $args['where']." AND t2.RoleID IS NULL";
            }
        }    
        
        $args['where'] = $args['where']." GROUP BY t1.PermissionID";
        
        return $this->ServeDataTables(
                $this->conn, 
                $tables, 
                array('t1.PermissionID', 't1.Name', 't1.Description', 't2.RoleID'), 
                $args);

    }
    
    /**
     * Get a product id from a supplied model number and unit type
     * 
     * @param string $mNo       Model Number
     *        string $uType     Unit Type
     * 
     * @return getProductIdFromModelNoAndUnitType  The product ID or null if not found
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     */
    public function getProductIdFromModelNoAndUnitType($mNo, $uType) {
    
        $sql = "
                SELECT
                    p.`ProductID`
                FROM
                    `model` m,
                    `product` p,
                    `unit_type` ut
                WHERE
                    ut.`UnitTypeID` = p.`UnitTypeID`
                    AND m.`ModelID` = p.`ModelID`
                    AND ut.`UnitTypeName` = '$uType'
                    AND m.`ModelNumber` = '$mNo'
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if (count($result) <0 ) {
            return($result['ProductID']);                                       /* ProductID found */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    public function getProductLocationName($plId) {
        
        $product_location_name = '';
       
        $product_location_rows = $this->getProductLocations();
       
        foreach ($product_location_rows as $pl)
        {
            if($pl['ProductLocation']==$plId)
            {
                $product_location_name = $pl['Name'];
                break;
            }
        }
        
        return $product_location_name;
    }
    
    
    
    /**
    * 
    * 
    * @param array $args
    * @return array
    *
    * @author Simon Tsang s.tsang@pccsuk.com
    * 
    ********************************************/
    
    public function getProductLocations($ShowOnBookingPage=false) {

        $result = [
            array('ProductLocation'=>'Branch', 'Name'=>'Branch Address', 'ShowOnBookingPage'=>true),
            array('ProductLocation'=>'Customer', 'Name'=>'Customers Address', 'ShowOnBookingPage'=>true),
            array('ProductLocation'=>'Service Provider', 'Name'=>'Service Provider Address', 'ShowOnBookingPage'=>true),
            array('ProductLocation'=>'Supplier', 'Name'=>'Supplier', 'ShowOnBookingPage'=>false),
            array('ProductLocation'=>'Other', 'Name'=>'Manually Enter Collection Address', 'ShowOnBookingPage'=>true)
           // array('ProductLocation'=>'Store', 'Name'=>'Store')
        ];
       
        if($ShowOnBookingPage) {
            $newResult = array();    
            for($i=0;$i<count($result);$i++) {
                if($result[$i]['ShowOnBookingPage']) {
                    $newResult[] = $result[$i];
                }
            }
            $result = $newResult;
        }
        return $result;
    }
    
    
    
    public function getProductType(){

        return $this->collect('unit_type');  
       
    }
    
    public function getRepairTypes() {

        $result = array('0'=>array('RepairTypeID'=>'1', 'Name'=>'Customer'), '1'=>array('RepairTypeID'=>'2', 'Name'=>'Stock'));
        
        return $result;
    }
    
    public function getRepairTypeFromJobTypeID($JobTypeID) {
        
        if (is_numeric($JobTypeID)) {
            $sql = 'select TypeCode from job_type where JobTypeID=:JobTypeID';
            $params = array('JobTypeID' => intval($JobTypeID));
            $result = $this->Query($this->conn, $sql, $params);
            if (count($result) > 0) {
                if ($result[0]['TypeCode'] == 1) return 'Customer';
                if ($result[0]['TypeCode'] == 2) return 'Stock';
                if ($result[0]['TypeCode'] == 3) return 'Installation';
            }
        }

        return null;
    }
    
    /*
     * 
     */
    public function getRoles($UserID=null, $UserType=null){
        
        $where = "t1.Status='Active'";
        $where = $where.($UserType!=null ? " AND UserType='{$UserType}'" : '');
        
        $sql_userID = ($UserID == null ? 't2.UserID IS NULL' : "t2.UserID = {$UserID}");
        $dataset = $this->collect("role AS t1 LEFT JOIN user_role AS t2 ON t1.RoleID = t2.RoleID AND {$sql_userID}", $where, 't1.RoleID, t1.Name, t1.UserType, t2.UserID'); 
        #$this->controller->log('Skyline->getRoles :'.var_export($dataset, true));
        
       // $this->controller->log('UserType :'.var_export($UserType, true));

        $selected_access_rights = array();
        $dropdown_access_rights = array();
        
        // depend of the User which is selected or not
        if($UserID!=null){
            foreach($dataset as $d){
                if($d['UserID'] == $UserID)
                    $selected_access_rights[] = $d;
                else
                    $dropdown_access_rights[] = $d;
                    
            }
        }
        
        return (count($selected_access_rights) > 0 ) ? array('assigned' => $selected_access_rights, 'nonassigned' => $dropdown_access_rights) : $dataset;
    }       
    
    public function getSearchMethods() {
        
        $result = array('0'=>array('SearchMethodID'=>'1', 'Name'=>'Stock Code'), '1'=>array('SearchMethodID'=>'2', 'Name'=>'Model No'), '2'=>array('SearchMethodID'=>'3', 'Name'=>'Enter free text product description'));
        
        return $result;
    }

    /*
     * 
     */
    public function getSecurityQuestions() {
        
        return $this->collect('security_question', null, 'SecurityQuestionID, Question', 'Question');

    }
    
    public function getSerialNo(){
        $result = array(0=>array('JobTypeID'=>1, 'Name'=>'Test Job Type1'), 1=>array('JobTypeID'=>2, 'Name'=>'Test Job Type2'));
        
        return $result;           
    }
    
    public function getServiceCentre(){
        $result = array(0=>array('JobTypeID'=>1, 'Name'=>'Test Job Type1'), 1=>array('JobTypeID'=>2, 'Name'=>'Test Job Type2'));
        
        return $result;           
    }
    
    
    
     /**
     * Description
     * 
     * This method is used to get service centre details.
     * 
     * 
     * @return array $result The generated array of Plotforms.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    public function getServiceCenterDetails( $ServiceCenterID=null, $JobID=null ) {
               
        $result = array();
        
        if($ServiceCenterID)
        {           
            $sql = "SELECT T1.ServiceProviderID, T1.CompanyName, T1.ContactPhone AS ContactWorkPhone, T1.ContactEmail, T1.`BuildingNameNumber` , T1.`Street`, 
                T1.LocalArea, T1.PostalCode, T1.TownCity, T2.Name AS County, T3.Name AS Country, T1.WeekdaysOpenTime, T1.WeekdaysCloseTime, T1.SaturdayOpenTime, T1.SaturdayCloseTime,
                T1.ContactPhone, T1.ContactPhoneExt, T1.ServiceProvided, T1.SynopsisOfBusiness, 
                T1.AdminSupervisor, T1.ServiceManager, T1.Accounts, T1.OnlineDiary, T1.CountyID, T1.CountryID, T1.CourierStatus, T1.SetupUniqueTimeSlotPostcodes,
                CONCAT_WS(' ', T1.GeneralManagerForename, T1.GeneralManagerSurname) AS GeneralManagerName, T1.GeneralManagerEmail, CONCAT_WS(' ', T1.ServiceManagerForename, T1.ServiceManagerSurname) AS ServiceManagerName, T1.ServiceManagerEmail,
                CONCAT_WS(' ', T1.AdminSupervisorForename, T1.AdminSupervisorSurname) AS AdminSupervisorName, T1.AdminSupervisorEmail, T1.PublicWebsiteAddress
                
                FROM service_provider AS T1
                LEFT JOIN county AS T2 ON T1.CountyID=T2.CountyID
                LEFT JOIN country AS T3 ON T1.CountryID=T3.CountryID
                WHERE T1.Status='Active' AND T1.ServiceProviderID=:ServiceProviderID";
              
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':ServiceProviderID' => $ServiceCenterID));
            $result = $fetchQuery->fetch();
             
        }
        else if($JobID)
        {
             $sql = "SELECT T1.ServiceProviderID, T1.CompanyName, T1.ContactPhone AS ContactWorkPhone, T1.ContactEmail, T1.`BuildingNameNumber` , T1.`Street`, 
                T1.LocalArea, T1.PostalCode, T1.TownCity, T2.Name AS County, T3.Name AS Country, T1.WeekdaysOpenTime, T1.WeekdaysCloseTime, T1.SaturdayOpenTime, T1.SaturdayCloseTime,
                T1.ContactPhone, T1.ContactPhoneExt, T1.ServiceProvided, T1.SynopsisOfBusiness, T0.ServiceCentreJobNo, 
                T1.AdminSupervisor, T1.ServiceManager, T1.Accounts, T0.ClientID, T1.OnlineDiary, T1.CountyID, T1.CountryID, T1.CourierStatus, T1.SetupUniqueTimeSlotPostcodes,
                CONCAT_WS(' ', T1.GeneralManagerForename, T1.GeneralManagerSurname) AS GeneralManagerName, T1.GeneralManagerEmail, CONCAT_WS(' ', T1.ServiceManagerForename, T1.ServiceManagerSurname) AS ServiceManagerName, T1.ServiceManagerEmail,
                CONCAT_WS(' ', T1.AdminSupervisorForename, T1.AdminSupervisorSurname) AS AdminSupervisorName, T1.AdminSupervisorEmail, T1.PublicWebsiteAddress
                
                FROM job AS T0
                LEFT JOIN service_provider AS T1 ON T0.ServiceProviderID=T1.ServiceProviderID
                LEFT JOIN county AS T2 ON T1.CountyID=T2.CountyID
                LEFT JOIN country AS T3 ON T1.CountryID=T3.CountryID
                WHERE T1.Status='Active' AND T0.JobID=:JobID";
              
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':JobID' => $JobID));
            $result = $fetchQuery->fetch();
            
            
            
            
            if(isset($result['ServiceProviderID']))
            {   
               $ServiceProvidersModel = $this->controller->loadModel('ServiceProviders');
               $spClientContacts = $ServiceProvidersModel->getServiceProviderClientContacts($result['ServiceProviderID'], $result['ClientID']);
               
               
               
               
               if(is_array($spClientContacts))
               {
                   
                   if(isset($spClientContacts[0]['ContactPhone']) && $spClientContacts[0]['ContactPhone'])
                   {
                      $result['ContactWorkPhone']  = $spClientContacts[0]['ContactPhone'];
                   }
                   
                   if(isset($spClientContacts[0]['ContactEmail']) && $spClientContacts[0]['ContactEmail'])
                   {
                      $result['ContactEmail']  = $spClientContacts[0]['ContactEmail'];
                   }
               }    
            }
            
            
        }  
                
        if(is_array($result) && count($result))
        {
            $result['OpeningHours'] = $result['WeekdaysOpenTime']." - ".$result['WeekdaysCloseTime']."<br>".$result['SaturdayOpenTime']." - ".$result['SaturdayCloseTime'];
            $result['PrincipleContacts'] = array();
            $result['FullAddress'] =  trim($result['BuildingNameNumber']." ".$result['Street']);
            
            if($result['LocalArea'])
            {
                if($result['FullAddress'])
                {
                    $result['FullAddress'] .= ', ';
                    
                }
                
                $result['FullAddress'] .= $result['LocalArea'];
                
            }
            
            if($result['TownCity'])
            {
                if($result['FullAddress'])
                {
                    $result['FullAddress'] .= ', ';
                    
                }
                
                $result['FullAddress'] .= $result['TownCity'];
                
            }
            if($result['County'])
            {
                if($result['FullAddress'])
                {
                    $result['FullAddress'] .= ', '; 
                }
                
                $result['FullAddress'] .= $result['County'];
                
            }
            if($result['PostalCode'])
            {
                if($result['FullAddress'])
                {
                    $result['FullAddress'] .= ', ';
                }
                
                //$result['FullAddress'] .= str_replace(' ','&nbsp;', $result['PostalCode']);
                $result['FullAddress'] .= $result['PostalCode'];
                
            }
           
            $result['ServiceCentreJobNo'] = isset($result['ServiceCentreJobNo'])?$result['ServiceCentreJobNo']:'';
            
        }  
        
                
         //'JobNumber'=>'234567',
        /*
        $result = array(
                        'ServiceCentreID'=>'456',
                        'CompanyName'=>'DSL', 
                        'ContactWorkPhone'=>'78939282773', 
                        'ContactEmail'=>'services@dsl.com', 
                        'AddressLine1'=>'22 Primatt Crescent',
                        'AddressLine2'=>'Shenley Church End',
                        'PostalCode'=>'MK5 6AS',
                        'City'=>'Milton Keynes',
                        'State'=>'Buckinghamshire',
                        'Country'=>'UK',
                        'JobNumber'=>'234567',
                        'OpeningHours' => '[opening hours]',
                        'PrincipleContacts' => array(
                            array('Position' => '[position]', 'Fullname' => '[fullname]', 'ContactNo' => '[contact no]', 'Email' => '[email address]'),
                            array('Position' => '[position]', 'Fullname' => '[fullname]', 'ContactNo' => '[contact no]', 'Email' => '[email address]'),
                            array('Position' => '[position]', 'Fullname' => '[fullname]', 'ContactNo' => '[contact no]', 'Email' => '[email address]')
                        ),
                        'ServiceProvided' => 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices 
                    posuere cubilia Curae; Sed bibendum auctor vulputate. Vivamus et 
                    faucibus metus. Nullam vel odio nec sem volutpat eleifend vitae 
                    quis nulla',
                        'SynopsisOfBusiness' => 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices 
                    posuere cubilia Curae; Sed bibendum auctor vulputate. Vivamus et 
                    faucibus metus. Nullam vel odio nec sem volutpat eleifend vitae 
                    quis nulla'
            
                        );*/
        
        
        
        return $result;
    }

  
    public function getServiceTypeName($sId) {
        
//        $jb_service_type_name = '';
//       
//        $service_type_rows = $this->getServiceTypes();
//       
//        foreach ($service_type_rows as $st)
//        {
//            if($st['ServiceTypeID']==$sId)
//            {
//                $jb_service_type_name = $st['Name'];
//                break;
//            }
//        }
        
        $ServiceTypesModel  = $this->controller->loadModel('ServiceTypes');
        
        $ServiceTypesDetails  = $ServiceTypesModel->fetchRow(array('ServiceTypeID'=>$sId));
        
        if(isset($ServiceTypesDetails['ServiceTypeName']))
        {
            return $ServiceTypesDetails['ServiceTypeName'];
        }
        else
        {
            return '';
        }
        
    }
    
    
    
    public function getServiceTypes($BrandID = null, $JobTypeID = null, $NetworkID = null, $ClientID = null) {
        
        $result = [];
        
        if($NetworkID) {
	    
            $paramList = [':NetworkID' => $NetworkID];
            $whereTail = '';
            
            if($ClientID) {
                $whereTail = " AND T2.ClientID = :ClientID";
                $paramList[':ClientID'] = $ClientID;
            }
            
            if($BrandID) {
                $whereTail .= " AND T1.BrandID = :BrandID";
                $paramList[':BrandID'] = $BrandID;
            }
            
            if($JobTypeID) {
                $whereTail .= " AND T1.JobTypeID = :JobTypeID";
                $paramList[':JobTypeID'] = $JobTypeID;
            }
            
            $sql = "SELECT	DISTINCT
                                T1.ServiceTypeID, 
				IF (ISNULL(T3.`ServiceTypeName`),
				    T1.`ServiceTypeName`,
				    T3.`ServiceTypeName`
				) AS `Name`  
			    
		    FROM	service_type AS T1 
		    
		    LEFT JOIN	service_type_alias AS T2 ON T1.ServiceTypeID=T2.ServiceTypeID AND T2.NetworkID=:NetworkID  
		    LEFT JOIN	service_type AS T3 ON T2.ServiceTypeMask=T3.ServiceTypeID AND T3.Status='Active' 
		    
		    WHERE	T1.Status = 'Active' AND T2.Status = 'Active'" . $whereTail . " 
		
		    ORDER BY T1.Priority
		   ";
            
            $fetchQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            
            $fetchQuery->execute($paramList);
            
            while($row = $fetchQuery->fetch()){
		$result[] = ['ServiceTypeID' => $row['ServiceTypeID'], 'Name' => $row['Name']];
            }
            
        } else if($BrandID || $JobTypeID) {
                
            $where = "Status='Active'";
	    
            if($BrandID) {
                $where .= " AND BrandID='" . $BrandID . "'";
            }
            
            if($JobTypeID) {
                $where .= " AND JobTypeID='" . $JobTypeID . "'";
            }
            
            $result = $this->collect('service_type', $where." ORDER BY Priority", 'ServiceTypeID, ServiceTypeName AS Name');
	    
        } else {
	    
            $result = $this->collect('service_type', "Status='Active' ORDER BY Priority", 'ServiceTypeID, ServiceTypeName AS Name');
	    
        }
       
        return $result;
	
    }
     
    
    
    public function getStockCode(){
        $result = array(
            array( 'code' => 'A', 'stock_code' => '125alpha' ),
            array( 'code' => 'B', 'stock_code' => '1234beta' )
        );
        
        return $result;            
    }
    
    /**
     * getSystemStatus()
     * 
     * @return array
     ********************************************/
    public function getSystemStatus(){
        return $this->collect('status', "Status='Active' ORDER BY StatusName", "StatusID, StatusName");        
    }
    
    public function getStatusID($status) {
        $statusID = 0;
        $query = "select StatusID from status where StatusName=:status and Status='Active'";
        $result = $this->Query($this->conn, $query, array( 'status' => $status ));
        if ($result != null && count($result) > 0) {
            $statusID = $result[0]['StatusID'];
        }
        return $statusID;   
    }
    
    public function getAuditTrailActionID($action_code) {
        $actionID = 0;
        $query = "select AuditTrailActionID from audit_trail_action where (BrandID=:brandID or BrandID=:skyline) and ActionCode=:action_code and Status='Active' order by BrandID desc limit 0,1";
        $result = $this->Query($this->conn, $query, array( 'action_code' => $action_code, 
                                                           'brandID' => ($this->controller->user == null)  ? constants::SKYLINE_BRAND_ID : $this->controller->user->DefaultBrandID, 
                                                           'skyline' => 1000 ));
        if ($result != null && count($result) == 1) {
            $actionID = $result[0]['AuditTrailActionID'];
        }
        return $actionID;
    }

    public function getTitleName($tId) {
        
        $jb_title_name = '';
        $title_rows = $this->getTitles();
        foreach ($title_rows as $tr)
        { 
           if($tr['TitleID']==$tId)
            {
                $jb_title_name = $tr['Name'];
                break;
            }
        }
        
        return $jb_title_name;
    }
    
    public function getTitles($BrandID=null) {        
        
       if(!$BrandID)
       {
           $BrandID = $this->SkylineBrandID;
       }
       
       $result = $this->collect('customer_title', "Status='Active' AND BrandID='".$BrandID."'", 'CustomerTitleID AS TitleID, Title AS Name', 'Name');
       
       //$this->controller->log('Skyline->getTitles : ' . var_export($result, true));
        
       return $result;
    }
    
    public function getUnitLocation(){
        $result = array(
            array( 'code' => 'A', 'unit_location' => '125alpha' ),
            array( 'code' => 'B', 'unit_location' => '1234beta' )
        );
        
        return $result;               
        
    }


    public function getUnitTypes($stage=null, $ClientID=null, $RepairSkillID=null) {
        
            switch($stage){
                case 'alpha':

                    $query = $this->conn->query( 'SELECT * FROM unit_type' );

                    $dropdown_list = array();
                    while($row = $query->fetch()){
                        $dropdown_list[$row['UnitTypeName']] = $row['UnitTypeName'];
                    }
                    return $dropdown_list;  

                    break;
                default:

                    if($ClientID)
                    {    
                        $wherePart = "";
                        if($RepairSkillID)
                        {
                            $wherePart .= " AND T1.RepairSkillID='".$RepairSkillID."'";
                        }

                        $query = $this->conn->query( "SELECT T1.UnitTypeID, T1.UnitTypeName FROM unit_type AS T1 LEFT JOIN unit_client_type AS T2 ON T1.UnitTypeID=T2.UnitTypeID WHERE T2.ClientID='".$ClientID."' AND T1.Status='Active' AND T2.Status='Active' ".$wherePart." ORDER BY T1.UnitTypeName" );
                    }
                    else
                    {
                        $wherePart = "";
                        if($RepairSkillID)
                        {
                            $wherePart .= " AND RepairSkillID='".$RepairSkillID."'";
                        }

                        $query = $this->conn->query( "SELECT UnitTypeID, UnitTypeName FROM unit_type WHERE Status='Active' ".$wherePart." ORDER BY UnitTypeName" );
                    }

                    $result = array();

                    while($row = $query->fetch()){
                        $result[] = array('UnitTypeID'=>$row['UnitTypeID'], 'UnitTypeName'=>$row['UnitTypeName']);
                    }


            return $result;        
        }
        
    }

    public function getUnitTypeName($pId) {
        
        $jb_unit_type_name = '';
       
        $unit_type_rows = $this->getUnitTypes();
       
        foreach ($unit_type_rows as $pt)
        {
            if($pt['UnitTypeID']==$pId)
            {
                $jb_unit_type_name = $pt['UnitTypeName'];
                break;
            }
        }
        
        return $jb_unit_type_name;
    }
    
    /*
     * listed of user types is displayed depending on login user
     */
    public function getUserType(){
        #$this->controller->log('Skyline->getUserType :'.var_export($this->controller->user, true));
        $user_type = array();
        if( $this->controller->user->SuperAdmin != 0){   
            $user_type[] = array('Name' => 'ServiceProvider',  'Value' => 'Service Provider');
            $user_type[] = array('Name' => 'Manufacturer',  'Value' => 'Manufacturer');
            $user_type[] = array('Name' => 'ExtendedWarrantor',  'Value' => 'Extended Warrantor');
        }
        if( $this->controller->user->SuperAdmin != 0 || $this->controller->user->NetworkID != null){
            $user_type[] = array('Name' => 'Network',          'Value' => 'Network');
            $user_type[] = array('Name' => 'Client',           'Value' => 'Client');
        }
        $user_type[] = array('Name' => 'Branch',           'Value' => 'Branch');
        asort($user_type);
        return $user_type;
    }
    
    
    
    /**
    * Description
    * 
    * This method is for to get Authorisation Types.
    * 
    * @return array   
    * 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
    
    public function getAuthorisationTypes() {
      
	$authorisationTypes = [];
        
	$checkAlt = function($name) {
	    $q = "  SELECT	* 
		    FROM	alternative_fields AS af
		    LEFT JOIN	primary_fields AS pf ON af.primaryFieldID = pf.primaryFieldID
		    WHERE	af.BrandID = :brandID AND 
				pf.primaryFieldName = :name AND
				af.status = 'Active'
		 ";
	    $values = ["brandID" => $this->controller->user->DefaultBrandID, "name" => $name];
	    $result = $this->query($this->conn, $q, $values);
	    return (isset($result[0])) ? $result[0]["alternativeFieldName"] : false;
	};
	
        if(isset($this->controller->user)) {    
	    
            //$usrModel = $this->controller->loadModel('Users');
            
            if(Functions::hasPermission($this->controller->user, 'AP7029')) {
                $authorisationTypes[] = ['Name' => 'return', 'Value' => ($checkAlt("Return") ? $checkAlt("Return") : "Return")];
            }
            
            if(Functions::hasPermission($this->controller->user, 'AP7030')) {
                $authorisationTypes[] = ['Name' => 'repair', 'Value' => ($checkAlt("Repair") ? $checkAlt("Repair") : "Repair")];
            } 
            
            if(Functions::hasPermission($this->controller->user, 'AP7031')) {
                $authorisationTypes[] = ['Name' => 'on hold', 'Value' => ($checkAlt("On Hold") ? $checkAlt("On Hold") : "On Hold")];
            }
            
            if(Functions::hasPermission($this->controller->user, 'AP7032')) {
                $authorisationTypes[] = ['Name' => 'estimate', 'Value' => ($checkAlt("Estimate") ? $checkAlt("Estimate") : "Estimate")];
            }
            
            if(Functions::hasPermission($this->controller->user, 'AP7033')) {
                $authorisationTypes[] = ['Name' => 'invoice', 'Value' => ($checkAlt("Invoice") ? $checkAlt("Invoice") : "Invoice")];
            }
            
        }
        
        return $authorisationTypes;
	
    }
    
    
    
    // Temp
    public function getTimeline(){
        return 'data';
    }
    
//    24 Apr 2012
//    public function getFullAddress(){
//        return 'Test Contact full address : PC Control Systems Ltd, Hamilton House, 66 Palmerston Road, Northampton, NN1 5EX';
//    }
    
    public function getTAT(){
        return 10;
    }
    
    /**
     * 
     * collect fn gets the all the data from database table
     * 
     * @param string $table database table name
     * @param array $where name array( 'tabename' => 'value') 
     * @param string $select    
     * @return array
     * 
     * @author Simon Tsang s.tsang@pccsuk.com
     ********************************************/
       private function collect($table, $where=null, $select='*', $order = ''){        
        $where_clause = ''; 
         
        if( is_array($where) ){ 
            foreach($where as $field => $value){ 
                $where_clause .= ( $where_clause=='' ? 'WHERE ' : ' AND ' ) . "$field = '$value'"; 
            } 
        }else if(is_string($where)){ 
            if( stristr(strtolower( $where ), 'where') ) 
                $where_clause = $where; 
            else 
                $where_clause = ' WHERE ' . $where; 
        } 
        if($order != "")
            $orderby_clause = "order by ".$order;
        else
            $orderby_clause = "";
        $sql = "SELECT $select FROM $table $where_clause $orderby_clause";  
        #$this->controller->log('Skyline->collect :' . $sql); 
 
           
        $query = $this->conn->query( $sql ); 
        $query->setFetchMode( PDO::FETCH_ASSOC ); #FETCH_NUM    
         
        return $query->fetchAll();           
         
    }
    
    /**
     * 
     * collect fn gets the all the data from database table
     * 
     * @param string $table database table name
     * @param array $where name array( 'tabename' => 'value') 
     * @param string $select    
     * @return array
     * 
     * @author Simon Tsang s.tsang@pccsuk.com
     ********************************************/
    private function collectByID($table, $where=null, $select='*'){        
        $where_clause = '';
         
        if( is_array($where) ){ 
            foreach($where as $field => $value){ 
                $where_clause .= ( $where_clause=='' ? 'WHERE ' : ' AND ' ) . "$field = '$value'";
            } 
        }else if(is_string($where)){ 
            if( stristr(strtolower( $where ), 'where') ) 
                $where_clause = $where;
            else 
                $where_clause = ' WHERE ' . $where;
        } 
         
        $sql = "SELECT $select FROM $table $where_clause LIMIT 0, 1"; 
        #$this->controller->log('Skyline->collectByID :' . $sql); 
 
           
        $query = $this->conn->query( $sql ); 
        $query->setFetchMode( PDO::FETCH_ASSOC ); #FETCH_NUM    
         
        return $query->fetch();           
         
    } 
    
    
    /**
     * Description
     * 
     * This method is used to get array of Plotforms.
     * 
     * 
     * @return array $result The generated array of Plotforms.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getPlotforms() {
         $result = array(
             0=>array('Code'=>'ServiceBase', 'Name'=>'Service Base'),
             1=>array('Code'=>'RMA Tracker', 'Name'=>'RMA Tracker'),
             2=>array('Code'=>'API', 'Name'=>'API'),
             3=>array('Code'=>'Skyline', 'Name'=>'Skyline')
         );
         asort($result);
         return $result;        
    }
    
    
    
    
    
    /**
     * Description
     * 
     * This method is used for to get model details for given model number.
     * @param string $ModelNumber
     * @param string $NetworkID
     * @param string $ManufacturerID
     * 
     * @return array $result The generated array of Model details.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getModelDetails($ModelNumber, $NetworkID, $ManufacturerID) {
        
          $result = array();
          
          if($NetworkID && $ManufacturerID)
          {
                $sql = "SELECT T1.ModelID, T1.ManufacturerID, T1.UnitTypeID, T1.ModelDescription FROM model AS T1 LEFT JOIN network_manufacturer AS T2 ON T1.ManufacturerID=T2.ManufacturerID AND T2.NetworkID=:NetworkID WHERE T1.Status='Active' AND T2.Status='Active' AND T1.ManufacturerID=:ManufacturerID AND T1.ModelNumber=:ModelNumber LIMIT 0,1" ;
              
              
                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute(array(':ModelNumber' => $ModelNumber, ':NetworkID' => $NetworkID, ':ManufacturerID' => $ManufacturerID));
                $result = $fetchQuery->fetch();
          }
          else if($NetworkID)
          {
                $sql = "SELECT T1.ModelID, T1.ManufacturerID, T1.UnitTypeID, T1.ModelDescription FROM model AS T1 LEFT JOIN network_manufacturer AS T2 ON T1.ManufacturerID=T2.ManufacturerID AND T2.NetworkID=:NetworkID WHERE T1.Status='Active' AND T2.Status='Active' AND T1.ModelNumber=:ModelNumber LIMIT 0,1" ;
              
              
                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute(array(':ModelNumber' => $ModelNumber, ':NetworkID' => $NetworkID));
                $result = $fetchQuery->fetch();
          }
          else if($ModelNumber)
          {
              $sql = "SELECT ModelID, ManufacturerID, UnitTypeID, ModelDescription FROM model WHERE Status='Active' AND ModelNumber=:ModelNumber LIMIT 0,1" ;
              
              
              $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
              $fetchQuery->execute(array(':ModelNumber' => $ModelNumber));
              $result = $fetchQuery->fetch();

          }      
          return $result;  
                 
    } 
    
    
    
    
    
    /**
     * Description
     * 
     * This method is used for to get Job Source ID for given value.
     * @param string $Description
     * 
     * @return int $JobSourceID.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getJobSourceID($Description) {
        
          $JobSourceID = NULL;
          
          if($Description)
          {
                $sql = "SELECT JobSourceID FROM job_source WHERE Description=:Description LIMIT 0,1" ;
              
              
                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute(array(':Description' => $Description));
                $result = $fetchQuery->fetch();
                if(is_array($result) && isset($result['JobSourceID']))
                {
                    $JobSourceID = $result['JobSourceID'];
                }
          }
            
          return $JobSourceID;  
                 
    }
    
    
    
    
    
     /**
     * Description
     * 
     * This method is used for to get users for given parameters.
     * @param string $NetworkID
     * @param string $ClientID
     * @param string $BranchID
     * 
     * @return array $result The generated array of users.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getUsers($NetworkID, $ClientID, $BranchID) {
        
          $result = array();
          
          if($NetworkID && $ClientID && $BranchID)
          {
                $sql = "SELECT UserID, CONCAT( ContactFirstName, ' ', ContactLastName ) AS FullName FROM user WHERE Status='Active' AND NetworkID=:NetworkID AND ClientID=:ClientID AND BranchID=:BranchID ORDER BY FullName" ;
              
              
                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute(array(':NetworkID' => $NetworkID, ':ClientID' => $ClientID, ':BranchID' => $BranchID));
                $result = $fetchQuery->fetchAll(PDO::FETCH_ASSOC);
               
          }
          else if($NetworkID && $ClientID)
          {
                $sql = "SELECT UserID, CONCAT( ContactFirstName, ' ', ContactLastName ) AS FullName FROM user WHERE Status='Active' AND NetworkID=:NetworkID AND ClientID=:ClientID ORDER BY FullName" ;
              
              
                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute(array(':NetworkID' => $NetworkID, ':ClientID' => $ClientID));
                $result = $fetchQuery->fetchAll(PDO::FETCH_ASSOC);
               
          }
          else if($NetworkID)
          {
                $sql = "SELECT UserID, CONCAT( ContactFirstName, ' ', ContactLastName ) AS FullName FROM user WHERE Status='Active' AND NetworkID=:NetworkID ORDER BY FullName" ;
              
              
                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute(array(':NetworkID' => $NetworkID));
                $result = $fetchQuery->fetchAll(PDO::FETCH_ASSOC);
               
          }
          
          
          //$this->controller->log(var_export($result, true));
          
          return $result;  
                 
    }
    
    
    
    
    
    
    /**
     * Description
     * 
     * This method is used for to get models matched for the given search string.
     * @param string $ModelNumberSearchStr
     * @param string $NetworkID
     * @param string $ManufacturerID
     * 
     * @return array $result The generated array of Model details.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getModelsManufacturer($ModelNumberSearchStr, $NetworkID, $ManufacturerID) {
        
          $result = array();
          
          if($NetworkID && $ManufacturerID)
          {
                $sql = "SELECT T1.ModelNumber, T1.ManufacturerID, T3.ManufacturerName FROM model AS T1 LEFT JOIN network_manufacturer AS T2 ON T1.ManufacturerID=T2.ManufacturerID AND T2.NetworkID=:NetworkID LEFT JOIN manufacturer AS T3 ON T1.ManufacturerID=T3.ManufacturerID WHERE T1.Status='Active' AND T2.Status='Active' AND T1.ManufacturerID=:ManufacturerID AND T1.ModelNumber LIKE :ModelNumber" ;
              
              
                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute(array(':ModelNumber' => "%".$ModelNumberSearchStr."%", ':NetworkID' => $NetworkID, ':ManufacturerID' => $ManufacturerID));
                
          }
          else if($NetworkID)
          {
                $sql = "SELECT T1.ModelNumber, T1.ManufacturerID, T3.ManufacturerName FROM model AS T1 LEFT JOIN network_manufacturer AS T2 ON T1.ManufacturerID=T2.ManufacturerID AND T2.NetworkID=:NetworkID LEFT JOIN manufacturer AS T3 ON T1.ManufacturerID=T3.ManufacturerID WHERE T1.Status='Active' AND T2.Status='Active' AND T1.ModelNumber LIKE :ModelNumber" ;
              
              
                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute(array(':ModelNumber' => "%".$ModelNumberSearchStr."%", ':NetworkID' => $NetworkID));
                
          }
          else if($ManufacturerID)
          {
                $sql = "SELECT T1.ModelNumber, T1.ManufacturerID, T3.ManufacturerName FROM model AS T1 LEFT JOIN manufacturer AS T3 ON T1.ManufacturerID=T3.ManufacturerID WHERE T1.Status='Active' AND T1.ManufacturerID=:ManufacturerID AND T1.ModelNumber LIKE :ModelNumber" ;
              
              
                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute(array(':ModelNumber' => "%".$ModelNumberSearchStr."%", ':ManufacturerID' => $ManufacturerID));
                
          }
          else if($ModelNumberSearchStr)
          {
              $sql = "SELECT T1.ModelNumber, T1.ManufacturerID, T3.ManufacturerName FROM model AS T1 LEFT JOIN manufacturer AS T3 ON T1.ManufacturerID=T3.ManufacturerID WHERE T1.Status='Active' AND T1.ModelNumber LIKE :ModelNumber" ;
              
              
              $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
              $fetchQuery->execute(array(':ModelNumber' => "%".$ModelNumberSearchStr."%"));
             

          }      
          
          while($row = $fetchQuery->fetch())
          {     
              if(is_array($row))
              {
                  $result[$row['ModelNumber']] = array($row['ManufacturerID'], $row['ManufacturerName']);
              }
              
          }
          
          return $result;  
                 
    } 
    
    
    
    /**
     * Description
     * 
     * This method is used to get the country name.
     * 
     * @param integer $CountryID
     * 
     * @return string $result 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getCountryName($CountryID) {
        
         $result = false;
         if($CountryID)
         {    
            $query = $this->conn->query( "SELECT CountryID, Name FROM country WHERE CountryID='".$CountryID."'" );

            
            
            $row = $query->fetch();
                    
            $result = isset($row['Name'])?$row['Name']:false;
           
         }   
         return $result;  
    }
    
    
    
    
    /**
     * Description
     * 
     * This method is used for, to get the list of counties for selected country and brand.
     * 
     * @param integer $CountryID
     * @param integer $BrandID
     * 
     * @return string $result 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getCounties($CountryID, $BrandID) {
        
         $result = false;
          
             
        $sql = "SELECT CountyID, Name FROM county WHERE BrandID=:BrandID AND CountryID=:CountryID AND Status=:Status";

        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':BrandID' => $BrandID, ':CountryID' => $CountryID, ':Status' => "Active"));

        while($row = $fetchQuery->fetch())
        {
            $result[] = array("CountyID"=>$row['CountyID'], "Name"=>$row['Name']);
        }
             
            
         return $result;  
    }
    
    
    
    
    
    
    
    
    
    
    public function getCountries(){
        $default_brand = $this->controller->user->DefaultBrandID;
        
        $skyline_countries = $this->collect('country', "BrandID = '1000' AND Status = 'Active'", '*', 'Name');

        $merge_array = array();
        
        if($default_brand != 1000){
            $brand_countries = $this->collect('country', "BrandID = '{$default_brand}' AND Status = 'Active'", '*', 'Name');
            
            foreach($skyline_countries as $country){
                $CountryCode = $country['CountryCode'];
                
                $replaceCountry = false;
                
                foreach($brand_countries as $bcountry){
                    if($bcountry['CountryCode'] == $country['CountryCode']){
                        $replaceCountry = true;
                    }
                }
                
                $merge_array[] = ($replaceCountry === false ? $country : $bcountry);
            }
        }
        return ($default_brand != 1000 ? $merge_array : $skyline_countries );
    }

    
    
    
    /*
    * 
    * This method finds service centre for given job details.
    * 
    * @param array $args It contains job details.
    *
    * 
    * @return int $ServiceCentreID
    * 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    ********************************************/
    public function findServiceCentre($args) {       
        
       /* if(isset($args['JobID']))
        {
         
            $jobTables  = "job AS T1 LEFT JOIN product AS T2 ON T1.ProductID=T2.ProductID LEFT JOIN unit_type AS T3 ON T2.UnitTypeID=T3.UnitTypeID LEFT JOIN repair_skill AS T4 ON T3.RepairSkillID=T4.RepairSkillID LEFT JOIN customer T5 ON T1.CustomerID=T5.CustomerID";
            $fieldNames = "T1.NetworkID, T5.CountryID, T1.ClientID, T1.ManufacturerID, T1.JobTypeID, T1.ServiceTypeID, T5.CountyID, T5.Town, T3.UnitTypeID, T4.RepairSkillID";
            
            $jobDetails =  $this->collect($jobTables, "JobID='".$args['JobID']."'", $fieldNames);
            
            $this->controller->log(var_export($jobDetails,true));
            
        }*/
       

	//Get branch third party service provider if it is set up
	if(isset($args["BranchID"]) && $args["BranchID"] != "" && $args["BranchID"] != null) {
	    $branchModel = $this->controller->loadModel("Branches");
	    $spID = $branchModel->getBranchThirdPartyServiceProvider($args["BranchID"]);
	    if($spID) {
		return $spID;
	    }
	}
        
        
        if((!isset($args['UnitTypeID']) || !$args['UnitTypeID']) && isset($args['ServiceBaseUnitType']) && $args['ServiceBaseUnitType']!='')
        {
             
            $UnitTypesModel = $this->controller->loadModel("UnitTypes");
            $utID = $UnitTypesModel->getId($args['ServiceBaseUnitType']);   
            
            if($utID)
            {    
                $args['UnitTypeID'] = $utID;
            }
        }   
        
	
        if(!isset($args['RepairSkillID']) && isset($args['UnitTypeID'])) 
        {
            $ut_sql = "SELECT RepairSkillID FROM unit_type WHERE Status='Active' AND UnitTypeID=:UnitTypeID";
            
            $utFetchQuery = $this->conn->prepare($ut_sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $utFetchQuery->execute(array(':UnitTypeID' => $args['UnitTypeID']));
            $ut_row = $utFetchQuery->fetch();
            $args['RepairSkillID'] = isset($ut_row['RepairSkillID'])?$ut_row['RepairSkillID']:'';
        }
         
       // $this->controller->log("findServiceCentre");
     //   $this->controller->log(var_export($args, true));
        
       
        $ServiceProviderID = 0; 
        
        //Checking for Central Service Allocation.
        if(isset($args['NetworkID']) && isset($args['CountryID']) && isset($args['RepairSkillID']))
        {
            
            $CentralServiceTopSet = array("ClientID", "ManufacturerID", "JobTypeID", "ServiceTypeID", "UnitTypeID", "CountyID");
            
            $CentralServiceRules = array(
                
                0=>array("ClientID", "ManufacturerID", "JobTypeID", "ServiceTypeID", "UnitTypeID", "CountyID"),
                1=>array("ClientID", "ManufacturerID", "JobTypeID", "ServiceTypeID", "UnitTypeID"),
                2=>array("ClientID", "ManufacturerID", "JobTypeID", "ServiceTypeID", "CountyID"),
                3=>array("ClientID", "ManufacturerID", "JobTypeID", "ServiceTypeID"),
                4=>array("ClientID", "ManufacturerID", "JobTypeID", "UnitTypeID", "CountyID"),
                5=>array("ClientID", "ManufacturerID", "JobTypeID", "UnitTypeID"),
                6=>array("ClientID", "ManufacturerID", "JobTypeID", "CountyID"),
                7=>array("ClientID", "ManufacturerID", "JobTypeID"),
                8=>array("ClientID", "ManufacturerID", "UnitTypeID", "CountyID"),
                9=>array("ClientID", "ManufacturerID", "UnitTypeID"),
                10=>array("ClientID", "ManufacturerID", "CountyID"),
                11=>array("ClientID", "ManufacturerID"),
                12=>array("ClientID", "JobTypeID", "ServiceTypeID", "UnitTypeID", "CountyID"),
                13=>array("ClientID", "JobTypeID", "ServiceTypeID", "UnitTypeID"),
                14=>array("ClientID", "JobTypeID", "ServiceTypeID", "CountyID"),
                15=>array("ClientID", "JobTypeID", "ServiceTypeID"),
                16=>array("ClientID", "JobTypeID", "UnitTypeID", "CountyID"),
                17=>array("ClientID", "JobTypeID", "UnitTypeID"),
                18=>array("ClientID", "JobTypeID", "CountyID"),
                19=>array("ClientID", "JobTypeID"),
                20=>array("ClientID", "UnitTypeID", "CountyID"),
                21=>array("ClientID", "UnitTypeID"),
                22=>array("ClientID", "CountyID"),
                23=>array("ClientID"),
                24=>array("ManufacturerID", "JobTypeID", "ServiceTypeID", "UnitTypeID", "CountyID"),
                25=>array("ManufacturerID", "JobTypeID", "ServiceTypeID", "UnitTypeID"),
                26=>array("ManufacturerID", "JobTypeID", "ServiceTypeID", "CountyID"),
                27=>array("ManufacturerID", "JobTypeID", "ServiceTypeID"),
                28=>array("ManufacturerID", "JobTypeID", "UnitTypeID", "CountyID"),
                29=>array("ManufacturerID", "JobTypeID", "UnitTypeID"),
                30=>array("ManufacturerID", "JobTypeID", "CountyID"),
                31=>array("ManufacturerID", "JobTypeID"),
                32=>array("ManufacturerID", "UnitTypeID", "CountyID"),
                33=>array("ManufacturerID", "UnitTypeID"),
                34=>array("ManufacturerID", "CountyID"),
                35=>array("ManufacturerID")
                
            );
            
            
            foreach($CentralServiceRules AS $csr)
            {
                
                if($ServiceProviderID)
                {
                    break;
                }
                
                
                $extraCondition    = "";
               // $queryArgs         = array();
                $defaultQueryArgs  = array(
                    
                    ":Status"=>"Active",
                    ":NetworkID"=>$args['NetworkID'],
                    ":CountryID"=>$args['CountryID'],
                    ":RepairSkillID"=>$args['RepairSkillID']
                    
                    );
                
                $queryArgs       = $defaultQueryArgs;
                
                foreach($CentralServiceTopSet as $topValue)
                {
                      
                     if (in_array($topValue, $csr)) 
                     {
                         if(isset($args[$topValue]) && $args[$topValue])
                         {    
                            $extraCondition .= " AND ".$topValue."=:".$topValue;  
                            $queryArgs[':'.$topValue] = $args[$topValue];
                         }
                         else
                         {
                             $extraCondition .= " AND ".$topValue." IS NULL";
                         }
                     }
                     else
                     {
                        $extraCondition .= " AND ".$topValue." IS NULL";
                        //$queryArgs[':'.$topValue] = 'NULL';
                     }
                }
                
                $sql ="	SELECT	ServiceProviderID 
			FROM	central_service_allocation 
			WHERE	Status=:Status AND 
				NetworkID=:NetworkID AND 
				CountryID=:CountryID AND 
				RepairSkillID=:RepairSkillID".$extraCondition ;

		//SSSS
                //$this->controller->log("Query on line " . __LINE__ . ":" . $sql);                 
		
                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute($queryArgs);
                
                $row = $fetchQuery->fetch();
                
                
                if(is_array($row))
                {
                    $ServiceProviderID = $row['ServiceProviderID'];
                }  
                
               //  $this->controller->log(var_export("Details1:", true));
              //   $this->controller->log(var_export($ServiceProviderID, true));
              //   $this->controller->log(var_export($sql, true));
              //   $this->controller->log(var_export($queryArgs, true));
                 
            }
            
        }
        
        
        //Check if collection address post code exists.
        
        
        
//        $PostCodeSplit = explode(" ", isset($args['ColAddPostcode'])?$args['ColAddPostcode']:'');
//        $PostCodeArea  = (isset($PostCodeSplit[0]) && $PostCodeSplit[0]!='')?$PostCodeSplit[0]:false;
//        
//        if(!$PostCodeArea)
//        {    
//            $PostCodeSplit = explode(" ", isset($args['PostalCode'])?$args['PostalCode']:'');
//            $PostCodeArea  = (isset($PostCodeSplit[0]) && $PostCodeSplit[0]!='')?$PostCodeSplit[0]:false;
//        
//        }
//        
        
        
        $PostCodeArea   = (isset($args['ColAddPostcode']) && $args['ColAddPostcode'])?trim(substr($args['ColAddPostcode'], 0, -3)):false;
                            
        if(!$PostCodeArea)
        {    
            $PostCodeArea   = (isset($args['PostalCode']) && $args['PostalCode'])?trim(substr($args['PostalCode'], 0, -3)):false;
        }
        
        
        
        
        
        //Checking for Postcode Allocation.
        if(!$ServiceProviderID && isset($args['NetworkID']) && $PostCodeArea && isset($args['RepairSkillID']))
        {
            
          
            $PostcodeAllocationTopSet = array("ManufacturerID", "JobTypeID", "ServiceTypeID", "ClientID");
            
            $PostcodeAllocationRules = array(
                
                0=>array("ManufacturerID", "JobTypeID", "ServiceTypeID", "ClientID"),
                1=>array("ManufacturerID", "JobTypeID", "ServiceTypeID"),
                2=>array("ManufacturerID", "JobTypeID", "ClientID"),
                3=>array("ManufacturerID", "JobTypeID"),
                4=>array("ManufacturerID", "ClientID"),
                5=>array("ManufacturerID"),
                6=>array("JobTypeID", "ServiceTypeID", "ClientID"),
                7=>array("JobTypeID", "ClientID"),
                8=>array("JobTypeID"),
                9=>array("ClientID"),
                10=>array()
                
            );
            
            
            foreach($PostcodeAllocationRules AS $par)
            {
                
                if($ServiceProviderID)
                {
                    break;
                }
                
                
                $extraCondition    = "";
                $queryArgs         = array();
                $defaultQueryArgs  = array(
                    
                    ":Status"=>"Active",
                    ":NetworkID"=>$args['NetworkID'],
                    ":PostCodeArea"=>$PostCodeArea,
                    ":RepairSkillID"=>$args['RepairSkillID']
                    
                    );
                
                $queryArgs    = $defaultQueryArgs;
                
                
                foreach($PostcodeAllocationTopSet as $topValue)
                {
                    
                    if (in_array($topValue, $par)) 
                    {
                        if(isset($args[$topValue]) && $args[$topValue])
                        {    
                           $extraCondition .= " AND T1.".$topValue."=:".$topValue;  
                           $queryArgs[':'.$topValue] = $args[$topValue];
                        }
                        else
                        {
                            $extraCondition .= " AND T1.".$topValue." IS NULL";
                        }
                    }
                    else
                    {
                       $extraCondition .= " AND T1.".$topValue." IS NULL";
                       //$queryArgs[':'.$topValue] = 'NULL';
                    }
                    
                }    
                
                
//                foreach($par AS $parKey=>$parValue)
//                {
//                    $extraCondition .= " AND T1.".$parValue."=:".$parValue;
//                    $queryArgs[':'.$parValue] = isset($args[$parValue])?$args[$parValue]:'0';
//                }   
//                
                
                
                
                $sql ="	SELECT	    T1.ServiceProviderID 
			FROM	    postcode_allocation AS T1 
			LEFT JOIN   postcode_allocation_data AS T2 ON T1.PostcodeAllocationID=T2.PostcodeAllocationID  
			WHERE	    T1.Status=:Status AND 
				    T2.PostCodeArea=:PostCodeArea AND 
				    T1.NetworkID=:NetworkID AND 
				    T1.RepairSkillID=:RepairSkillID".$extraCondition ;

		//SSSS
                //$this->controller->log("Query on line " . __LINE__ . ":" . $sql);                 
                 
                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute($queryArgs);
                
                $row = $fetchQuery->fetch();
                
                
                if(is_array($row))
                {
                    $ServiceProviderID = $row['ServiceProviderID'];
                }    
                
//                  $this->controller->log(var_export("Details2:", true));
//                  $this->controller->log(var_export($ServiceProviderID, true));
//                  $this->controller->log(var_export($sql, true));
//                  $this->controller->log(var_export($row, true));
//                  $this->controller->log(var_export($queryArgs, true));
//                  $this->controller->log(var_export($args, true));
                  
                    
            }
            
           
        }
        
        
         //Checking for Town Allocation.
        if(!$ServiceProviderID && isset($args['NetworkID']) && isset($args['CountryID']) && isset($args['ManufacturerID']) && isset($args['CountyID']))
        {
            
            $TownAllocationTopSet = array("Town", "RepairSkillID", "ClientID", "JobTypeID", "ServiceTypeID");
          
            $TownAllocationRules = array(
                
                0=>array("Town", "RepairSkillID", "ClientID", "JobTypeID", "ServiceTypeID"),
                1=>array("Town", "RepairSkillID", "ClientID", "JobTypeID"),
                2=>array("Town", "RepairSkillID", "ClientID"),
                3=>array("Town", "ClientID", "JobTypeID", "ServiceTypeID"),
                4=>array("Town", "ClientID", "JobTypeID"),
                5=>array("Town", "ClientID"),
                6=>array("RepairSkillID", "ClientID", "JobTypeID", "ServiceTypeID"),
                7=>array("RepairSkillID", "ClientID", "JobTypeID"),
                8=>array("RepairSkillID", "ClientID"),
                9=>array("ClientID", "JobTypeID", "ServiceTypeID"),
                10=>array("ClientID", "JobTypeID"),
                11=>array("ClientID"),
                12=>array("Town", "RepairSkillID", "JobTypeID", "ServiceTypeID"),
                13=>array("Town", "RepairSkillID", "JobTypeID"),
                14=>array("Town", "RepairSkillID"),
                15=>array("Town", "JobTypeID", "ServiceTypeID"),
                16=>array("Town", "JobTypeID"),
                17=>array("Town"),
                18=>array("RepairSkillID", "JobTypeID", "ServiceTypeID"),
                19=>array("RepairSkillID", "JobTypeID"),
                20=>array("RepairSkillID"),
                21=>array("JobTypeID", "ServiceTypeID"),
                22=>array("JobTypeID"),
                23=>array()
                
              
            );
            
            
            foreach($TownAllocationRules AS $tar)
            {
                
                if($ServiceProviderID)
                {
                    break;
                }
                
                
                $extraCondition    = "";
                $queryArgs         = array();
                $defaultQueryArgs  = array(
                    
                    ":Status"=>"Active",
                    ":NetworkID"=>$args['NetworkID'],
                    ":CountryID"=>$args['CountryID'],
                    ":ManufacturerID"=>$args['ManufacturerID'],
                    ":CountyID"=>$args['CountyID']
                    
                    );
                
                $queryArgs    = $defaultQueryArgs;
                
                
                
                
                foreach($TownAllocationTopSet as $topValue)
                {
                    
                    if (in_array($topValue, $tar)) 
                    {
                        
                        $topValueIndex = $topValue;
                       
                        if($topValueIndex=="Town")
                        {
                            if(isset($args['City']))
                            {
                                $topValueIndex = 'City';
                            } 
                            else if(isset($args['TownCity']))
                            {
                                $topValueIndex = 'TownCity';
                            }    
                            
                        }    
                        
                        
                        if(isset($args[$topValueIndex]) && $args[$topValueIndex])
                        {    
                           $extraCondition .= " AND ".$topValue."=:".$topValue;  
                           
                           $queryArgs[':'.$topValue] = $args[$topValueIndex];
                        }
                        else
                        {
                            $extraCondition .= " AND ".$topValue." IS NULL";
                        }
                    }
                    else
                    {
                       $extraCondition .= " AND ".$topValue." IS NULL";
                       //$queryArgs[':'.$topValue] = 'NULL';
                    }
                    
                } 
                
                
                
//                foreach($tar AS $tarKey=>$tarValue)
//                {
//                    $extraCondition .= " AND ".$tarValue."=:".$tarValue;
//                    $queryArgs[':'.$tarValue] = isset($args[$tarValue])?$args[$tarValue]:'0';
//                }
                
                
                
                $sql ="	SELECT	ServiceProviderID 
			FROM	town_allocation 
			WHERE	Status=:Status AND 
				NetworkID=:NetworkID AND 
				CountryID=:CountryID AND 
				ManufacturerID=:ManufacturerID AND 
				CountyID=:CountyID".$extraCondition ;

		//SSSS
                //$this->controller->log("Query on line " . __LINE__ . ":" . $sql);                 
                
                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute($queryArgs);
                
                $row = $fetchQuery->fetch();
                
                
                if(is_array($row))
                {
                    $ServiceProviderID = $row['ServiceProviderID'];
                    
                  
                    
                } 
                
//                  $this->controller->log(var_export("Details3:", true));
//                     $this->controller->log(var_export($ServiceProviderID, true));
//                     $this->controller->log(var_export($sql, true));
            }
            
           
        }
        
        return $ServiceProviderID;          
        
    }
    
    
     /**
     * 
     * Bought Out Guarantee.
     * 
     * @param array $args It contains job details.
     *
     * 
     * @return int $ServiceTypeID
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     ********************************************/
     public function boughtOutGuarantee($args){  
         $ServiceTypeID = NULL;
         if(isset($args['ServiceTypeID']))
         {
             
            //Getting Repair skill id from unit type if its unset.
            if(!isset($args['RepairSkillID']) && isset($args['UnitTypeID'])) 
            {
                $ut_sql = "SELECT RepairSkillID FROM unit_type WHERE Status='Active' AND UnitTypeID=:UnitTypeID";

                $utFetchQuery = $this->conn->prepare($ut_sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $utFetchQuery->execute(array(':UnitTypeID' => $args['UnitTypeID']));
                $ut_row = $utFetchQuery->fetch();
                $args['RepairSkillID'] = isset($ut_row['RepairSkillID'])?$ut_row['RepairSkillID']:'';
            } 
             
             
            $ServiceTypeID = $args['ServiceTypeID'];
            
            //Checking whether service type is Warranty or not
            $sql = "SELECT ServiceTypeID, BrandID FROM service_type WHERE Status=:Status AND ServiceTypeID=:ServiceTypeID AND Code=:Code";
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(":Status"=>"Active", ":ServiceTypeID"=>$ServiceTypeID, ":Code"=>'W'));

            $row = $fetchQuery->fetch();
            
            //$this->controller->log(var_export($row, true));
            
            if(is_array($row) && $row['ServiceTypeID'])
            {
                //Checking in bought out guarantee table.
                if(isset($args['NetworkID']) && isset($args['ClientID']) && isset($args['ManufacturerID']))
                {


                    $BOGRules = array(

                        0=>array("RepairSkillID", "UnitTypeID", "ModelID"),
                        1=>array("RepairSkillID", "UnitTypeID"),
                        2=>array("RepairSkillID", "ModelID"),
                        3=>array("RepairSkillID"),
                        4=>array("UnitTypeID", "ModelID"),
                        5=>array("UnitTypeID"),
                        6=>array("ModelID")
                        
                    );


                    $ruleFound = 0;
                    foreach($BOGRules AS $bog)
                    {

                        if($ruleFound)
                        {
                            break;
                        }


                        $extraCondition    = "";
                        $queryArgs         = array();
                        $defaultQueryArgs  = array(

                            ":Status"=>"Active",
                            ":NetworkID"=>$args['NetworkID'],
                            ":ClientID"=>$args['ClientID'],
                            ":ManufacturerID"=>$args['ManufacturerID']

                            );

                        $queryArgs    = $defaultQueryArgs;
                        foreach($bog AS $bogKey=>$bogValue)
                        {
                            $extraCondition .= " AND ".$bogValue."=:".$bogValue;
                            $queryArgs[':'.$bogValue] = isset($args[$bogValue])?$args[$bogValue]:'0';
                        }   

                        $sql = "SELECT BoughtOutGuaranteeID FROM bought_out_guarantee WHERE Status=:Status AND NetworkID=:NetworkID AND ClientID=:ClientID AND ManufacturerID=:ManufacturerID".$extraCondition ;


                        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                        $fetchQuery->execute($queryArgs);

                        $bRow = $fetchQuery->fetch();


                        if(is_array($bRow))
                        {
                            $ruleFound = $bRow['BoughtOutGuaranteeID'];
                        }    
                    }
                    
                    //If rule was found, then we are changing service type from Warranty to Trade
                    if($ruleFound)
                    {
                        $sql        = "SELECT ServiceTypeID, BrandID FROM service_type WHERE Status=:Status AND (BrandID=:BrandID OR BrandID=:DefaultBrandID ) AND Code=:Code ORDER BY BrandID DESC LIMIT 0,1";
                        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                        $fetchQuery->execute(array(":Status"=>"Active", ":BrandID"=>$row['BrandID'], ":DefaultBrandID"=>$this->SkylineBrandID, ":Code"=>'T'));

                        $stRow = $fetchQuery->fetch();
                        
                        if(is_array($stRow))
                        {
                            $ServiceTypeID = $stRow['ServiceTypeID'];
                        } 
                    }    


                }//End of checking Network, client and manufcaturer ids are provided.
               
            }   //End of checking service type exists in the database. 
          
         }
         return $ServiceTypeID;
     }
    
    
    
     
     
     
      /**
     * 
     * Appointment details for given job.
     * 
     * @param int $JobID 
     * @param date $CompletedDate  Job completeion date, default null
     * 
     * @return array it returns details as array.
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     ********************************************/
     public function getAppointmentDetails($JobID, $CompletedDate=null){  
         
        /* Execute a prepared statement by passing an array of values */
        if($CompletedDate)//Getting appointment date after job is completed.
        {
            $sql = 'SELECT AppointmentID, UserID, AppointmentDate, AppointmentTime FROM appointment WHERE JobID=:JobID AND AppointmentDate>=:AppointmentDate AND OutCardLeft=:OutCardLeft ORDER BY AppointmentID DESC LIMIT 0,1';
            $params = array('JobID' => $JobID, 'OutCardLeft' => 0, 'AppointmentDate' => $CompletedDate);
            
        }   
        else//Getting appointment date before job is completed.
        {
            $sql = 'SELECT AppointmentID, UserID, AppointmentDate, AppointmentTime FROM appointment WHERE JobID=:JobID AND OutCardLeft=:OutCardLeft ORDER BY AppointmentID DESC LIMIT 0,1';
            $params = array('JobID' => $JobID, 'OutCardLeft' => 0);
           
        }
         
        $result = $this->Query($this->conn, $sql, $params);
        
        if (count($result) == 0)
            return array();
        
        return $result[0];
        
        
     }
     
     
     
    

    /**
     * Get a field from a table based on another field. This is intended to 
     * allow to be quickly found by the API when presented with a field by a
     * third party system that passes the field content rather than the ID (as
     * theird party systems will not know the SkyLine ID of anything other than
     * a job)
     * 
     * @param array $args         The status for which we wish to find the id
     *              => 'want'     The name of the field we want from the database
     *              => 'got'      The name of the field we have   
     *              => 'from'     The name of the table in the database we wish to serach
     *        string $is         The value of the field we have
     * 
     * @return getFieldFromTable  The value we wished to find on null if no match
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     */
    public function getFieldFromTable($args = array(), $is = null) {
    
        $want = isset($args['want']) ? $args['want'] : null;
        $got = isset($args['got']) ? $args['got'] : null;
        $from = isset($args['from']) ? $args['from'] : null;
        
        $sql = "
                SELECT
			`$want`
		FROM
			`$from`
		WHERE
			`$got` = '$is'
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0][$want]);                                          /* Requested item exists */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    /**
     * ukPostCodeFormat
     * 
     * Formaty a UK PostCode. (UK Postcodes are of various lengths but the last
     * thee characters are always a number and two digits proceeded by a space.
     * Examples :- G77 6UA, CV1 5FB, B1 3SD, W1A 1AA, PL21 0JA
     * 
     * @param string $pc    Post Code     
     * 
     * @return string   Post Code
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function ukPostCodeFormat($pc) {
        $retVal = $pc;
        if (strlen($pc) > 5) {
            if (substr($pc, -4, 1) != ' ' ) {
                $retVal = substr($pc, 0, strlen($pc)-3).' '.substr($pc, -3);
            } /* fi substr */
        } /* fi strlen */
        return(strtoupper($retVal));
    }
    
    
    
    //returns excel-like column names
    //Eg. 0 - A, 27 - AB
    
    public function getNameFromNumber($num) {
	$numeric = $num % 26;
	$letter = chr(65 + $numeric);
	$num2 = intval($num / 26);
	if ($num2 > 0) {
	    return getNameFromNumber($num2 - 1) . $letter;
	} else {
	    return $letter;
	}
    }
    
    function rangeWeek($datestr) {
    $dt = strtotime($datestr);
    $res['start'] = date('N', $dt)==1 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('last monday', $dt));
    $res['end'] = date('N', $dt)==7 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('next sunday', $dt));
    return $res;
    }  
     
    
    public function getJobChargeType($jobID){
       $sql="select Warranty as w,Chargeable as c from job j
        join service_type st on st.ServiceTypeID=j.ServiceTypeID   
        where JobID=$jobID"; 
       $res=$this->Query($this->conn, $sql);
       if($res[0]['w']=="Y"&&$res[0]['c']=="Y"){
           return "m";//mixed invoice
       }elseif($res[0]['w']=="Y"&&$res[0]['c']!="Y"){
           return "w";//warranty
       }else{
           return "c";//chargeable
       }
    } 
    
    
}
?>
