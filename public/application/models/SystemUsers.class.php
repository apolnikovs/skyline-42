<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of System Users Page in Organisation Setup section under System Admin
 *
 * @author Simon Tsang <s.tsang@pccsuk.com>
 * @version     1.01A
 *   
 * Changes
 * Date        Version Author                Reason
 * ??/??/????  1.00    Simon Tsang           Initial Version
 * 12/07/2012  1.01    Andrew J. Williams    Fixed Issue 13 - The company filter is not working properly in organisation setup/system users/
 * 12/07/2012  1.01A   Andrew J. Williams    Fixed Issue 4 - When we add new branch user, its not inserting network and client ids in the database
 ******************************************************************************/

class SystemUsers extends CustomModel {
    
    #public $debug = true;
    private $conn;
    private $table     = "user AS t1 LEFT JOIN network AS t2 ON t1.NetworkID = t2.NetworkID
                                     LEFT JOIN service_provider AS t3 ON t1.ServiceProviderID = t3.ServiceProviderID
                                     LEFT JOIN client AS t4 ON t1.ClientID = t4.ClientID
                                     LEFT JOIN branch AS t5 ON t1.BranchID = t5.BranchID
                                     LEFT JOIN manufacturer AS t6 ON t1.ManufacturerID = t6.ManufacturerID
                                     LEFT JOIN extended_warrantor AS t7 ON t1.ExtendedWarrantorID = t7.ExtendedWarrantorID";
    
      
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
    
  
   
    
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->tables
     * @return array 
     * 
     * @author Simon Tsang <s.tsang@pccsuk.com>
     * 
     * Changes to fix Issue 13 - Allow filtering by Network, Client or Branch
     * @author Andrew Williams <Andrew.Williams@awcomputech.com>
     **************************************************************************/  
    
    public function fetch($args) {
        
       
        /*
        
        if (( isset($args['StockCodeSearchStr']) && $args['StockCodeSearchStr'] != '')  || 
                (isset($args['ModelNoSearchStr']) && $args['ModelNoSearchStr'] != '' )) {
           
            $args['where'] = '(';
            
            if ( $args['StockCodeSearchStr'] != '') {
                
                for ( $i=0 ; $i<count($columns) ; $i++ ) {
		$args['where'] .= $columns[$i].' LIKE '.$this->conn->quote( '%'.$args['StockCodeSearchStr'].'%' ).' OR ';
                }
                
            }
            else
            {
                for ( $i=0 ; $i<count($columns) ; $i++ ) {
		$args['where'] .= $columns[$i].' LIKE '.$this->conn->quote( '%'.$args['ModelNoSearchStr'].'%' ).' OR ';
                }
            }
            
            
            $args['where'] = substr_replace( $args['where'], "", -3 );
            $args['where'] .= ')';
	}
        
        */
        
        $NetworkID = isset($args['firstArg'])?$args['firstArg']:'';
        $ClientID  = isset($args['secondArg'])?$args['secondArg']:'';
        $BranchID  = isset($args['thirdArg'])?$args['thirdArg']:'';
        
        if($NetworkID!='' || $ClientID!='' || $BranchID!='')
        {
            $sep = '';
            if($NetworkID!='')
            {
                $args['where'] = "t1.NetworkID='".$NetworkID."'";
                $sep = ' AND ';
            }
            
            if($ClientID!='')
            {
                $args['where'] .= $sep."t1.ClientID='".$ClientID."'";
                $sep = ' AND ';
            }
            
            if($BranchID!='')
            {
                $args['where'] .= $sep."t1.BranchID='".$BranchID."'";
            }
        }
        
        $col = array('t1.UserID', 'CONCAT_WS(" ", t1.ContactFirstName, t1.ContactLastName)', 't1.Username', 'CONCAT_WS(" ", t2.CompanyName, t3.CompanyName, t4.ClientName, t5.BranchName,t6.ManufacturerName, t7.ExtendedWarrantorName)', 't1.BranchJobSearchScope', array('t1.UserID', "( SELECT GROUP_CONCAT(r.Name SEPARATOR '; ') AS AccessRights FROM user_role AS ur LEFT JOIN role AS r ON ur.RoleID=r.RoleID WHERE ur.UserID=t1.UserID GROUP BY ur.UserID )"), 't1.Status');
        $output = $this->ServeDataTables($this->conn, $this->table, $col, $args);
        
        #$this->controller->log('System user ->fetch' . var_export($output, true));
       
        return  $output;
        
    }
    
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
     * @return array It contains status and message.
     * @author Simon Tsang <s.tsang@pccsuk.com>
     */   
     public function processData($args) {
         #$this->controller->log('SystemUser->processData :'.var_export($args, true));

         if(!isset($args['UserID']) || !$args['UserID']) {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
    /*
     * 
     */
    public function isPasswordChanged($userID, $password){
        $sql = "SELECT Password FROM user WHERE UserID={$userID} LIMIT 0,1";

        $query = $this->conn->query( $sql );

        $data = $query->fetch(); 
        
        return ($data['Password'] == $password || $data['Password'] == md5($password) ? false : true);
    }  
    
    /**
     * Description
     * 
     * This method is used for to validate name.
     *
     * @param interger $CompanyName  
     * @param interger $NetworkID.
     * @global $this->table
     * 
     * @return boolean.
     * @author Simon Tsang <s.tsang@pccsuk.com>
     */ 
    public function isValid($Username, $ContactEmail, $UserID=null) {

         /* Execute a prepared statement by passing an array of values */
        $userSQL = ($UserID!=null ? "UserID != {$UserID}" : 'UserID IS NOT NULL');

        $sql = "SELECT UserID, Username, ContactEmail FROM user WHERE {$userSQL} AND Username='{$Username}'";

        $query = $this->conn->query( $sql );
        $query->setFetchMode( PDO::FETCH_ASSOC ); #FETCH_NUM   

        $result = $query->fetch();            

        #$this->controller->log('SystemeUser->isValid :'.var_export($result, true));

        
        if(is_array($result) && $result['UserID'])
            return false;
        else
            return true;
    
    }
    
    
    /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args  
     * @global $this->table 
     * @return array It contains status of operation and message.
     * @author Simon Tsang <s.tsang@pccsuk.com>
     * 
     * 
     * 
     */ 
    public function create($args) {
        
        if($this->isValid($args['Username'], $args['ContactEmail'])) {
                
              //  $user_type = array( 'Network', 'ServiceProvider', 'Client', 'Branch' );  
            
                $fields = array(
                    'Username' => $args['Username'], 
                    'CustomerTitleID' => (isset($args['CustomerTitleID']) && $args['CustomerTitleID']!='')?$args['CustomerTitleID']:NULL, 
                    'ContactFirstName' => $args['ContactFirstName'],
                    'ContactLastName' => (isset($args['ContactLastName']) && $args['ContactLastName']!='')?$args['ContactLastName']:NULL,
                    'ContactEmail' => (isset($args['ContactEmail']) && $args['ContactEmail']!='')?$args['ContactEmail']:NULL,
                    'ContactMobile' => $args['ContactMobile'],
                    'SecurityQuestionID' => $args['SecurityQuestionID'], 
                    'Answer' => $args['Answer'], 
                    'DefaultBrandID' => $args['DefaultBrandID'],
                    'Status' => $args['Status']
                 );             
            
            
            
                $fields['Password'] = $this->encrypt($args['Password']);
                $fields['CreatedDate'] = date('Y-m-d H:i:s'); 
            
                 
                if($args['UserType']=="Network") 
                {
                    $fields["NetworkID"] = $args['CompanyID'];
                    $fields["ServiceProviderID"] = null;
                    $fields["ClientID"] = null;
                    $fields["BranchID"] = null;
                    $fields["ManufacturerID"] = null;
                    $fields["ExtendedWarrantorID"] = null;
                } 
                else if($args['UserType']=="ServiceProvider") 
                {
                    $fields["NetworkID"] = null;
                    $fields["ServiceProviderID"] = $args['CompanyID'];
                    $fields["ClientID"] = null;
                    $fields["BranchID"] = null;
                    $fields["ManufacturerID"] = null;
                    $fields["ExtendedWarrantorID"] = null;
                }  
                else if($args['UserType']=="Client") 
                {
                    $clientModel = $this->controller->loadModel("Clients");
                    
                    $clientDetails = $clientModel->fetchRow(array("ClientID"=>$args['CompanyID']));
                    
                    $fields["NetworkID"] = isset($clientDetails['NetworkID'])?$clientDetails['NetworkID']:null;
                    $fields["ServiceProviderID"] = null;
                    $fields["ClientID"] = $args['CompanyID'];
                    $fields["BranchID"] = null;
                    $fields["ManufacturerID"] = null;
                    $fields["ExtendedWarrantorID"] = null;
                }
                else if($args['UserType']=="Branch") 
                {
                    $branchModel   = $this->controller->loadModel("Branches");
                    
                    $branchDetails = $branchModel->fetchRow(array("BranchID"=>$args['CompanyID']));
                    
                    $fields["NetworkID"] = isset($branchDetails['NetworkID'])?$branchDetails['NetworkID']:null;
                    $fields["ServiceProviderID"] = null;
                    $fields["ClientID"] = isset($branchDetails['ClientID'])?$branchDetails['ClientID']:null;
                    $fields["BranchID"] = $args['CompanyID'];
                    $fields["ManufacturerID"] = null;
                    $fields["ExtendedWarrantorID"] = null;
                } 
                else if($args['UserType']=="Manufacturer") 
                {
                     
                    $fields["NetworkID"] = null;
                    $fields["ServiceProviderID"] = null;
                    $fields["ClientID"] = null;
                    $fields["BranchID"] = null;
                    $fields["ManufacturerID"] = $args['CompanyID'];
                    $fields["ExtendedWarrantorID"] = null;
                }
                else if($args['UserType']=="ExtendedWarrantor") 
                {
                     
                    $fields["NetworkID"] = null;
                    $fields["ServiceProviderID"] = null;
                    $fields["ClientID"] = null;
                    $fields["BranchID"] = null;
                    $fields["ManufacturerID"] = null;
                    $fields["ExtendedWarrantorID"] = $args['CompanyID'];
                }
                
          
            
            
            /********
            foreach($user_type as $type)
            {
                
                if($type == $args['UserType']) 
                {
                    $fields["{$type}ID"] = $args['CompanyID'];
                } 
                else 
                {
                    
                    if ( !isset($args["{$type}ID"]) ) 
                    {
                        $fields["{$type}ID"] = null;
                    } 
                    else 
                    {
                        $fields["{$type}ID"] = $args["{$type}ID"];
                    } 
                    
                } 
                
            } 
            ********/
            
            
            
                        
            $this->controller->log('SystemUser->create : ' . var_export($fields, true));
            
           // $UserID = TableFactory::Modify( $this->conn, 'User', 'insert', $fields );
            $user_table = TableFactory::User();
            $cmd = $user_table->insertCommand( $fields );
            if ($this->Execute($this->conn, $cmd, $fields)) {
                $UserID = $this->conn->lastInsertId();
            } else {
                $this->controller->log('Error Creating User Record; '.$cmd);
                //$this->controller->log(var_export($fields, true));
                //$this->controller->log($this->lastPDOError()); 
                throw new Exception('Error Creating User Record');
            }         
            
            foreach($args as $key => $value){
                if(strstr($key, '_')){
                    list($fld, $val) = explode('_',$key);
                    if($fld == 'RoleID'){
                        //TableFactory::Modify( $this->conn, 'UserRole', 'insert', array(':RoleID' => $val, ':UserID' => $UserID) );
                        
                        $user_role_table = TableFactory::UserRole();
                        $role_args = array('RoleID' => $val, 'UserID' => $UserID);
                        $cmd = $user_role_table->insertCommand( $role_args );
                        if (!$this->Execute($this->conn, $cmd, $role_args)) {
                            $this->controller->log('Error Creating User Role Record: '.$cmd);
                            //$this->controller->log(var_export($role_args, true));
                            //$this->controller->log($this->lastPDOError()); 
                        }
            
                    }
                }
                
            }
            
            #$this->controller->log('SystemUser->create : ' . $UserID);
       
            #$insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));

            
//            $insertQuery->execute(array(
//                'Username' => 'Username',
//                'Password' => 'Password',
//                'CustomerTitleID' => '1',
//                'ContactFirstName' => '',
//                'ContactLastName' => '',
//                'ContactEmail' => 'Test@libero.es',
//                'ContactMobile' => '',
//                'SecurityQuestionID' => '',
//                'Answer' => '',
//                'UserType' => '',
//                'CompanyID' => '',
//                'CompanyID_temp' => '',
//                'DefaultBrandID' => '',
//                'DefaultBrandID_temp' => '',
//                'UserID' => ''
//                ));
        
        
              return array('status' => 'OK',
                        'message' => $this->controller->page['Text']['data_inserted_msg']);
        } else {
            
            return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    
    /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @global $this->table  
     * @return array It contains row of the given primary key.
     * @author Simon Tsang <s.tsang@pccsuk.com>
     */ 
     public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT t1.* FROM '.$this->table.' WHERE UserID=:UserID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':UserID' => $args['UserID']));
        $result = $fetchQuery->fetch();
        
        #$this->controller->log(var_export($result, true));
        
        return $result;
    }
    
    
     /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
     * @global $this->table   
     * @return array It contains status of operation and message.
     * @author Simon Tsang <s.tsang@pccsuk.com>
     */ 
    public function update($args) {
       
        $this->controller->log('systemUser->update : '.var_export($args, true));
        
        
        $user_type = array( 'Network', 'ServiceProvider', 'Client', 'Branch', 'Manufacturer', 'ExtendedWarrantor' );  
        
        if($args['SuperAdminFlag']=="1" && isset($args['SADefaultBrandID']))
        {
            $args['DefaultBrandID'] = $args['SADefaultBrandID'];
        } 
        
        if( $args['DefaultBrandID'] == '' ){
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1028, 'default', $this->controller->lang));            
        }else if($this->isValid($args['Username'], $args['ContactEmail'], $args['UserID'])) {        
            $sql_fields = '';
            
               
            
            $fields = array(
                ':UserID' => $args['UserID'], 
                ':Username' => $args['Username'], 
                ':CustomerTitleID' => (isset($args['CustomerTitleID']) && $args['CustomerTitleID']!='')?$args['CustomerTitleID']:NULL, 
                ':ContactFirstName' => $args['ContactFirstName'],
                ':ContactLastName' => (isset($args['ContactLastName']) && $args['ContactLastName']!='')?$args['ContactLastName']:NULL,
                ':ContactEmail' => (isset($args['ContactEmail']) && $args['ContactEmail']!='')?$args['ContactEmail']:NULL,
                ':ContactMobile' => $args['ContactMobile'],
                ':SecurityQuestionID' => $args['SecurityQuestionID'], 
                ':Answer' => $args['Answer'], 
                ':DefaultBrandID' => $args['DefaultBrandID'],
                ':Status' => $args['Status']
             );             
            
            #The password that is loaded is in MD5 format
            if($this->isPasswordChanged($args['UserID'], $args['Password'])){
                $fields[':Password'] = $this->encrypt($args['Password']);
                $sql_fields .= " Password=:Password, ";
            }
               
            
            if($args['SuperAdminFlag']!="1")
            {
                foreach($user_type as $type)
                {
                       $sql_fields .= "{$type}ID = :{$type}ID, ";
                }
                
                if($args['UserType']=="Network") 
                {
                    $fields["NetworkID"] = $args['CompanyID'];
                    $fields["ServiceProviderID"] = null;
                    $fields["ClientID"] = null;
                    $fields["BranchID"] = null;
                    $fields["ManufacturerID"] = null;
                    $fields["ExtendedWarrantorID"] = null;
                } 
                else if($args['UserType']=="ServiceProvider") 
                {
                    $fields["NetworkID"] = null;
                    $fields["ServiceProviderID"] = $args['CompanyID'];
                    $fields["ClientID"] = null;
                    $fields["BranchID"] = null;
                    $fields["ManufacturerID"] = null;
                    $fields["ExtendedWarrantorID"] = null;
                }  
                else if($args['UserType']=="Client") 
                {
                    $clientModel = $this->controller->loadModel("Clients");
                    
                    $clientDetails = $clientModel->fetchRow(array("ClientID"=>$args['CompanyID']));
                    
                    $fields["NetworkID"] = isset($clientDetails['NetworkID'])?$clientDetails['NetworkID']:null;
                    $fields["ServiceProviderID"] = null;
                    $fields["ClientID"] = $args['CompanyID'];
                    $fields["BranchID"] = null;
                    $fields["ManufacturerID"] = null;
                    $fields["ExtendedWarrantorID"] = null;
                }
                else if($args['UserType']=="Branch") 
                {
                    $branchModel   = $this->controller->loadModel("Branches");
                    
                    $branchDetails = $branchModel->fetchRow(array("BranchID"=>$args['CompanyID']));
                    
                    $fields["NetworkID"] = isset($branchDetails['NetworkID'])?$branchDetails['NetworkID']:null;
                    $fields["ServiceProviderID"] = null;
                    $fields["ClientID"] = isset($branchDetails['ClientID'])?$branchDetails['ClientID']:null;
                    $fields["BranchID"] = $args['CompanyID'];
                    $fields["ManufacturerID"] = null;
                    $fields["ExtendedWarrantorID"] = null;
                } 
                else if($args['UserType']=="Manufacturer") 
                {
                    $fields["NetworkID"] = null;
                    $fields["ServiceProviderID"] = null;
                    $fields["ClientID"] = null;
                    $fields["BranchID"] = null;
                    $fields["ManufacturerID"] = $args['CompanyID'];
                    $fields["ExtendedWarrantorID"] = null;
                } 
                else if($args['UserType']=="ExtendedWarrantor") 
                {
                    $fields["NetworkID"] = null;
                    $fields["ServiceProviderID"] = null;
                    $fields["ClientID"] = null;
                    $fields["BranchID"] = null;
                    $fields["ManufacturerID"] = null;
                    $fields["ExtendedWarrantorID"] = $args['CompanyID'];
                }   
                
                
                
                
                
                /************
                 foreach($user_type as $type)
                 {
                        $sql_fields .= "{$type}ID = :{$type}ID, ";
                         
                        if($type == $args['UserType']) 
                        {
                            $fields[":{$type}ID"] = $args['CompanyID'];
                        } 
                        else 
                        {
                            if ( !isset($args["{$type}ID"]) ) 
                            {
                                $fields[":{$type}ID"] = null;
                            } 
                            else 
                            {
                                $fields[":{$type}ID"] = $args["{$type}ID"];
                            } 
                            
                            
                        } 
                        
                } 
                 * **************
                 */
            }
            
            
            
           if($args['BranchJobSearchScope'] != '') {
               $sql_fields .= "BranchJobSearchScope = '{$args['BranchJobSearchScope']}', ";
           }

            
            $sql = "
                    UPDATE
                        user 
                    SET 
                        {$sql_fields} 
                        Username = :Username, 
                        CustomerTitleID = :CustomerTitleID, 
                        ContactFirstName = :ContactFirstName, 
                        ContactLastName = :ContactLastName, 
                        ContactEmail = :ContactEmail, 
                        ContactMobile = :ContactMobile, 
                        SecurityQuestionID = :SecurityQuestionID, 
                        Answer = :Answer, 
                        DefaultBrandID = :DefaultBrandID, 
                        Status = :Status
                    WHERE
                        UserID=:UserID
                    ";    
            
           $this->controller->log('SystemUser->update : ' . $sql . "\n" . var_export($fields, true));

            
            $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $updateQuery->execute( $fields );  
            
            #TableFactory::Modify( $this->conn, 'User', 'update', $fields ); 
            
            return array('status' => 'OK',
                        'message' => $this->controller->page['Text']['data_updated_msg']);
        } else {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => $this->controller->page['data_deleted_msg']);
    }
    
    /*
     * @tutotial extra salt: |sKyLiNe|
     */
    public function encrypt($password){
        return md5($password.'|sKyLiNe|'.strrev($password));
    }
    

    
    public function deleteAltField($id) {
	
	$q = "DELETE FROM alternative_fields WHERE alternativeFieldID=:id";
	$values = array("id" => $id);
	$result = $this->execute($this->conn, $q, $values);
	return $result;
	
    }
    
    
    
}
?>