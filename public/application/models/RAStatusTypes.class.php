<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of RA Status Types Page in Lookup Tables section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */

class RAStatusTypes extends CustomModel {
    
    private $conn;
    private $dbColumns = [
	"t1.RAStatusID", 
	"t1.RAStatusCode", 
	"t1.RAStatusName",  
	"t1.ListOrder",
	"t1.SubsetOnly",
	"t1.Final",
	"(SELECT COUNT(*) FROM ra_status_subset AS rass WHERE t1.RAStatusID = rass.ParentRAStatusID) AS subset",
	"t1.Colour", 
	"t1.Status", 
	"t2.BrandName", 
	"t1.BrandID"
    ];
    private $tables = "ra_status AS t1 LEFT JOIN brand AS t2 ON t1.BrandID = t2.BrandID";
    private $table = "ra_status";
    
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    

    
    /**
    * Description
    * 
    * This method is for fetching data from database
    * 
    * @param array $args Its an associative array contains where clause, limit and order etc.
    * @global $this->conn
    * @global $this->tables
    * @global $this->dbColumns
    * @return array 
    * 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */
    
    public function fetch($args) {
        
        if ($this->controller->user->SuperAdmin) {
	    
	    if (isset($args["firstArg"])) {
		$authTypeID = addslashes($args["firstArg"]);
		if ($authTypeID != "") {
		    $args["where"] = " t1.AuthorisationTypeID = {$authTypeID} ";
		}
	    }
	    
	    if (isset($_GET["active"]) && $_GET["active"]) {
		if (isset($args["where"])) {
		    $args["where"] .= " AND t1.Status = 'Active'";
		} else {
		    $args["where"] = " t1.Status = 'Active'";
		}
	    }
	    
            $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
	    
        } else if (is_array($this->controller->user->Brands)) {    
            
	    $brandsList = implode(",", array_keys($this->controller->user->Brands));
	    
	    if ($brandsList) {
		$brandsList .= "," . $this->controller->SkylineBrandID;
	    }    
          
            $args["where"] = "t1.BrandID IN (" . $brandsList . ")";

	    if (isset($args["firstArg"])) {
		$authTypeID = addslashes($args["firstArg"]);
		if ($authTypeID != "") {
		    $args["where"] .= " AND t1.AuthorisationTypeID = {$authTypeID} ";
		}
	    }
	  
	    if (isset($_GET["active"]) && $_GET["active"]) {
		if (isset($args["where"])) {
		    $args["where"] .= " AND t1.Status = 'Active'";
		} else {
		    $args["where"] = " t1.Status = 'Active'";
		}
	    }
	    
            $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
       
        }
        
        return  $output;
        
    }
    
    
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
     public function processData($args) {
         
         if (!isset($args["RAStatusID"]) || !$args["RAStatusID"]) {
	    return $this->create($args);
         } else {
	    return $this->update($args);
         }
	 
     }
    
     
     
     /**
     * Description
     * 
     * This method finds the maximum code for given brand in database table.
     * 
     * @param interger $BrandID This is primary key of brand table.
     * @global $this->table
     * @return integer It returns maximum code if it finds in the database table otherwise it returns 0.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */  
    
     public function getCode($BrandID) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT RAStatusCode FROM '.$this->table.' WHERE BrandID=:BrandID ORDER BY RAStatusID DESC LIMIT 0,1';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        //$this->controller->log(var_export($BrandID, true));
        
        $fetchQuery->execute(array(':BrandID' => $BrandID));
        $result = $fetchQuery->fetch();
        if(isset($result[0]))
        {
           return $result[0];
        }
        else
        {
             return 0;
        }
       
    }
    
    
    /**
     * Description
     * 
     * This method is used for to validate action code for given brand.
     *
     * @param interger $RAStatusCode  
     * @param interger $BrandID.
     * @param interger $RAStatusID.
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValidAction($RAStatusCode, $BrandID, $RAStatusID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT RAStatusID FROM '.$this->table.' WHERE RAStatusCode=:RAStatusCode AND BrandID=:BrandID AND RAStatusID!=:RAStatusID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':RAStatusCode' => $RAStatusCode, ':BrandID' => $BrandID, ':RAStatusID' => $RAStatusID));
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && $result['RAStatusID'])
        {
                return false;
        }
        
        return true;
    
    }
    
    
    
    /**
    * Description
    * 
    * This method is used for to insert data into database.
    *
    * @param array $args  
    * @global $this->table 
    * @return array It contains status of operation and message.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */
    
    public function create($args) {
        
        /* Execute a prepared statement by passing an array of values */
        $sql = "INSERT INTO " . $this->table . " 
			    (
				RAStatusName, 
				RAStatusCode, 
				ListOrder, 
				Colour, 
				Status, 
				BrandID, 
				ModifiedUserID, 
				ModifiedDate,
				AuthorisationTypeID,
				SBAuthorisationStatusID,
				SubsetOnly,
				Final
			    )
		VALUES	    
			    (
				:RAStatusName, 
				:RAStatusCode, 
				:ListOrder, 
				:Colour, 
				:Status, 
				:BrandID, 
				:ModifiedUserID, 
				:ModifiedDate, 
				:authorisationTypeID,
				:serviceBaseStatus,
				:subsetOnly,
				:final
			    )
	";
        
        if (!isset($args["RAStatusCode"]) || !$args["RAStatusCode"]) {
            $args["RAStatusCode"] = $this->getCode($args["BrandID"]) + 1;   //Preparing next  code.
        }
        
        if ($this->isValidAction($args["RAStatusCode"], $args["BrandID"], 0)) {
	    
            $insertQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            
            $insertQuery->execute([
		":RAStatusName" => $args["RAStatusName"],
		":RAStatusCode" => $args["RAStatusCode"], 
		":ListOrder" => $args["ListOrder"], 
		":Colour" => $args["Colour"], 
		":Status" => $args["Status"], 
		":BrandID" => $args["BrandID"],
		":ModifiedDate" => date("Y-m-d H:i:s"), 
		":ModifiedUserID" => $this->controller->user->UserID, 
		":authorisationTypeID" => $args["AuthorisationType"],
		":serviceBaseStatus" => $args["ServiceBaseStatus"],
		":subsetOnly" => $args["SubsetOnly"],
		":final" => $args["Final"]
	    ]);
            
	    $RAStatusID = $this->conn->lastInsertId();
                    
	    if(!isset($args['Subset'])) {
		$args['Subset'] = [];
	    }    
                  
	    $this->insertRAStatusSubset($args['Subset'], $RAStatusID);
            
	    return [
		"status" => "OK",
		"message" => $this->controller->page["Text"]["data_inserted_msg"]
	    ];
	    
        } else {
            
            return [
		"status" => "ERROR",
		"message" => $this->controller->messages->getError(1024, "default", $this->controller->lang)
	    ];
	    
        }
	
    }
    
    
    
    /**
    * Description
    * 
    * This method is used for to fetch a row from database.
    *
    * @param array $args
    * @global $this->table  
    * @return array It contains row of the given primary key.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */
    
    public function fetchRow($args) {
        
        /*Execute a prepared statement by passing an array of values*/
        $sql = "SELECT	    ra.RAStatusID, 
			    ra.RAStatusName, 
			    ra.RAStatusCode, 
			    ra.ListOrder, 
			    ra.Colour, 
			    ra.Status, 
			    ra.BrandID,
			    ra.SBAuthorisationStatusID,
			    ra.SubsetOnly,
			    ra.AuthorisationTypeID,
			    ra.Final,
			    at.AuthorisationTypeName
		
		FROM	    ra_status AS ra
		
		LEFT JOIN   authorisation_types AS at ON at.AuthorisationTypeID = ra.AuthorisationTypeID
		
		WHERE	    ra.RAStatusID = :RAStatusID
	       ";
	
        $fetchQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
        
        $fetchQuery->execute([':RAStatusID' => $args['RAStatusID']]);
        $result = $fetchQuery->fetch();
        
        if(is_array($result)) {
            
	    $result['Subset'] = [];
            
            //Getting subset details.
            $sql2 = "	SELECT  ChildRAStatusID 
			FROM    ra_status_subset 
			WHERE   ParentRAStatusID = :ParentRAStatusID";
	    
            $fetchQuery2 = $this->conn->prepare($sql2, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            $fetchQuery2->execute([':ParentRAStatusID' => $args['RAStatusID']]);
            $result2     = $fetchQuery2->fetchAll();
            
            foreach($result2 as $ras) {
                $result['Subset'][] = $ras['ChildRAStatusID'];
            }  
            
        }
        
	//$this->controller->log("FETCH ROW: " . var_export($result, true));
	
        return $result;
    }
    
    
    
    /**
     * Description
     * 
     * This method is used for to insert RA status subset for given RAStatusID.
     *
     * @param  array $Subset
     * @param  int   $RAStatusID
     
     * 
    
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function insertRAStatusSubset($Subset, $RAStatusID) {
        
        
        if($RAStatusID)
        {    
            /* Execute a prepared statement by passing an array of values */
           $sql = 'DELETE FROM ra_status_subset WHERE ParentRAStatusID=:ParentRAStatusID';
           $deleteQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));


           $deleteQuery->execute(array(':ParentRAStatusID' => $RAStatusID));
           
           if(is_array($Subset))
           {
                $sql = 'INSERT INTO ra_status_subset (ParentRAStatusID, ChildRAStatusID) VALUES (:ParentRAStatusID, :ChildRAStatusID)';
                $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));

                foreach($Subset as $raSetValue)
                {    
                    $insertQuery->execute(array(':ParentRAStatusID' => $RAStatusID, ':ChildRAStatusID'=>$raSetValue));
                }
           }
        }
        
    }
    
    
    
    /**
     * Description
     * 
     * This method is used for to get latest status id for given Ra type and job id.
     *
     * @param int $JobID
     * @param string $RAType
     *  
     * @return int status id
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function getLatestRAHisotryStatusID($JobID, $RAType) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT RAStatusID FROM ra_history WHERE JobID=:JobID AND RAType=:RAType ORDER BY RAHistoryID DESC';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':JobID' => $JobID, ':RAType' => $RAType));
        $result = $fetchQuery->fetch();
        
        if(isset($result['RAStatusID']))
        {
            return $result['RAStatusID'];
        }   
        else
        {
            return 0;
        }
        
    }
    
    
    
    /**
    * Description
    * 
    * This method is used for to fetch all active rows from database.
    *
    * @param int $RAStatusID
    * @param int $includeParent
    * 
    * @global $this->table  
    * @return array 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */
    
    public function getRAStatusTypes($RAStatusID = 0, $includeParent = false, $brandID = false, $authType = false, $subset = false) {
        
        $result = [];
        
	if ($authType) {
	    
	    $at = addslashes($authType);
	    $where = " AND T1.AuthorisationTypeID = (   
							SELECT  at.AuthorisationTypeID
							FROM    authorisation_types AS at
							WHERE   at.AuthorisationTypeName LIKE '{$at}'
						    )
		     ";
							
	} else {
	    
	    $where = "";
	    
	}
	
        if ($RAStatusID) {
            
            if ($includeParent) {
		
                $sql = "SELECT	    T1.RAStatusID, 
				    T1.RAStatusName,
				    T1.RAStatusCode
				
			FROM	    ra_status AS T1 
			    
			LEFT JOIN   ra_status_subset AS T0 ON T0.ChildRAStatusID = T1.RAStatusID AND 
				    T0.ParentRAStatusID = :RAStatusID 
			
			WHERE	    T1.Status = :Status AND 
				    (T0.ParentRAStatusID IS NOT NULL OR T1.RAStatusID = :pRAStatusID) 
				    /*AND T1.SubsetOnly != 'Yes'*/
				    {$where}
				    
			ORDER BY    T1.ListOrder ASC";
		
                $fetchQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
                
                $fetchQuery->execute(["Status" => "Active", "RAStatusID" => $RAStatusID, "pRAStatusID" => $RAStatusID]);
                $result = $fetchQuery->fetchAll();
                
            } else {
		
                $sql = "SELECT	    T1.RAStatusID, 
				    T1.RAStatusName, 
				    T1.RAStatusCode
				    
			FROM	    ra_status_subset AS T0 
			
			LEFT JOIN   ra_status AS T1 ON T0.ChildRAStatusID = T1.RAStatusID AND 
				    T0.ParentRAStatusID = :RAStatusID 
				    
			WHERE	    T1.Status = :Status 
				    /*AND T1.SubsetOnly != 'Yes'*/
				    {$where}
			
			ORDER BY    T1.ListOrder ASC";
		
                $fetchQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);

                $fetchQuery->execute(["Status" => "Active", "RAStatusID" => $RAStatusID]);
                $result = $fetchQuery->fetchAll();
		
            }
         
        }
        
	
	if (!$RAStatusID) {
	    
	    if(!$subset) {
		$sub = " AND SubsetOnly != 'Yes' ";
	    } else {
		$sub = "";
	    }
	    
	    $q = "  SELECT  RAStatusID, 
			    RAStatusName,
			    RAStatusCode

		    FROM    ra_status AS T1

		    WHERE   CASE
				WHEN	(	
					    SELECT	COUNT(*)
					    FROM	ra_status
					    WHERE	BrandID = :brandID AND Status = :status
					) > 0
					
				THEN	BrandID = :brandID
					
				ELSE	BrandID = 1000
			    END	
			    
			    AND Status = :status
			    
			    {$sub}
			    
			    {$where}
				
		    ORDER BY T1.ListOrder ASC
		 ";

	    
	    $fetchQuery = $this->conn->prepare($q, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);

	    if (!$brandID) {
		$brandID = $this->controller->user->DefaultBrandID;
	    }
	    
	    $values = ["status" => "Active", "brandID" => $brandID];
	    
	    $fetchQuery->execute($values);
	    $result = $fetchQuery->fetchAll();
	    
        }
        
        return $result;
	
    }
    
    
    
    /**
    * Description
    * 
    * This method is used for to udpate a row into database.
    *
    * @param array $args
    * @global $this->table   
    * @return array It contains status of operation and message.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
    
    public function update($args) {
        
        if($this->isValidAction($args["RAStatusCode"], $args["BrandID"], $args["RAStatusID"])) {        
            
	    /* Execute a prepared statement by passing an array of values */
	    $sql = "UPDATE  " . $this->table . "
		
		    SET	    RAStatusName = :RAStatusName, 
			    RAStatusCode = :RAStatusCode, 
			    ListOrder = :ListOrder, 
			    Colour = :Colour, 
			    Status = :Status, 
			    BrandID = :BrandID, 
			    ModifiedUserID = :ModifiedUserID, 
			    ModifiedDate = :ModifiedDate,
			    SBAuthorisationStatusID = :serviceBaseStatus,
			    SubsetOnly = :subsetOnly,
			    Final = :final

		    WHERE   RAStatusID = :RAStatusID
	    ";
        
	    $updateQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
	    $updateQuery->execute([
		":RAStatusName" => $args["RAStatusName"],
		":RAStatusCode" => $args["RAStatusCode"],
		":ListOrder" => $args["ListOrder"],
		":Colour" => $args["Colour"],
		":Status" => $args["Status"],
		":BrandID" => $args["BrandID"],
		":ModifiedUserID" => $this->controller->user->UserID,
		":ModifiedDate"=>date("Y-m-d H:i:s"),
		":RAStatusID" => $args["RAStatusID"],
		":serviceBaseStatus" => $args["ServiceBaseStatus"],
		":subsetOnly" => $args["SubsetOnly"],
		":final" => $args["Final"]
	    ]);
              
	    if(!isset($args["Subset"])) {
		$args["Subset"] = [];
	    }    
                  
	    $this->insertRAStatusSubset($args["Subset"], $args["RAStatusID"]);
            
	    return [
		"status" => "OK",
		"message" => $this->controller->page["Text"]["data_updated_msg"]
	    ];
	       
        } else {
	    
	    return [
		"status" => "ERROR",
		"message" => $this->controller->messages->getError(1024, "default", $this->controller->lang)
	    ];
	     
        }
	
    }
    
    
    
    /**
    * Description
    * 
    * This method is used for to insert a row into ra_history.
    *
    * @param array $args
    * @return void 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
    public function recordRAHisotry($args) {
        
	$sql = "INSERT INTO ra_history 
			    (
				JobID, 
				RAStatusID, 
				RAType, 
				Notes,
				Image,
				ModifiedDate, 
				ModifiedUserID
			    ) 
		VALUES	    
			    (
				:JobID, 
				:RAStatusID, 
				:RAType, 
				:Notes,
				:Image,
				:ModifiedDate, 
				:ModifiedUserID
			    )
	      ";
        
	$insertQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
	      
	$insertQuery->execute([
	    ":JobID" => $args["JobID"], 
	    ":RAStatusID" => $args["RAStatusID"], 
	    ":RAType" => $args["RAType"], 
	    ":Notes" => $args["Notes"],
	    ":Image" => $args["Image"],
	    ":ModifiedUserID" => $this->controller->user->UserID, 
	    ":ModifiedDate" => date("Y-m-d H:i:s")  
	]);
       
    }
    

    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => $this->controller->page['Text']['data_deleted_msg']);
    }
    
    
    
    public function getRAStatusSubset($brandID, $statusCode) {
	
	$q = "	SELECT	    RAStatusID

		FROM	    ra_status

		WHERE	    RAStatusID IN   (
						SELECT	subset.ChildRAStatusID
						FROM	ra_status_subset AS subset
						JOIN	ra_status AS status ON subset.ParentRAStatusID = status.RAStatusID
						WHERE   status.BrandID = :brandID AND status.RAStatusCode = :statusCode
					    )
		
		ORDER BY    ra_status.ListOrder ASC
	     ";
	
	$values = ["brandID" => $brandID, "statusCode" => $statusCode];
	$result = $this->query($this->conn, $q, $values);
	
	foreach($result as $row) {
	    $subset[] = $row["RAStatusID"];
	}
	
	return isset($subset) ? $subset : [];
	
    }
    
    
    
    public function getPrimaryRAStatus($raStatusID) {
	
	$q = "	SELECT	* 
		FROM	ra_status 
		WHERE	RAStatusID = (	
					SELECT	RAStatusCode
					FROM	ra_status
					WHERE	RAStatusID = :raStatusID
				     )
	     ";
	$values = ["raStatusID" => $raStatusID];
	$result = $this->query($this->conn, $q, $values);
	return isset($result[0]) ? $result[0] : false;
	
    }
    
    
    
    public function getAuthorisationTypes() {
	
	$q = "SELECT AuthorisationTypeID, AuthorisationTypeName FROM authorisation_types";
	$result = $this->query($this->conn, $q);
	return $result;
	
    }
    
    
    
    public function getSBStatuses() {
	
	$q = "SELECT SBAuthorisationStatusID, SBAuthorisationStatus FROM sb_authorisation_statuses";
	$result = $this->query($this->conn, $q);
	return $result;
	
    }
    
    
    
}
?>
