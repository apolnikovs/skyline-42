<?php

require_once('CustomBusinessModel.class.php');
require_once('Functions.class.php');
require_once('Constants.class.php');

/**
 * Short Description of Skyline Business Model.
 * 
 * Long description of Skyline Business Model.
 *
 * @author     Brian Etherington <b.etherington@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.5
 * 
 *  
 * Changes
 * Date        Version Author                Reason
 * 30/01/2013 1.0      Brian Etherington     Initial Version
 * 07/03/2013 1.1      Brian etherington     Added CreateJob method
 *                                           Updated getJobDetails to include
 *                                                   Service Centre and job booking details
 * 12/03/2013 1.2      Brian Etherington     Fixed problem getJobDetails with $branch array
 *                                           not being initialised.
 * 14/03/2013 1.3      Brian Etherington     Add method PostcodeLookup
 * 23/05/2013 1.4      Brian Etherington     Added ServiceBaseUnitType (free text) lookup in create job if
 *                                           ModelID is absent and UnitTypeID is present 
 * 09/07/2013 1.5      Brian Etherington     Added JobSourceID to CreateJob (default is null)
 * **************************************************************************** */
class SkylineBusinessModel extends CustomBusinessModel {

    public function __construct($controller) {
        parent::__construct($controller);
    }
    
    public function PostcodeLookup( $postcode ) {       
        
        $postcode_model = $this->loadModel('Postcode');
        $addresses = $postcode_model->getAddress($_POST['Postcode']);
                
        if (is_array($addresses) && $addresses[0][0]=='Wrong Post Code') {
            
            return false;
            
        }        
        
        if (is_array($addresses)) {
            
            //format address ass a single string and add to array as element 11
            foreach ($addresses as &$row) {
                
                $add_str = '';
                
                if ($row[4] || $row[6]  || $row[7]) {
                  $add_str =  trim($row[4]." ".$row[6]." ".$row[7]);
                }
                if ($row[5]  || $row[0]) {
                  if($add_str) $add_str .=  ", "; 
                  $add_str .=  trim($row[5].' '.$row[0]);
                }
                if ($row[2]) {
                  if($add_str) $add_str .=  ", "; 
                  $add_str .=  $row[2];
                }    
                if ($row[3]) {
                  if($add_str) $add_str .=  ", "; 
                  $add_str .=  $row[3];
                }
            
                $row[11]  = $add_str;
            
            }          
            
        }
        
        return $addresses;
        
    }
    
    public function findJobs( array $args, stdClass $User=null ) {
        
        /* $args is an associative array of search terms */
        
        $params = array();
        $cust = array();
        
        if (!empty($args['LastName'])) {
            $cust['ContactLastName'] = $args['LastName'];
        }
        if (!empty($args['Email'])) {
            $cust['ContactEmail'] = $args['Email'];
        }
        if (!empty($args['PostalCode'])) {
            $cust['PostalCode'] = $args['PostalCode'];
        }
        if (!empty($args['Phone'])) {
            $cust['ContactHomePhone'] = '%'.$args['Phone'].'%';
            $cust['ContactWorkPhone'] = '%'.$args['Phone'].'%';
            $cust['ContactMobile'] = '%'.$args['Phone'].'%';
        }
        
        $params['Customer'] = $cust;
        
        $model = $this->loadModel( 'SkylineQueriesModel' );
        $result = $model->findJobs( $params, $User );
        
        $jobs = array();
        foreach($result as $row) {
            $jobs[] = $row['JobID'];
        }
 
        return $jobs;
    
    }

    public function getJobDetails( $JobID, stdClass $User=null ) {

        /* Note:  This method currently just returns address Details
         *        for an individual job but can easily be extended to return
         *        other structured job information as required.
         */

        $model = $this->loadModel('SkylineQueriesModel');
        $data = $model->getJobDetails($JobID, $User);
        
        $network = array(
            'name' => '',
            'address' => '',
            'phone' => ''
        );

        $client = array(
            'name' => '',
            'address' => '',
            'phone' => ''
        );
        
        $branch = array(
            'brand' => '',
            'name' => '',
            'address' => '',
            'phone' => ''
        );

        $user = array(
            'name' => '',
            'username' => '',
            'usertype' => '',
            'company' => ''
        );
        
        $service_provider = array(
            'id' => null,
            'name' => '',
            'address' => array( 'line_1' => '',
                                'line_2' => '',
                                'line_3' => '',
                                'line_4' => '',
                                'postcode' => '',
                                'country' => ''),
            'phone' => '',
            'ext' => '',
            'email'=> '',
            'WeekdaysOpenTime'=> '',
            'WeekdaysCloseTime'=> '',
            'SaturdayOpenTime'=> '',
            'SaturdayCloseTime'=> ''            
        );
        
        $customer = array(
            'id' => null,
            'title' => '',
            'first_name' => '',
            'last_name' => '',
            'address' => array( 'line_1' => '',
                                'line_2' => '',
                                'line_3' => '',
                                'line_4' => '',
                                'postcode' => '',
                                'country' => ''),
            'home_phone' => '',
            'work_phone' => null,
            'work_ext' => null,
            'mobile' => null,
            'email'=> null
        );
        
        $collection = array(
            'collection_date' => null,
            'courier_id' => null,
            'consignment_no' => '',
            'packaging_type' => '',
            'name' => '',
            'address' => array( 'line_1' => '',
                                'line_2' => '',
                                'line_3' => '',
                                'line_4' => '',
                                'postcode' => '',
                                'country' => ''),
            'phone' => null,
            'phone_ext' => null,
            'email'=> null,
            'county_id' => null,
            'country_id' => null
        );
        
        $job = array(
             'date_booked' => '',
             'current_status' => '',
             'manufacturer' => '',
             'model' => '',
             'service_type' => '',
             'unit_type' => '',
             'ServiceCentreJobNo' => ''   
        );
                
        $appointments = array();
        
        if ($data !== null) {
            
            $job = array(
                'date_booked' => $data['DateBooked'],
                'current_status' => $data['Status'],
                'manufacturer' => $data['Manufacturer'],
                'model' => $data['Model'],
                'service_type' => $data['serviceType'],
                'unit_type' => $data['UnitType'],
                'ServiceCentreJobNo' => $data['ServiceCentreJobNo']
            );

            if (isset($data['NetworkName'])) {
                $network = array(
                    'name' => $data['NetworkName'],
                    'address' => Functions::FormatAddress( $data['NetworkBuildingNameNumber'], 
                                                           $data['NetworkStreet'], 
                                                           $data['NetworkLocalArea'], 
                                                           $data['NetworkTownCity'], 
                                                           $data['NetworkCounty'], 
                                                           $data['NetworkPostalCode'], 
                                                           $data['NetworkCountry'] ),
                    'phone' => $data['NetworkContactPhone']
                );
            }

            if (isset($data['ClientName'])) {
                $client = array(
                    'name' => $data['ClientName'],
                    'address' => Functions::FormatAddress( $data['ClientBuildingNameNumber'], 
                                                           $data['ClientStreet'], 
                                                           $data['ClientLocalArea'], 
                                                           $data['ClientTownCity'], 
                                                           $data['ClientCounty'], 
                                                           $data['ClientPostalCode'], 
                                                           $data['ClientCountry'] ),
                    'phone' => $data['ClientContactPhone']
                );
            }

            if (isset($data['BranchName'])) {
                $branch = array(
                    'brand' => $data['BrandName'],
                    'name' => $data['BranchName'],
                    'address' => Functions::FormatAddress( $data['BranchBuildingNameNumber'], 
                                                           $data['BranchStreet'], 
                                                           $data['BranchLocalArea'], 
                                                           $data['BranchTownCity'], 
                                                           $data['BranchCounty'], 
                                                           $data['BranchPostalCode'], 
                                                           $data['BranchCountry'] ),
                    'phone' => $data['BranchContactPhone']
                );
            }
            
            $customer = array(
                    'id' => $data['CustomerID'],
                    'title' => $data['CustomerTitle'],
                    'first_name' => $data['CustomerFirstName'],
                    'last_name' => $data['CustomerLastName'],
                    'address' => Functions::FormatAddress( $data['CustomerBuildingNameNumber'], 
                                                           $data['CustomerStreet'], 
                                                           $data['CustomerLocalArea'], 
                                                           $data['CustomerTownCity'], 
                                                           $data['CustomerCounty'], 
                                                           $data['CustomerPostalCode'], 
                                                           $data['CustomerCountry'],
                                                           'Array' ),
                    'home_phone' => $data['CustomerMobile'],
                    'work_phone' => $data['CustomerWorkPhone'],
                    'work_ext' => $data['CustomerWorkPhoneExt'],
                    'mobile' => $data['CustomerMobile'],
                    'email'=> $data['CustomerEmail'],
            );
            
            $collection = array(
                    'collection_date' => $data['CollectionDate'],
                    'courier_id' => $data['CourierID'],
                    'consignment_no' => $data['ConsignmentNo'],
                    'packaging_type' => $data['PackagingType'],
                    'name' => $data['ColAddCompanyName'],
                    'address' => Functions::FormatAddress( $data['ColAddBuildingNameNumber'], 
                                                           $data['ColAddStreet'], 
                                                           $data['ColAddLocalArea'], 
                                                           $data['ColAddTownCity'], 
                                                           $data['CollectionCounty'], 
                                                           $data['ColAddPostcode'], 
                                                           $data['CollectionCountry'],
                                                           'Array' ),
                    'phone' => $data['ColAddPhone'],
                    'phone_ext' => $data['ColAddPhoneExt'],
                    'email'=> $data['ColAddEmail'],
                    'county_id' => $data['ColAddCountyID'],
                    'country_id' => $data['ColAddCountryID']
            );
            
            if (isset($data['ServiceProviderName'])) {
                $service_provider = array(
                    'id' => $data['ServiceProviderID'],
                    'name' => $data['ServiceProviderName'],
                    'address' => Functions::FormatAddress( $data['ServiceProviderBuildingNameNumber'], 
                                                           $data['ServiceProviderStreet'], 
                                                           $data['ServiceProviderLocalArea'], 
                                                           $data['ServiceProviderTownCity'], 
                                                           $data['ServiceProviderCounty'], 
                                                           $data['ServiceProviderPostalCode'], 
                                                           $data['ServiceProviderCountry'],
                                                           'Array' ),
                    'phone' => $data['ServiceProviderContactPhone'],
                    'ext' => $data['ServiceProviderContactPhoneExt'],
                    'email'=> $data['ServiceProviderContactEmail'],
                    'WeekdaysOpenTime'=> $data['ServiceProviderWeekdaysOpenTime'],
                    'WeekdaysCloseTime'=> $data['ServiceProviderWeekdaysCloseTime'],
                    'SaturdayOpenTime'=> $data['ServiceProviderSaturdayOpenTime'],
                    'SaturdayCloseTime'=> $data['ServiceProviderSaturdayCloseTime']            
                );
            }

            if (isset($data['UserName'])) {
                $user = array(
                    'name' => trim($data['UserContactFirstName'] . ' ' . $data['UserContactLastName']),
                    'username' => $data['UserName'],
                    'usertype' => Functions::UserType( $data['UserSuperAdmin'], 
                                                       $data['UserNetworkID'], 
                                                       $data['UserServiceProviderID'], 
                                                       $data['UserClientID'], 
                                                       $data['UserBranchID'], 
                                                       $data['UserManufacturerID'], 
                                                       $data['UserExtendedWarrantorID'] ),
                    'company' => $data['UserBrandName']
                );
            }
            
            $appointments = $model->getAppointments($JobID);
        }
        
        $result = array( 'JobID' => $JobID,
                         'error' => ($data === null) ? "JOB $JobID NOT FOUND" : '',
                         'network' => $network,
                         'client' => $client,
                         'branch' => $branch,
                         'user' => $user,
                         'job' => $job,
                         'customer' => $customer,
                         'collection' => $collection,
                         'service_provider' => $service_provider,
                         'appointments' => $appointments );

        return $result;
    }
    
    public function createJob($args, $messages) {
               
        $emailModel = $this->loadModel('Email');

        try {

            $jobModel = $this->loadModel('Job');
            $skyline = $this->loadModel('Skyline');

            $user = $this->controller->user;
            if ($user->UserID == 0)
                $user->UserID = null;
            
            $branch_id = empty($args['BranchID']) ? (empty($args['DefaultBranchID']) ? null : $args['DefaultBranchID']) : $args['BranchID'];

            $raRequired = $jobModel->checkRARequired( empty($args['ServiceTypeID']) ? null : $args['ServiceTypeID'], 
                                                      empty($args['ManufacturerID']) ? null : $args['ManufacturerID'], 
                                                      empty($args['AuthorisationNo']) ? null : $args['AuthorisationNo'] );
                        
            // allocate service centre
            if (empty($args['ServiceProviderID'])) { 
                
                if ( !empty($args['ClientID']) ) { 
                    
                    $ClientsModel = $this->loadModel('Clients');
                    $ClientDetails = $ClientsModel->fetchRow(array('ClientID'=>$args['ClientID']));
                    
                    if ( !empty($ClientDetails['ServiceProviderID']) ) {
                        $args['ServiceProviderID'] = $ClientDetails['ServiceProviderID'];
                    }
                }  
            
                if (empty($args['ServiceProviderID'])) {
                    $args['ServiceProviderID']  = $skyline->findServiceCentre($args);
                }
            }
            
            $ServiceProviderDetails = array();
        
            if (!empty($args['ServiceProviderID'])) { 
                $ServiceProviderDetails = $skyline->getServiceCenterDetails($args['ServiceProviderID']);
            }

            // Initialise the Job Status 
            if ($branch_id !== null && $jobModel->isServiceAppraisalRequired($branch_id)) {
                $appraisalRequired = true;
                $statusID = $skyline->getStatusID('00 APPRAISAL REQUIRED');
            } else {
                $appraisalRequired = false;
                if ($raRequired) {
                    $statusID = $skyline->getStatusID('00 REPAIR AUTHORISATION');
                } else if (empty($args['ServiceProviderID'])) {
                    $statusID = $skyline->getStatusID('01 UNALLOCATED JOB');
                } else {
                    $statusID = $skyline->getStatusID('02 ALLOCATED TO AGENT');
                }
            }

            $repair_type = $skyline->getRepairTypeFromJobTypeID($args['JobTypeID']);

            //Create Customer Record          
            $customer = $this->loadModel('Customer');
            $cust_data = [
                'CustomerTitleID' => empty($args['CustomerTitleID']) ? NULL : $args['CustomerTitleID'],
                'CompanyName' => empty($args['CompanyName']) ? null : $args['CompanyName'],
                'ContactFirstName' => empty($args['ContactFirstName']) ? null : $args['ContactFirstName'],
                'ContactLastName' => empty($args['ContactLastName']) ? null : $args['ContactLastName'],
                'PostalCode' => empty($args['PostalCode']) ? null : strtoupper($args['PostalCode']),
                'BuildingNameNumber' => empty($args['BuildingNameNumber']) ? null : $args['BuildingNameNumber'],
                'Street' => empty($args['Street']) ? null : $args['Street'],
                'LocalArea' => empty($args['LocalArea']) ? null : $args['LocalArea'],
                'TownCity' => empty($args['City']) ? null : $args['City'],
                'CountyID' => empty($args['CountyID']) ? null : $args['CountyID'],
                'CountryID' => empty($args['CountryID']) ? null : $args['CountryID'],
                'ContactEmail' => empty($args['ContactEmail']) ? null : $args['ContactEmail'],
                'ContactHomePhone' => empty($args['ContactHomePhone']) ? null : $args['ContactHomePhone'],
                'ContactMobile' => empty($args['ContactMobile']) ? null : $args['ContactMobile'],
                'DataProtection' => (isset($args['data_protection_act']) && $args['data_protection_act'] == "1") ? 'Yes' : 'No',
                'DataProtectionEmail' => (isset($args['contact_method_mail']) && $args['contact_method_mail'] == "1") ? '1' : '0',
                'DataProtectionLetter' => (isset($args['contact_method_letter']) && $args['contact_method_letter'] == "1") ? '1' : '0',
                'DataProtectionSMS' => (isset($args['contact_method_sms']) && $args['contact_method_sms'] == "1") ? '1' : '0',
            ];

            $cust = $customer->create($cust_data);
            if ($cust['status'] == 'OK')
                $custID = $cust['id'];
            else
                $custID = null;

            $ModelID = empty($args['ModelID']) ? null : $args['ModelID'];
            $ProductID = empty($args['ProductID']) ? null : $args['ProductID'];

            $args['ServiceBaseModel'] = null;
            if ($ModelID === null) {
                if (!empty($args['ModelName'])) {
                    $args['ServiceBaseModel'] = $args['ModelName'];
                } else if (!empty($args['ProductDescription'])) {
                    $args['ServiceBaseModel'] = $args['ProductDescription'];
                }
                if (!isset($args['ServiceBaseUnitType']) && isset($args['UnitTypeID'])) {
                    $unitTypeModel = $this->loadModel('UnitTypes');
                    $args['ServiceBaseUnitType'] = $unitTypeModel->getName($args['UnitTypeID']);
                }
            }


            if (isset($args['ProductLocation']) && ( $args['ProductLocation'] == "Service Provider" || $args['ProductLocation'] == "Branch")) {
                $Accessories = empty($args["Accessories"]) ? null : $args["Accessories"];
                $UnitCondition = empty($args["UnitCondition"]) ? null : $_POST["UnitCondition"];
            } else {
                $Accessories = NULL;
                $UnitCondition = NULL;
            }

            // Create Job Record
            $job = [
                'NetworkID' => empty($args['NetworkID']) ? null : $args['NetworkID'],
                'ClientID' => empty($args['ClientID']) ? null : $args['ClientID'],
                'BranchID' => $branch_id,
                'ServiceProviderID' => empty($args['ServiceProviderID']) ? null : $args['ServiceProviderID'],
                'CustomerID' => $custID,
                'JobTypeID' => empty($args['JobTypeID']) ? null : $args['JobTypeID'],
                'ServiceTypeID' => empty($args['ServiceTypeID']) ? null : $args['ServiceTypeID'],
                'ServiceBaseUnitType' => empty($args['ServiceBaseUnitType']) ? null : $args['ServiceBaseUnitType'],
                'ServiceBaseModel' => $args['ServiceBaseModel'],
                'ManufacturerID' => empty($args['ManufacturerID']) ? null : $args['ManufacturerID'],
                'ModelID' => $ModelID,
                'SerialNo' => empty($args['SerialNo']) ? null : $args['SerialNo'],
                'ReportedFault' => empty($args['ReportedFault']) ? null : $args['ReportedFault'],
                'ProductLocation' => empty($args['ProductLocation']) ? null : $args['ProductLocation'],
                'OriginalRetailer' => trim((empty($args['OriginalRetailerPart1']) ? '' : $args['OriginalRetailerPart1'])
                        . " " . (empty($args['OriginalRetailerPart2']) ? '' : $args['OriginalRetailerPart2'])
                        . " " . (empty($args['OriginalRetailerPart3']) ? '' : $args['OriginalRetailerPart3'])),
                'Insurer' => empty($args['Insurer']) ? null : $args['Insurer'],
                'PolicyNo' => empty($args['PolicyNo']) ? null : $args['PolicyNo'],
                'AuthorisationNo' => empty($args['AuthorisationNo']) ? null : $args['AuthorisationNo'],
                'Notes' => empty($args['Notes']) ? null : $args['Notes'],
                'JobSourceID' => empty($args['JobSourceID']) ? null : $args['JobSourceID'],
                'AgentRefNo' => empty($args['ReferralNo']) ? null : $args['ReferralNo'],
                'StockCode' => empty($args['HiddenStockCode']) ? null : $args['HiddenStockCode'],
                'ProductID' => $ProductID,
                'ReceiptNo' => empty($args['ReceiptNo']) ? null : $args['ReceiptNo'],
                'ColAddCompanyName' => empty($args['ColAddCompanyName']) ? null : $args['ColAddCompanyName'],
                'ColAddBuildingNameNumber' => empty($args['ColAddBuildingNameNumber']) ? null : $args['ColAddBuildingNameNumber'],
                'ColAddStreet' => empty($args['ColAddStreet']) ? null : $args['ColAddStreet'],
                'ColAddLocalArea' => empty($args['ColAddLocalArea']) ? null : $args['ColAddLocalArea'],
                'ColAddTownCity' => empty($args['ColAddCity']) ? null : $args['ColAddCity'],
                'ColAddCountyID' => empty($args['ColAddCountyID']) ? null : $args['ColAddCountyID'],
                'ColAddCountryID' => empty($args['ColAddCountryID']) ? null : $args['ColAddCountryID'],
                'ColAddPostcode' => empty($args['ColAddPostcode']) ? null : strtoupper($_POST['ColAddPostcode']),
                'ColAddEmail' => empty($args['ColAddEmail']) ? null : $args['ColAddEmail'],
                'ColAddPhone' => empty($args['ColAddPhone']) ? null : $args['ColAddPhone'],
                'ColAddPhoneExt' => empty($args['ColAddPhoneExt']) ? null : $args['ColAddPhoneExt'],
                'DateBooked' => date('Y-m-d'),
                'TimeBooked' => date('H:i:s'),
                'StatusID' => $statusID,
                'BookedBy' => $user->UserID,
                'ModifiedUserID' => $user->UserID,
                'ModifiedDate' => date('Y-m-d H:i:s'),
                'batchID' => empty($args['batchID']) ? null : $args['batchID'],
                'JobSourceID' => empty($args['JobSourceID']) ? null : $args['JobSourceID'],
                'RepairType' => $repair_type,
                'Accessories' => $Accessories,
                'UnitCondition' => $UnitCondition,
                'OpenJobStatus' => ((isset($args["isProductAtBranch"]) && $args["isProductAtBranch"] == "Yes") || (isset($args["ProductLocation"]) && $args["ProductLocation"] == "Branch")) ? "in_store" : null
            ];

            if (isset($args['ProductLocation'])) {

                if ($args['ProductLocation'] == "Service Provider") {
                    if (isset($args['ServiceProviderID'])) {
                        $spModel = $this->loadModel("ServiceProviders");
                        $sp = $spModel->getServiceProvider($args['ServiceProviderID']);
                        $job['ColAddCompanyName'] = empty($sp["CompanyName"]) ? null : $sp["CompanyName"];
                        $job['ColAddBuildingNameNumber'] = empty($sp["BuildingNameNumber"]) ? null : $sp["BuildingNameNumber"];
                        $job['ColAddStreet'] = empty($sp["Street"]) ? null : $sp["Street"];
                        $job['ColAddLocalArea'] = empty($sp["LocalArea"]) ? null : $sp["LocalArea"];
                        $job['ColAddTownCity'] = empty($sp["TownCity"]) ? null : $sp["TownCity"];
                        $job['ColAddCountyID'] = empty($sp["CountyID"]) ? null : $sp["CountyID"];
                        $job['ColAddCountryID'] = empty($sp["CountryID"]) ? null : $sp["CountryID"];
                        $job['ColAddPostcode'] = empty($sp["PostalCode"]) ? null : $sp["PostalCode"];
                        $job['ColAddEmail'] = empty($sp["ContactEmail"]) ? null : $sp["ContactEmail"];
                        $job['ColAddPhone'] = empty($sp["ContactPhone"]) ? null : $sp["ContactPhone"];
                        $job['ColAddPhoneExt'] = empty($sp["ContactPhoneExt"]) ? null : $sp["ContactPhoneExt"];
                    }
                } else if ($args['ProductLocation'] == "Customer") {
                    $job['ColAddCompanyName'] = empty($_POST['CompanyName']) ? null : $args['CompanyName'];
                    $job['ColAddBuildingNameNumber'] = empty($args['BuildingNameNumber']) ? null : $args['BuildingNameNumber'];
                    $job['ColAddStreet'] = empty($args['Street']) ? null : $args['Street'];
                    $job['ColAddLocalArea'] = empty($args['LocalArea']) ? null : $args['LocalArea'];
                    $job['ColAddTownCity'] = empty($args['City']) ? null : $args['City'];
                    $job['ColAddCountyID'] = empty($args['CountyID']) ? null : $args['CountyID'];
                    $job['ColAddCountryID'] = empty($args['CountryID']) ? null : $args['CountryID'];
                    $job['ColAddPostcode'] = empty($args['PostalCode']) ? null : strtoupper($args['PostalCode']);
                    $job['ColAddEmail'] = empty($args['ContactEmail']) ? null : $args['ContactEmail'];
                    $job['ColAddPhone'] = empty($args['ContactHomePhone']) ? null : $args['ContactHomePhone'];
                    //$job['ColAddPhoneExt'] = empty($args['ColAddPhoneExt']) ? null : $args['ColAddPhoneExt'];
                }
            }

            if (!empty($args['DateOfPurchase']) && $args['DateOfPurchase'] != 'dd/mm/yyyy') {
                $dateArray = explode("/", $args['DateOfPurchase']);
                if (count($dateArray) == 3) {
                    $job['DateOfPurchase'] = $dateArray[2] . '-' . $dateArray[1] . '-' . $dateArray[0];
                }
            }

            /*if ($appraisalRequired || $raRequired) {
                $branchModel = $this->loadModel("Branches");
                $args['ServiceProviderID'] = $branchModel->getBranchServiceProvider($branch_id);
                if ( $args['ServiceProviderID'] == 0 ) $job['ServiceProviderID'] = null; 
            }*/

            $result = $jobModel->create($job);

            if ($result['status'] == 'SUCCESS') {
                
                // Add Allocate Service Centre to result
                //$result['AllocatedServiceProvider'] = $ServiceProviderDetails;
               
                //Updating branch address starts here ...
                try {

                    if (isset($user->Permissions['AP7007']) || $user->SuperAdmin) {
                        if (!empty($branch_id) && isset($args['ProductLocation']) && $args['ProductLocation'] == "Branch") {
                            $branchAddress = [
                                'BuildingNameNumber' => empty($args['ColAddBuildingNameNumber']) ? null : $args['ColAddBuildingNameNumber'],
                                'Street' => empty($args['ColAddStreet']) ? null : $args['ColAddStreet'],
                                'LocalArea' => empty($args['ColAddLocalArea']) ? null : $args['ColAddLocalArea'],
                                'TownCity' => empty($args['ColAddCity']) ? null : $args['ColAddCity'],
                                'CountyID' => empty($args['ColAddCountyID']) ? null : $args['ColAddCountyID'],
                                'CountryID' => empty($args['ColAddCountryID']) ? null : $args['ColAddCountryID'],
                                'PostalCode' => empty($args['ColAddPostcode']) ? null : $args['ColAddPostcode'],
                                'ContactEmail' => empty($args['ColAddEmail']) ? null : $args['ColAddEmail'],
                                'ContactPhoneExt' => empty($args['ColAddPhoneExt']) ? null : $args['ColAddPhoneExt'],
                                'ContactPhone' => args($args['ColAddPhone']) ? null : $args['ColAddPhone'],
                                'BranchID' => $branch_id
                            ];
                            $branches_model = $this->loadModel('Branches');
                            $branches_model->updateAddress($branchAddress);
                        }
                    }
                } catch (Exception $e) {

                    $this->log($messages['Errors']['job_booking_branch_subject'] . ":");
                    $this->log($e->getMessage());
                    
                    $emailModel->Send( $this->controller->config['SMTP']['FromName'], 
                                       $this->controller->config['SMTP']['From'], 
                                       $this->controller->config['General']['skyline_issue_email'], 
                                       $messages['Errors']['job_booking_branch_subject'], 
                                       $e->getMessage() );
                }

                //Updating Unit type for given model number..-starts here..
                try {

                    if (!empty($args['UpdateModelUnitType']) ) {

                        $models_model = $this->loadModel('Models');
                        $models_model->UpdateModel(array("ModelID" => $ModelID, "UnitTypeID" => $args['UpdateModelUnitType']));

                        if (!isset($user->Permissions['AP7040'])) {

                            //Sending email to admin if unit type has been assigned to model no.
                            $this->log("New Unit Type has been assigned to the Model Number:");
                            $this->log("ModelID:" . $ModelID . " and UnitTypeID:" . $_POST['UpdateModelUnitType'] . ".  Please correct it if it is wrong.");

                            $emailModel->Send( $this->controller->config['SMTP']['FromName'], 
                                               $this->controller->config['SMTP']['From'], 
                                               $this->controller->config['General']['skyline_issue_email'], 
                                               "New Unit Type has been assigned to the Model Number", 
                                               "ModelID:" . $ModelID . " and UnitTypeID:" . $args['UpdateModelUnitType'] . ".  Please correct it if it is wrong." );
                        }
                    }
                } catch (Exception $e) {

                    $this->log($messages['Errors']['update_model_unittype_subject'] . ":");
                    $this->log(var_export($e->getMessage(), true));
                    
                    $emailModel->Send( $this->controller->config['SMTP']['FromName'], 
                                       $this->controller->config['SMTP']['From'], 
                                       $this->controller->config['General']['skyline_issue_email'], 
                                       $messages['Errors']['update_model_unittype_subject'], 
                                       $e->getMessage() );
                }

                //Requesting new unit type name. - starts here..
                try {

                    if (!empty($args['RequestNewUnitTypeText'])) {

                        //Sending email to admin for New Unit type needed in Skyline.
                        $this->log("New Unit type needed in Skyline");
                        $NetworkName = $skyline->getNetworkName($args['NetworkID']);
                        $NetworkName = $NetworkName . ' (NetworkID: ' . $args['NetworkID'] . ')';
                        $ClientName = $skyline->getClientName($args['ClientID']);
                        $ClientName = $ClientName . ' (ClientID: ' . $args['ClientID'] . ')';
                        $RequestNewUnitTypeMailBody = 'User <b>' . $user->UserID . 
                            '</b> from <b>' . $NetworkName . '</b> - <b>' . $ClientName . '</b> has requested the following unit type to be added to Skyline.<br>
			    New Unit Type requested: <b>' . $args['RequestNewUnitTypeText'] . '</b><br>
			    Job: <b>' . $result['jobId'] . '</b>';

                        $this->log($RequestNewUnitTypeMailBody);

                        $emailModel->Send( $this->controller->config['SMTP']['FromName'], 
                                           $this->controller->config['SMTP']['From'], 
                                           $this->controller->config['General']['skyline_issue_email'], 
                                           'New Unit type needed in Skyline', 
                                           $RequestNewUnitTypeMailBody );
                    }
                } catch (Exception $e) {

                    $this->log("Error in New Unit type needed in Skyline:");
                    $this->log($e->getMessage());

                    $emailModel->Send( $this->controller->config['SMTP']['FromName'], 
                                       $this->controller->config['SMTP']['From'], 
                                       $this->controller->config['General']['skyline_issue_email'], 
                                       "Error in New Unit type needed in Skyline", 
                                       $e->getMessage() );
                }

                // Write Status History / Audit Trail / Credit Deduction....
                try {

                    if ($branch_id == null) {
                        $mail_subject = "ClientID: " . $args['ClientID'] . " missing Default Assigned Branch. Job No: SL" . $result['jobId'];
                        $emailModel->Send( $this->controller->config['SMTP']['FromName'], 
                                           $this->controller->config['SMTP']['From'], 
                                           $this->controller->config['General']['skyline_issue_email'], 
                                           $mail_subject, 
                                           $mail_subject );
                        $this->log("Email Sent:");
                        $this->log($this->controller->config['General']['skyline_issue_email']);
                        $this->log($mail_subject);
                    }

                    //Create Status history Record
                    $status_history = $this->loadModel('StatusHistory');
                    $status_history->create([ 'JobID' => $result['jobId'],
                                              'StatusID' => $statusID,
                                              'UserID' => $user->UserID,
                                              'Date' => date('Y-m-d H:i:s') ]);

                    //Create Audit Record
                    $audit = $this->loadModel('Audit');
                    $audit_actionID = $skyline->getAuditTrailActionID(AuditTrailActions::JOB_CREATED);
                    $audit->create([ 'JobID' => $result['jobId'],
                                     'AuditTrailActionID' => $audit_actionID,
                                     'Description' => '' ]);

                    //Credit Deduction
                    $network = $this->loadModel('ServiceNetworks');
                    $CreditDetails = $network->CreditDeduction($args['NetworkID']);

                    if (is_array($CreditDetails)) {

                        if (isset($CreditDetails['CreditLevelSendMail']) && $CreditDetails['CreditLevelSendMail'] == 1) {
                            //To do: Send mail to $CreditDetails['ContactEmail'] if it is not already sent.
                            if (APPLICATION_PATH != "C:\wamp\www\skyline\public\application") {
                                $this->log("Send mail: credit level is less than record level");
                            }
                        }

                        if ($CreditDetails['CreditLevel'] < 0) {
                            //To do: Send mail to $CreditDetails['ContactEmail'] always.
                            if (APPLICATION_PATH != "C:\wamp\www\skyline\public\application") {
                                $this->log("Send mail: credit level is less than zero");
                            }
                        }
                    }
                } catch (Exception $e) {

                    $this->log($messages['Errors']['job_booking_credit_level'] . ":");
                    $this->log($e->getMessage());

                    $emailModel->Send( $this->controller->config['SMTP']['FromName'], 
                                       $this->controller->config['SMTP']['From'], 
                                       $this->controller->config['General']['skyline_issue_email'], 
                                       $messages['Errors']['job_booking_credit_level'], 
                                       $e->getMessage() );
                }

                // RAStatus...
                try {

                    $cur = $jobModel->fetch($result['jobId']);

                    //Checking if RA required and if yes - setting job.RepairAuthRequired = Yes
                    
                    if ($raRequired) {
                        $jobModel->setRAStatus($result['jobId'], "required");
                        $jobModel->setRAType($jobModel->current_record, "repair");
                        $brandModel = $this->loadModel("Brands");
                        $brand = $brandModel->getBrandByBranchID($jobModel->current_record["BranchID"]);
                        $jobModel->setRAStatusID($jobModel->current_record, "Authorisation Required", $brand["BrandID"]);
                    } else if (!empty($cur["AuthorisationNo"])) {
                        $jobModel->setRAStatus($result['jobId'], "confirmed");
                        $raRequired = false;
                    } else {
                        $jobModel->setRAStatus($result['jobId'], "not_required");
                    }
                    
                } catch (Exception $e) {

                    $this->log($messages['Errors']['job_booking_ra'] . ":");
                    $this->log($e->getMessage());

                    $emailModel->Send( $this->controller->config['SMTP']['FromName'], 
                                       $this->controller->config['SMTP']['From'], 
                                       $this->controller->config['General']['skyline_issue_email'], 
                                       $messages['Errors']['job_booking_ra'], 
                                       $e->getMessage() );
                }

                if ($appraisalRequired) {
                    //SSSS email needs to be sent here
                    //skipping RA sending emails, etc.
                    
                    
                    $emailModel = $this->loadModel('Email'); 
	            $emailModel->sendAppraisalRequestEmail($result['jobId']);

                   
                    
                    
                } else {

                    try {

                        //Sending service request details mail to customer based on auto send email flag set for brand - starts here...
                        if ($jobModel->current_record['AutoSendEmails'] == "1") {

                            //Setting up email templates to send multiple emails.
                            $emails = [];

                            if ($raRequired) {

                                //Notification about RA Request is sent to client if client email address and SP ID are present
                                if ($jobModel->current_record['ContactEmail'] && $args['ServiceProviderID']) {
                                    $emails[] = $emailModel->fetchRow(['EmailCode' => "ra_required_client"]);
                                    end($emails);
                                    $key = key($emails);
                                    $emails[$key]["toEmail"] = $cur["ContactEmail"];
                                    $emails[$key]["fromName"] = "Skyline";
                                    $emails[$key]["fromEmail"] = "no-reply@skylinecms.co.uk";
                                    $emails[$key]["replyEmail"] = ($cur['ServiceReplyEmail']) ? $cur['ServiceReplyEmail'] : false;
                                    $emails[$key]["replyName"] = ($cur['ServiceCentreName']) ? $cur['ServiceCentreName'] : false;
                                    $emails[$key]["ccEmails"] = false;
                                }

                                //Notification about RA Request is sent to manufacturer
                                $emails[] = $emailModel->fetchRow(['EmailCode' => "ra_required_manufacturer"]);
                                $manufacturerModel = $this->loadModel("Manufacturers");
                                $manufacturerEmail = $manufacturerModel->getManufacturer($cur["ManufacturerID"])["AuthorisationManagerEmail"];
                                $networkModel = $this->loadModel("ServiceNetworks");
                                $network = $networkModel->fetchRow(["NetworkID" => $cur["NetworkID"]]);
                                end($emails);
                                $key = key($emails);
                                $emails[$key]["toEmail"] = $manufacturerEmail;
                                $emails[$key]["fromName"] = "Skyline";
                                $emails[$key]["fromEmail"] = "no-reply@skylinecms.co.uk";
                                $emails[$key]["replyName"] = ($cur['ServiceCentreName']) ? $cur['ServiceCentreName'] : false;
                                $emails[$key]["replyEmail"] = ($cur['ServiceReplyEmail']) ? $cur['ServiceReplyEmail'] : false;
                                $emails[$key]["ccEmails"] = $network["NetworkManagerEmail"] . ":j.berry@pccsuk.com";
                                
                            } else {

                                //RA is not required, send a standard job booking email to client
                                //Notification about RA Request is sent to client if client email address and SP ID are present
                                if ($jobModel->current_record['ContactEmail'] && $args['ServiceProviderID']) {
                                    
                                    
                                    if($jobModel->current_record['EmailType']=='CRM')
                                    {    
                                        $emails[] = $emailModel->fetchRow(['EmailCode' => "job_booking_crm"]); 
                                    }
                                    else if($jobModel->current_record['EmailType']=='Tracker')
                                    {    
                                        $emails[] = $emailModel->fetchRow(['EmailCode' => "job_booking_smart_service_tracker"]); 
                                    }
                                    else
                                    {
                                        $emails[] = $emailModel->fetchRow(['EmailCode' => "job_booking"]);
                                    }
                                    
                                    
                                    end($emails);
                                    $key = key($emails);
                                    $emails[$key]["toEmail"] = $cur["ContactEmail"];
                                    $emails[$key]["fromName"] = "Skyline";
                                    $emails[$key]["fromEmail"] = "no-reply@skylinecms.co.uk";
                                    $emails[$key]["replyEmail"] = ($cur['ServiceReplyEmail']) ? $cur['ServiceReplyEmail'] : false;
                                    $emails[$key]["replyName"] = ($cur['ServiceCentreName']) ? $cur['ServiceCentreName'] : false;
                                    $emails[$key]["ccEmails"] = false;
                                }

                                //Sending Service Instruction email to service provider's admin supervisor - starts here..
                                if ($jobModel->current_record['SPEmailServiceInstruction'] == 'Active' && ($jobModel->current_record['SPAdminSupervisorEmail'] || $jobModel->current_record['SPServiceManagerEmail']) && $args['ServiceProviderID']) {

                                    $email_service_instruction = $emailModel->fetchRow(['EmailCode' => "email_service_instruction"]);

                                    if (is_array($email_service_instruction) && count($email_service_instruction) > 0) {
                                        $emails[] = $email_service_instruction;
                                        end($emails);
                                        $key = key($emails);
                                        
                                        
                                        $sp_send_email1 = ($jobModel->current_record['SPAdminSupervisorEmail'])?$jobModel->current_record['SPAdminSupervisorEmail']:false;
                                        $sp_send_email2 = false;
                                        if(!$sp_send_email1)
                                        {
                                          $sp_send_email1 =  $jobModel->current_record['SPServiceManagerEmail']; 
                                        }
                                        else if($jobModel->current_record['SPServiceManagerEmail'])
                                        {
                                            $sp_send_email2 = $jobModel->current_record['SPServiceManagerEmail'];
                                        }
                                        
                                        $emails[$key]["toEmail"] = $sp_send_email1;
                                        $emails[$key]["fromName"] = "Skyline";
                                        $emails[$key]["fromEmail"] = "no-reply@skylinecms.co.uk";
                                        $emails[$key]["replyEmail"] = false;
                                        $emails[$key]["replyName"] = false;
                                        $emails[$key]["ccEmails"] = $sp_send_email2;
                                    }
                                }
                            }

                            foreach ($emails as $email) {
                                $emailModel->processEmail($email, $cur);
                            }
                        }

                        //Sending email to admin if the Job is not assigned to service center  - starts here...
                        if (empty($args['ServiceProviderID'])) {

                            $UnallocatedJobEmailSubject = 'New Skyline Unallocated job ' . $jobModel->current_record["JobID"];
                            $UnallocatedJobEmailBody = 'A new job (' . $jobModel->current_record["JobID"] . ') has been booked onto Skyline which is currently unallocated, please visit the Unallocated jobs page to allocate it manually.';
                            $emailModel->Send( $this->controller->config['SMTP']['FromName'], 
                                               $this->controller->config['SMTP']['From'], 
                                               $this->controller->config['General']['skyline_issue_email'], 
                                               $UnallocatedJobEmailSubject, 
                                               $UnallocatedJobEmailBody, 
                                               $this->controller->config['General']['unallocated_jobs_email'] );
                        }
                        
                    } catch (Exception $e) {

                        $this->log($messages['Errors']['job_booking_sendemail'] . ":");
                        $this->log($e->getMessage());
                        
                        $emailModel->Send( $this->controller->config['SMTP']['FromName'], 
                                           $this->controller->config['SMTP']['From'], 
                                           $this->controller->config['General']['skyline_issue_email'], 
                                           $messages['Errors']['job_booking_sendemail'], 
                                           $e->getMessage() );
                    }

                    // call Service Centre API to transmit job details
                    try {

                        //Finally transmit allocated job to Service Centre.....
                        //If RA is not required then transmit, otherwise not.
                        if (!$raRequired) {
                            $jobModel->pushJobToSC( $result['jobId'] );
                        }
                        
                    } catch (Exception $e) {

                        $this->log($messages['Errors']['job_booking_api'] . ":");
                        $this->log($e->getMessage());

                        $emailModel->Send( $this->controller->config['SMTP']['FromName'], 
                                           $this->controller->config['SMTP']['From'], 
                                           $this->controller->config['General']['skyline_issue_email'], 
                                           $messages['Errors']['job_booking_api'], $e->getMessage() );
                    }
                    
                }
                
                if (isset($this->controller->session->BookCollectionFormData)) {
                    $bcf = $this->session->BookCollectionFormData;
                    $bcf['JobID'] = $result['jobId'];
                    $courier_business_model = $this->loadModel('CourierBusinessModel');
                    $courier_business_model->BookCourierCollection($bcf, false); 
                    unset($this->controller->session->BookCollectionFormData);
                }
                
                // gather a few job details for the job confirmation
                //$job_details = array();
                
                //$job_details['CustomerTitle'] = $cur['ContactTitle'];
                //$job_details['CustomerFirstName'] = $cur['ContactFirstName'];
                //$job_details['CustomerLastName'] = $cur['ContactLastName'];
                //$job_details['DateBooked'] = date('Y-m-d');
                //$job_details['ServiceType'] = $cur['ServiceTypeName'];
                //$job_details['Manufacturer'] = $cur['ManufacturerName'];               
                //$job_details['UnitType'] = $cur['ServiceBaseUnitType'];
                
                // and add to result...
                //$result['Job'] = $job_details;
            }

            return $result;
        } catch (Exception $e) {

            $this->log($messages['Errors']['job_booking_subject'] . ":");
            $this->log($e->getMessage());

            $emailModel->Send($this->controller->config['SMTP']['FromName'], $this->controller->config['SMTP']['From'], $this->controller->config['General']['skyline_issue_email'], $messages['Errors']['job_booking_subject'], $e->getMessage()
            );

            $result = [ 'status' => 'FAIL',
                'jobId' => 0,
                'message' => $messages['Errors']['job_booking']
            ];
        }
    }
    
    public function getTitles( $BrandID=Constants::SKYLINE_BRAND_ID ) {
    
        $model = $this->loadModel('SkylineQueriesModel');
        $titles = $model->getTitles(Constants::SKYLINE_BRAND_ID);
        
        if ($BrandID != Constants::SKYLINE_BRAND_ID) {
            
            $brand_titles = $model->getTitles($BrandID);
            foreach($titles as &$title) {
                foreach($brand_titles as $brand_title) {
                    if ($title['TitleCode'] == $brand_title['TitleCode']) {
                        $title = $brand_title;
                        break;
                    }
                }
            }
            
        }
            
        return $titles;
  
    }
    
    public function getServiceTypes( $JobTypeID, $BrandID=Constants::SKYLINE_BRAND_ID ) {
    
        $model = $this->loadModel('SkylineQueriesModel');
        $service_types = $model->getServiceTypes($JobTypeID, Constants::SKYLINE_BRAND_ID);
        
        if ($BrandID != Constants::SKYLINE_BRAND_ID) {
            
            $brand_service_types = $model->getServiceTypes($JobTypeID, $BrandID);
            foreach($service_types as &$service_type) {
                foreach($brand_service_types as $brand_service_type) {
                    if ($service_type['ServiceTypeCode'] == $brand_service_type['ServiceTypeCode']) {
                        $service_type = $brand_service_type;
                        break;
                    }
                }
            }
            
        }
            
        return $service_types;
  
    }
   

}

?>
