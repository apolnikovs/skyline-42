<?php

/**
 * Short Description of LoginController.
 * 
 * Long description of LoginController.
 *
 * @author     Brian Etherington 
 * @copyright  2012 PC Control systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.0
 */

require_once('CustomSmartyController.class.php');

class LoginController extends CustomSmartyController {
    
    public $config;  
    public $session;
    public $skyline;
    public $user;
    public $messages;
    
    private $lang = 'en';
    
    public function __construct() { 
        
        parent::__construct(); 
        
       /* ==========================================
        * Read Application Config file.
        * ==========================================
        */
        $this->config = $this->readConfig('application.ini');
        
       /* ==========================================
        *  Initialise Session Model
        * ==========================================
        */
        $this->session = $this->loadModel('Session'); 
        
        /* ==========================================
         *  Initialise Messages Model
         * ==========================================
         */
        $this->messages = $this->loadModel('Messages'); 
        
        //$this->smarty->assign('_theme', 'skyline');
        
        
        /* ==========================================
         *  Initialise User Class
         * ==========================================
         */
        
        if(isset($this->session->UserID)) {
            
            $user_model = $this->loadModel('Users');
            $this->user = $user_model->GetUser( $this->session->UserID );
            
            $this->smarty->assign('name',$this->user->Name);
            $this->smarty->assign('session_user_id', $this->session->UserID);
            $this->smarty->assign('_theme', $this->user->Skin);
            
            if($this->user->BranchName) {
                $this->smarty->assign('logged_in_branch_name', " - ".$this->user->BranchName." ".$this->config['General']['Branch']." ");
            } else {
                $this->smarty->assign('logged_in_branch_name', "");
            }
            
            if($this->session->LastLoggedIn) {
                $this->smarty->assign('last_logged_in', " - ".$this->config['General']['LastLoggedin']." ".date("jS F Y G:i", $this->session->LastLoggedIn));
            } else {
                $this->smarty->assign('last_logged_in', '');
            } 
            
            $topLogoBrandID = $this->user->DefaultBrandID;
            
        } else {
            
	    $topLogoBrandID = isset($_COOKIE['brand']) ? $_COOKIE['brand'] : 0;
            
	    if(isset($_COOKIE["brand"]) && $_COOKIE["brand"] == 2030) {
		$secondaryLogo = "samsung_secondary.png";
		$this->smarty->assign("secondaryLogo", $secondaryLogo);
	    }
	    
	    $brandModel = $this->loadModel("Brands");
	    $brand = $brandModel->getBrandByID($topLogoBrandID);
	    
            $this->smarty->assign('session_user_id', '');
            $this->smarty->assign('name', '');
            $this->smarty->assign('last_logged_in', '');  
            $this->smarty->assign('logged_in_branch_name', '');
            if(isset($brand["Skin"]) && $brand["Skin"] != NULL && $brand["Skin"] != "") {
		$this->smarty->assign('_theme', $brand["Skin"]);
	    } else {
		$this->smarty->assign('_theme', 'skyline');
	    }
            
        }
        
        $this->skyline = $this->loadModel('Skyline');
        if($topLogoBrandID) {
            $topBrandLogo = $this->skyline->getBrand($topLogoBrandID);
            $this->smarty->assign('_brandLogo', (isset($topBrandLogo[0]['BrandLogo']))?$topBrandLogo[0]['BrandLogo']:'');
            $this->smarty->assign('showTopLogoBlankBox', false);
        } else {
            $this->smarty->assign('_brandLogo', '');
            $this->smarty->assign('showTopLogoBlankBox', true);
        }
        
        if(isset($this->session->lang)) {
            $this->lang = $this->session->lang;
        }
            
      
    }
       
    
    
    public function indexAction($args) { 
        
        // make sure logged out....
        $this->clearLogin(); 
        
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            
            $branch   = isset($_POST['branch'])?$_POST['branch']:0;
            $username = $_POST['name'];
            $pwd      = $_POST['password'];
            
            $user_model = $this->loadModel('Users');
            
            $this->user = $user_model->CheckUser($username, $pwd);          
                       
            if(isset($this->user->AuthOK) && $this->user->AuthOK == 1) {

                $this->login($branch, $this->user, $pwd, $this->user->DefaultBrandID);
                /* Tracker Base Log 313 - Raju on 4th June 2013 Starts Here - Service Provider User Redirects Online Dairy Page if Online Dairy is Active */
                if(isset($this->user->ServiceProviderID) && $this->user->ServiceProviderID > 0)
                {
                    $serviceProvider_model = $this->loadModel('ServiceProviders');
                    $spData = $serviceProvider_model->fetchRowById($this->user->ServiceProviderID);
                    if($spData['OnlineDiary'] == "Active" && $spData['DiaryType'] == "FullViamente")
                    {
                        $arrayHomeRedirect = array('AP12003','AP12007','P7003','AP7008','AP7009','AP7015','AP7023','AP7024','AP7026','AP7027','AP7028','AP7042');
                        $spAccessArray = array('AP7041','AP7011','AP11006');
                        $redirectHome = false;
                        foreach($this->user->Permissions as $key=>$val)
                        {
                            if(in_array($key, $arrayHomeRedirect))
                            {
                                $redirectHome = true;
                                break;
                            }
                        }
                        if(!$redirectHome)
                        {
                            $count = 0;
                            foreach($this->user->Permissions as $key=>$val)
                            {
                                if(in_array($key, $spAccessArray))
                                    $count++;
                            }
                            if($count == 3)
                            {
                                $this->redirect('Diary');
                            }
                        }
                    }
                }
                /* Tracker Base Log 313 - Raju on 4th June 2013 Ends Here */
                if($this->user->SuperAdmin) {
                    $this->redirect('SystemAdmin');
                } else {    
                    $this->redirect('index');
                }
                
            } else {
                    
                $login_attempts = (int)$this->session->LoginAttempts;   
                $maxLoginAttempts = $this->skyline->maxLoginAttempts();
                    
                if($maxLoginAttempts == 0 || $login_attempts < $maxLoginAttempts) {
                        
                    $this->session->LoginAttempts = $login_attempts + 1;
                    
                    if(isset($args['page'])) {
                        $this->showLoginForm(1004);
                    } else {    
                        $this->redirect('index/index/error=1004');
                    }
                        
                } else {

                    $this->smarty->assign('maxLoginAttempts', $maxLoginAttempts);
                    $this->smarty->assign('error', '');
                    $this->smarty->assign('page', $this->messages->getPage('accessDenied',$this->lang));
                    $this->smarty->display('accessDenied.tpl');
		    
                }
                    
            }
        
        } else {
           
            if (!empty($args['skin'])) {
                $this->smarty->assign('_theme', $args['skin']);    
            }
            $this->session->LoginAttempts = 0;
            $this->showLoginForm();
  
        }

    }
    
    
    
    public function logoutAction (  $args ) {
	
        $this->clearLogin();
        
        $this->session->LoginAttempts = 0;
        //$this->redirect('DefaultController','indexAction', $args );
        $this->redirect('index');
    
    }   
    
    
    
    public function newuserAction( $args ) {
        
        $this->skyline = $this->loadModel('Skyline');
        
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            
            switch ($_POST['function']) {
                
                 case 'login':
                     
                    // TODO
                    // update user details
                     
                    $user_model = $this->loadModel('Users');
                    $temp_user = $user_model->CheckTemporaryPassword($_POST['tempass']);
                    
                    if ($temp_user) {
                        
                        $branches = $this->skyline->getBranches();
                        $questions = $this->skyline->getSecurityQuestions();
                        
                        $this->smarty->assign('branches',$branches);
                        $this->smarty->assign('questions',$questions);
                        $this->smarty->assign('error','');
                        $this->smarty->assign('page',$this->messages->getPage('registration',$this->lang));
                        $this->smarty->display('registration.tpl');
                        
                    } else {
                     
                        $new_user_attempts = (int) $this->session->NewUserAttempts;                 
                        $maxLoginAttempts = $this->skyline->maxLoginAttempts();
                    
                        if ($maxLoginAttempts == 0 || $new_user_attempts < $maxLoginAttempts) {
                        
                            $this->session->NewUserAttempts = $new_user_attempts + 1;
                            $this->showNewUserForm(1004);
                    
                        } else {

                            $this->smarty->assign('maxLoginAttempts',$maxLoginAttempts);
                            $this->smarty->assign('error','');
                            $this->smarty->assign('page',$this->messages->getPage('accessDenied',$this->lang));
                            $this->smarty->display('accessDenied.tpl');
                        
                        }
                    }
                    
                     break;
                 
                 case 'problem':

                    // TODO
                    // update user details
            
                    $this->redirect('DefaultController','indexAction', $args );
                    
                     break;
                 
                 default:
                     
                     throw new Exception('Unrecognised function: '.$_POST['function']);
                     
            }           
            
        } else {
            
            $this->session->NewUserAttempts = 0;
            $this->showNewUserForm();
            
        }
        
    }
    
    public function registrationAction( $args ) {
        
        $this->redirect('DefaultController','indexAction', $args );
        
    }
    
    public function registrationQueryAction( $args ) {
        
        $this->skyline = $this->loadModel('Skyline');
        
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            
            // TODO:
            // send email TASK 9002
                       
            $this->session->LoginAttempts = 0;
            $this->redirect('DefaultController','indexAction', $args );
            
        } else {
            
            $branches = $this->skyline->getBranches();
            
            // get branch cookie if it is set....
            if (isset($_COOKIE['branch'])) {
                $this->smarty->assign('branch',$_COOKIE['branch']);
            } else {
                $this->smarty->assign('branch','');
            }
            
            $this->smarty->assign('branches',$branches);
            $this->smarty->assign('error','');
            $this->smarty->assign('page',$this->messages->getPage('registrationQuery',$this->lang));

            $this->smarty->display('registrationQuery.tpl');  
        }
        
    }
    
    public function lostUsernameAction( $args ) {
                
        $this->skyline = $this->loadModel('Skyline');
        
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            
            $temp_user_info = $this->loadModel('Users');
            $temp_user_info->lostUsername( $_POST );
            
            // TODO:
            // send email TASK 9002
                       
            $this->session->LoginAttempts = 0;
            #$this->redirect('DefaultController','indexAction', $args );
            
        } else {
            
            $branches = $this->skyline->getBranches();
            
            // get branch cookie if it is set....
            if (isset($_COOKIE['branch'])) {
                $this->smarty->assign('branch',$_COOKIE['branch']);
            } else {
                $this->smarty->assign('branch','');
            }
            
            $this->smarty->assign('branches',$branches);
            $this->smarty->assign('error','');
            $this->smarty->assign('page',$this->messages->getPage('lostUsername',$this->lang));

            $this->smarty->display('lostUsername.tpl');  
        }
        
    }
    
    /*
     * 
     */
    public function lostPasswordAction( $args  ) {
        $this->log('lostPasswordAction : '. var_export($_POST, true) );
        
        $user_model = $this->loadModel( 'Users' );
        
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            
            $user = $user_model->GetUser( $_POST['username'] );
            
            $this->smarty->assign('branch',$_POST['branch']);
            $this->smarty->assign('username',$user->Username);
                
            if ($user->SecurityAnswer == $_POST['answer']) {
                
                $this->smarty->assign('error','');
                $this->smarty->assign('page',$this->messages->getPage('resetPassword',$this->lang));
                $this->smarty->display('resetPassword.tpl');                 
                
            } else {
                
                $this->skyline = $this->loadModel('Skyline');
                
                $security_question_attempts = (int) $this->session->SecurityQuestionAttempts;                 
                $maxLoginAttempts = $this->skyline->maxLoginAttempts();
                
                if ($maxLoginAttempts == 0 || $security_question_attempts < $maxLoginAttempts) {
                    
                    $this->session->SecurityQuestionAttempts = $security_question_attempts + 1;
                    
                    $this->smarty->assign('security_question',$user->SecurityQuestion);
                    $this->smarty->assign('error','Answer incorrect: Please try again');
                    $this->smarty->assign('page',$this->messages->getPage('lostPassword',$this->lang));
                    $this->smarty->display('lostPassword.tpl');
                    
                } else {
                    
                    $this->smarty->assign('maxLoginAttempts',$maxLoginAttempts);
                    $this->smarty->assign('error','');
                    $this->smarty->assign('page',$this->messages->getPage('accessDenied',$this->lang));
                    $this->smarty->display('accessDenied.tpl');
                    
                }
                
            }
            
        } else {
            
            //$this->log( $args[1]  );
            
            
            $user = $user_model->GetUser( $args[1] );
        
            if ($user === false) {
            
                $this->skyline = $this->loadModel('Skyline');
                $this->showLoginForm(1001);
            
            } else {
                
                $this->session->SecurityQuestionAttempts = 0;

                $this->smarty->assign('branch',$args[0]);
                $this->smarty->assign('username',$user->Username);
                $this->smarty->assign('security_question',$user->SecurityQuestion);
                $this->smarty->assign('error','');
                $this->smarty->assign('page',$this->messages->getPage('lostPassword',$this->lang));
                $this->smarty->display('lostPassword.tpl'); 
     
            }
        
        }
        
    }
    
    public function resetPasswordAction( $args ) {
        $this->log('resetPasswordAction : '. var_export($_POST, true) );
        
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            throw new Exception('resetPasswordAction not POST');
        }
        
        $user_model = $this->loadModel( 'Users' );       
        $user = $user_model->ResetPassword( $_POST['username'], $_POST['pwd'] );
        
        $this->login($_POST['branch'], $user, $_POST['pwd']);       
        $this->redirect('DefaultController','indexAction', $args ); 
        
    }
    
    public function accessDeniedAction( $args ) {
        
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            throw new Exception('accessDeniedAction not POST');
        }
        
        // TODO:
        // send email TASK 9002
              
        //$this->log('accessDeniedAction POST');       
        $this->redirect('DefaultController','indexAction', $args );
        
    }
    
     /* ===========================================================
     * 
     * Private Methods
     * 
     * =========================================================== */  
        
    private function login($branch, $user, $pwd, $brand=0) {
     
        $this->session->UserID       = $user->Username;
        $this->session->Pwd          = $pwd;
        $this->session->LastLoggedIn = $user->LastLoggedIn;
        
        
                    
        // set branch cookie....
        setcookie("branch", $branch, time()+31536000, '/'); // expires 1 year
        setcookie("brand",  $brand, time()+31536000, '/'); // expires 1 year
                    
        /*if ($user->LoginType == 'C') { 
            $this->smarty->assign('name',$user->CallCentrename);
        } else if ($user->LoginType == 'R'){ 
            $this->smarty->assign('name',$user->RetailerName);
        } else {
            $this->smarty->assign('name','Noise UK'); 
        }*/
       
        $this->smarty->assign('name',$user->Name);
        $this->smarty->assign('last_logged_in', date("jS F Y G:i", $this->session->LastLoggedIn));
        
        
        $user_model = $this->loadModel( 'Users' );       
        $user_model->UpdateLastLoggedIn( $user->Username, date("Y-m-d H:i:s"));
        
       
    }
    
    private function clearLogin() {
        
        //unset($this->session->UserID);
        //unset($this->session->Pwd);
        //unset($this->session->LastLoggedIn);
        $this->session->clear();
        $this->smarty->assign('session_user_id','');
        $this->smarty->assign('name','');
        $this->smarty->assign('last_logged_in',''); 
        
    }
    
    private function showLoginForm($error = '') {

        $branches = $this->skyline->getBranches();
            
        // get branch cookie if it is set....
        if (isset($_COOKIE['branch'])) {
            $this->smarty->assign('branch',$_COOKIE['branch']);
        } else {
            $this->smarty->assign('branch','');
        }
            
        $this->smarty->assign('branches',$branches);
        $this->smarty->assign('page',$this->messages->getPage('login',$this->lang));
        $this->smarty->assign('error', is_string($error) ? $error : $this->messages->getError($error));
         
        $this->smarty->display('login.tpl');
        
    }
    
    private function showNewUserForm( $error='' ) {
        
            $branches = $this->skyline->getBranches();
            
            // get branch cookie if it is set....
            if (isset($_COOKIE['branch'])) {
                $this->smarty->assign('branch',$_COOKIE['branch']);
            } else {
                $this->smarty->assign('branch','');
            }
            
            $this->smarty->assign('branches',$branches);  
            $this->smarty->assign('error',is_string($error) ? $error : $this->messages->getError($error));
            $this->smarty->assign('page',$this->messages->getPage('newUser',$this->lang));
            
            $this->smarty->display('newUser.tpl');
    }
}

?>
