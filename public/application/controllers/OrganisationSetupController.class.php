<?php

require_once('CustomSmartyController.class.php');
require_once('Constants.class.php');

/**
 * Description
 *
 * This class handles all actions of Organisation Setup under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.05
 *   
 * Changes
 * Date        Version Author                Reason
 * ??/??/????  1.00    Nageswara Rao Kanteti Initial Version
 * 04/07/2012  1.01    Andrew J. Williams    Changes to allow editing of Brand ID in Brand Screen
 * 12/07/2012  1.02    Andrew J. Williams    Fixed Issue 13 - The company filter is not working properly in organisation setup/system users/
 * 12/07/2012  1.02A   Andrew J. Williams    Fixed Issue 4 - When we add new branch user, its not inserting network and client ids in the database
 * 16/07/2012  1.03    Andrew J. Williams    Fixed Issue 9 - We are using vat rates in service networks, clients and service providers pages. But the vat rates list is not filtered based on country.
 * 27/03/2013  1.04    Andris Polnikovs      Work started on Suppliers setup page
 * 13/05/2013  1.05    Andris Polnikovs      Added AutoReorderStock 
 * 01/07/2013          Thirumalesh Bandi      Added page title and page description dynamically for every page.
 ******************************************************************************/

class OrganisationSetupController extends CustomSmartyController {
    
    public $config;  
    public $session;
    public $skyline;
    public $messages;
    public $lang = 'en';
    public $page;
    public $SkylineBrandID=1000;
    public $statuses  = array(
                     
                     0=>array('Name'=>'Active', 'Code'=>'Active'),
                     1=>array('Name'=>'In-Active', 'Code'=>'In-active')
                    
                     );
    
    public $secondModel;
    
    
    
    public function __construct() { 
        
        parent::__construct(); 
        
       /* ==========================================
         * Read Application Config file.
         * ==========================================
        */
        
        $this->config = $this->readConfig('application.ini');
        
       /* ==========================================
         *  Initialise Session Model
         * ==========================================
         */
               
        $this->session = $this->loadModel('Session'); 
        
        /* ==========================================
         *  Initialise Messages Model
         * ==========================================
         */
               
        $this->messages = $this->loadModel('Messages'); 
        
        //$this->smarty->assign('_theme', 'skyline');
       
       
       if (isset($this->session->UserID)) {

            $user_model = $this->loadModel('Users');
            $this->user = $user_model->GetUser( $this->session->UserID );
	    $this->smarty->assign('loggedin_user', $this->user);

            $this->smarty->assign('name',$this->user->Name);
            $this->smarty->assign('session_user_id', $this->session->UserID);
            $this->smarty->assign('_theme', $this->user->Skin);
            
            if($this->user->BranchName)
            {
                $this->smarty->assign('logged_in_branch_name', " - ".$this->user->BranchName." ".$this->config['General']['Branch']." ");
            }
            else
            {
                $this->smarty->assign('logged_in_branch_name', "");
            }
            
            if($this->session->LastLoggedIn)
            {
                $this->smarty->assign('last_logged_in', " - ".$this->config['General']['LastLoggedin']." ".date("jS F Y G:i", $this->session->LastLoggedIn));
            }
            else
            {
                $this->smarty->assign('last_logged_in', '');
            } 
            
            $topLogoBrandID = $this->user->DefaultBrandID;

        } else {
            
            $topLogoBrandID = isset($_COOKIE['brand'])?$_COOKIE['brand']:0;
            
            $this->smarty->assign('session_user_id','');
            $this->smarty->assign('name','');
            $this->smarty->assign('last_logged_in','');
            $this->smarty->assign('logged_in_branch_name', '');
            $this->smarty->assign('_theme', 'skyline');
            $this->redirect('index',null, null);

        } 
        
        if($topLogoBrandID)
        {
            $skyline = $this->loadModel('Skyline');
            $topBrandLogo = $skyline->getBrand($topLogoBrandID);

            $this->smarty->assign('_brandLogo', (isset($topBrandLogo[0]['BrandLogo']))?$topBrandLogo[0]['BrandLogo']:'');
        }
        else
        {
            $this->smarty->assign('_brandLogo', '');
        }
        $this->smarty->assign('showTopLogoBlankBox', false);
        
         if (isset($this->session->lang)) {
            $this->lang = $this->session->lang;
        }
        
         
       
    }
       
    
    
    /*
    * Description
    * 
    * This method is used to process the data i.e fething, updating and inserting.
    * 
    * @param array $args It contains modelname as first element where second element as process type
    * @return void It prints json encoded result where datatable using this data.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    * 
    * When the data is question relates to a brand (ie is using the brands model)
    * an additional test is undertaken to make sure that the BrandID is unique.
    * This is as a result of a change in the database making the BrandID user
    * editable rather than an auto increment.
    * @author Andrew Williams <a.williams@pccsuk.com>
    *  
    **************************************************************************/
    
    public function ProcessDataAction($args) {
            
	$modelName = isset($args[0]) ? $args[0] : '';
	$type = isset($args[1]) ? $args[1] : '';
         
	if($modelName && $modelName != '') {

	    $this->page = $this->messages->getPage(lcfirst($modelName), $this->lang);

	    $model = $this->loadModel($modelName);

	    if($type == 'fetch') {

		$_POST['firstArg'] = isset($args[2]) ? $args[2] : '';
		$_POST['secondArg'] = isset($args[3]) ? $args[3] : '';
		$result = $model->fetch($_POST);

	    } else {

		//$this->log(var_export($_FILES, true));
		if($modelName == "Clients" && isset($_POST['AutomaticallyCreateBrand'])) {    
		    $this->secondModel = $this->loadModel("Brands");
		}  

		if($modelName == "Brands") {				    /* Additional tests are needed for dealing with a brand */
		    if($_POST['BrandID'] != $_POST['NewBrandID']) {	    /* Check to see if we are changing the Brand ID fielkd */
			if($model->brandExists($_POST['NewBrandID'])) {	    /* Check if the requested new BrandID exists */
			    $result = [					    /* Return an appropriatre error message */
				'status' => 'ERROR',
				'message' => 'That Brand ID already exists. Brand IDs must be unique.'
			    ];
			    echo json_encode($result);
			    return;
			} /* fi $model->brandExists */
		    } /* fi $_POST['BrandID'] != $_POST['OldBrandID'] */
		} /* fi $modelName == "Brands" */

		if($modelName == "SystemUsers") {			    /* Additional processing required if we are dealing with systemData */
		    if($_POST['UserType'] == 'Branch') {		    /* If user is a branch */
			$branch_model = $this->loadModel('Branches');
			$output = $branch_model->getBranchClientNetwork($_POST['CompanyID']);    /* Get client and network */
			$_POST['NetworkID'] = $output['NetworkID'];
			$_POST['ClientID'] = $output['ClientID'];
		    }
		}

		$result = $model->processData($_POST);
	    }

	    echo json_encode( $result );

	}
	    
	return;
	    
    }
    
     
     
    public function indexAction( /*$args*/ ) { 

    }
      
      
      
    /*
    * Description
    * 
    * This method is used for to manuplate data of Service Networks Page.
    * 
    * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
    * @return void It prints data to the loaded template.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
    */
      
    public function serviceNetworksAction($args) { 
       
	$functionAction = isset($args[0]) ? $args[0] : '';
	$selectedRowId = isset($args[1]) ? $args[1] : '';
          
	$Skyline = $this->loadModel('Skyline');
	$vat_rates_model = $this->loadModel('VATRates');
          
	$countries = $Skyline->getCountriesCounties();
	$vatRates = $vat_rates_model->getCountryVatRates();
          
	$this->smarty->assign('statuses', $this->statuses);
	$this->smarty->assign('countries', $countries);
	$this->smarty->assign('vatRates', $vatRates);
	$this->smarty->assign('SupderAdmin', $this->user->SuperAdmin);	    //this value should be from user calss
	$this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss
	$this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));
                
	$accessErrorFlag = false;

	$this->page =  $this->messages->getPage('serviceNetworks', $this->lang);
        
                    //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('serviceNetworks');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
        
	$this->smarty->assign('page', $this->page);

	switch($functionAction) {

	    case 'insert': {

		//We are fetching data for the selected row where user clicks on copy button
		$datarow = [
		    'NetworkID' => '', 
		    'CompanyName' => '', 
		    'BuildingNameNumber' => '', 
		    'PostalCode' => '', 
		    'Street' => '', 
		    'LocalArea' => '',
		    'TownCity' => '',
		    'CountyID' => '',
		    'CountryID' => '',
		    'ContactPhone' => '',
		    'ContactPhoneExt' => '',
		    'ContactEmail' => '',
		    'CreditLevel' => '',
		    'ReorderLevel' => '',
		    'CreditPrice' => '',
		    'VATRateID' => '',
		    'VATNumber' => '',
		    'InvoiceCompanyName' => '',
		    'SageReferralNominalCode' => '',
		    'SageLabourNominalCode' => '',
		    'SagePartsNominalCode' => '',
		    'SageCarriageNominalCode' => '',
		    'Status' => 'Active',
		    "ServiceManagerForename" => "",
		    "ServiceManagerSurname" => "",
		    "AdminSupervisorForename" => "",
		    "AdminSupervisorSurname" => "",
		    "ServiceManagerEmail" => "",
		    "AdminSupervisorEmail" => "",
		    "NetworkManagerForename" => "",
		    "NetworkManagerSurname" => "",
		    "NetworkManagerEmail" => "",
		    "SendASCReport" => "No"
		];
                        
		//Checking user permissons to display the page.
		if($this->user->SuperAdmin) {
		    $accessErrorFlag = false;
		} else if(isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {
		    $accessErrorFlag = true;
		}

		$this->smarty->assign('datarow', $datarow);
		$this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
		$this->smarty->assign('accessErrorFlag', $accessErrorFlag);

		$htmlCode = $this->smarty->fetch('serviceNetworksForm.tpl');

                echo $htmlCode;

                break;
		
	    }


	    case 'update': {

		$args['NetworkID'] = $selectedRowId;
		$model = $this->loadModel('ServiceNetworks');

		$datarow = $model->fetchRow($args);

		$this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

		$this->smarty->assign('datarow', $datarow);

		//Checking user permissons to display the page.
		if($this->user->SuperAdmin) {
		    $accessErrorFlag = false;
		} else if(isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {
		    $accessErrorFlag = true;
		}    

		$this->smarty->assign('accessErrorFlag', $accessErrorFlag);

		$htmlCode = $this->smarty->fetch('serviceNetworksForm.tpl');

		echo $htmlCode;

		break;
		    
	    }
	    
	    
	    default: {
		
		$this->smarty->display('serviceNetworks.tpl'); 
		
	    }

	}
           
    }
       
    public function UpdateManufacturerAuthAction( /* $args */ ) {  
        
        $ServiceProviderID = $_POST['asc'] == '' ? null : $_POST['asc'];
        $auths = isset($_POST['auth']) ? $_POST['auth'] : array();
        $checks = $_POST['check'] ? $_POST['check'] : array();
        
        $model = $this->loadModel('ManufacturerServiceProvider');
        $list = $model->getManufacturersByServiceProvider( $ServiceProviderID );
        
        // set default DELETE action for each row to delete
        // remember original settings for Authorised and DataChecked
        // & unset Authorised and DataChecked values
        // because html form checkboxes only contain checked values
        foreach($list as &$row) {
            $row['mode']  = Constants::DELETE;
            $row['Authorised_orig'] = $row['Authorised'];
            $row['DataChecked_orig'] = $row['DataChecked'];
            $row['Authorised'] = 'No';
            $row['DataChecked'] = 'No';
        }

        // Update list with Authorised checkboxes
        foreach($auths as $auth) {
            $isFound = false;
            foreach($list as &$row) {
                if ( $row['ManufacturerID'] == $auth) {
                    $isFound = true;
                    $row['Authorised'] = 'Yes';
                    $row['mode'] = Constants::UPDATE;
                    break;
                }
            }
            if (!$isFound) {
                $list[] = array( 'ManufacturerID' => $auth,
                                 'ServiceProviderID' => $ServiceProviderID,
                                 'Authorised' => 'Yes',
                                 'DataChecked' => 'No',
                                 'mode' => Constants::CREATE
                               );
            }
        }
        
        // Update list with DataChecked checkboxes
        foreach($checks as $check) {
            $isFound = false;
            foreach($list as &$row) {
                if ( $row['ManufacturerID'] == $check) {
                    $isFound = true;
                    $row['DataChecked'] = 'Yes';
                    // Do not overwrite mode if new INSERT as a result of Authorised update check above.
                    if ($row['mode'] == Constants::DELETE) $row['mode'] = Constants::UPDATE;
                    break;
                }
            }
            if (!$isFound) {
                $list[] = array( 'ManufacturerID' => $check,
                                 'ServiceProviderID' => $ServiceProviderID,
                                 'Authorised' => 'No',
                                 'DataChecked' => 'Yes',
                                 'mode' => Constants::CREATE
                               );
            }
        }
        
        // Now check for updated rows that haven't changed and mark as no update required
        foreach($list as &$row) {
            if ( $row['mode'] == Constants::UPDATE
                 && $row['Authorised'] == $row['Authorised_orig']
                 && $row['DataChecked'] == $row['DataChecked_orig']) {
                $row['mode'] = Constants::NONE;
                
            }
        }
        
        $isError = $model->update( $list );
        
        if ($isError) {       
            $result =  ['status' => 'OK',
	                'message' => 'Data Updated OK'];
        } else {
             $result =  ['status' => 'ERROR',
	                'message' => 'Errors in Update'];           
        }
        
        echo json_encode( $result );
        
    }
       
     /**
     * Description
     * 
     * This method is used for to manuplate data of Clients Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
     public function clientsAction(  $args  ) { 

        $functionAction   = isset($args[0])?$args[0]:'';
        $selectedRowId     = isset($args[1])?$args[1]:'';

        $Skyline    = $this->loadModel('Skyline');
        $vat_rates_model = $this->loadModel('VATRates');
          
        $countries  = $Skyline->getCountriesCounties();
        $vatRates  = $vat_rates_model->getCountryVatRates();
        
        
        
        $networks          = $Skyline->getNetworks();
        $serviceProviders   = $Skyline->getServiceProviders();


        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('countries', $countries);
        $this->smarty->assign('vatRates', $vatRates);
        $this->smarty->assign('networks', $networks);
        $this->smarty->assign('serviceProviders', $serviceProviders);


        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin);//this value should be from user calss
        $this->smarty->assign('userPermissions', $this->user->Permissions);//this value should be from user calss
        $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));

        $accessErrorFlag = false;

        $this->page =  $this->messages->getPage('clients', $this->lang);
        
                            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('clients');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
        
        
        $this->smarty->assign('page', $this->page);

            switch( $functionAction ) {

                case 'insert':

                        //We are fetching data for the selected row where user clicks on copy button
                        $datarow = array(
                            
                            'ClientID'=>'', 
                            'NetworkID'=>'', 
                            'ClientName'=>'', 
                            'BuildingNameNumber'=>'', 
                            'PostalCode'=>'', 
                            'Street'=>'', 
                            'LocalArea'=>'',
                            'TownCity'=>'',
                            'CountyID'=>'',
                            'CountryID'=>'',
                            'ContactPhone'=>'',
                            'ContactPhoneExt'=>'',
                            'ContactEmail'=>'',
                            'AccountNumber'=>'',
                            'GroupName'=>'',
                            'ServiceProviderID'=>'',
                            'InvoiceNetwork'=>'',
                            'AutomaticallyCreateBrand'=>'No',
                            'AutomaticallyCreateBranch'=>'No',
                            'VATRateID'=>'',
                            'VATNumber'=>'',
                            'InvoicedCompanyName'=>'',
                            'InvoicedPostalCode'=>'',
                            'InvoicedBuilding'=>'',
                            'InvoicedStreet'=>'',
                            'InvoicedTownCity'=>'',
                            'InvoicedArea'=>'',
                            'PaymentDiscount'=>'',
                            'PaymentTerms'=>'',
                            'VendorNumber'=>'',
                            'PurchaseOrderNumber'=>'',
                            'SageAccountNumber'=>'',
                            'SageNominalCode'=>'',
                            'EnableProductDatabase'=>'No',
			    'EnableCatalogueSearch'=>'No',
                            'Status'=>'Active',
                            'DefaultTurnaroundTime' => '',
                            'ForceReferralNumber'=>'No'
                            );
                        

                        //Checking user permissons to display the page.
                        if($this->user->SuperAdmin)
                        {
                            $accessErrorFlag = false;
                        }
                        else if(isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002']))
                        {
                            
                            $accessErrorFlag = true;
                            
                        }

                        $this->smarty->assign('datarow', $datarow);
                        $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                        $this->smarty->assign('accessErrorFlag', $accessErrorFlag);



                        $htmlCode = $this->smarty->fetch('clientsForm.tpl');

                        echo $htmlCode;

                    break;


                case 'update':

                        $args['ClientID']  = $selectedRowId;
                        $model              = $this->loadModel('Clients');

                        $datarow  = $model->fetchRow($args);
                        
                        $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

                        $this->smarty->assign('datarow', $datarow);

                        //Checking user permissons to display the page.
                        if($this->user->SuperAdmin)
                        {
                            $accessErrorFlag = false;
                        }
                        else if(isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002']))
                        {
                             $accessErrorFlag = true;
                        }    
                   

                        $this->smarty->assign('accessErrorFlag', $accessErrorFlag);


                        $htmlCode = $this->smarty->fetch('clientsForm.tpl');

                    echo $htmlCode;

                    break;

                default:

                    $this->smarty->assign('nId', $functionAction);
                    $this->smarty->display('clients.tpl'); 

                }
           
       }
      
     
       
       
       
       
       
       
       
       
      /**
     * Description
     * 
     * This method is used for to manuplate data of Brands Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
       public function brandsAction(  $args  ) { 
       
       
          $functionAction   = isset($args[0])?$args[0]:'';
          $selectedRowId     = isset($args[1])?$args[1]:'';
          
          
          if($functionAction=="uploadImage")//Uploading image...
          {
              
                    //$this->log(var_export($_FILES, true));
              
                    $error = "";
                    $msg = "";
                    $fileElementName = 'BrandLogo';
                    if(!empty($_FILES[$fileElementName]['error']))
                    {
                            switch($_FILES[$fileElementName]['error'])
                            {

                                    case '1':
                                            $error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
                                            break;
                                    case '2':
                                            $error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                                            break;
                                    case '3':
                                            $error = 'The uploaded file was only partially uploaded';
                                            break;
                                    case '4':
                                            $error = 'No file was uploaded.';
                                            break;

                                    case '6':
                                            $error = 'Missing a temporary folder';
                                            break;
                                    case '7':
                                            $error = 'Failed to write file to disk';
                                            break;
                                    case '8':
                                            $error = 'File upload stopped by extension';
                                            break;
                                    case '999':
                                    default:
                                            $error = 'No error code avaiable';
                            }
                    }elseif(empty($_FILES[$fileElementName]['tmp_name']) || $_FILES[$fileElementName]['tmp_name'] == 'none')
                    {
                            $error = 'No file was uploaded..';
                    }
                    else 
                    {                   
                                    
                                     $brandLogosDirPath = APPLICATION_PATH."/../".$this->config['Path']['brandLogosPath'];
                        
                                      $tmp_name = $_FILES[$fileElementName]["tmp_name"];
                                      $imgName  = $_FILES[$fileElementName]["name"];
                                     
                                      move_uploaded_file($tmp_name, $brandLogosDirPath.$imgName);
                                      
                                      
                                      $imgNameArray = explode(".", $_FILES[$fileElementName]["name"]);
                                      $imgExt = strtolower($imgNameArray[count($imgNameArray)-1]);
                                      
                                      $imgNewName = $selectedRowId.".".$imgExt;
                                      
                                      @rename ( $brandLogosDirPath.$imgName , $brandLogosDirPath.$imgNewName);
                                           
                                      $msg .= $imgName;
                        
                                   // $msg .= " File Name: " . $_FILES[$fileElementName]['name'] . ", ";
                                   // $msg .= " File Size: " . @filesize($_FILES[$fileElementName]['tmp_name']);

                                    //for security reason, we force to remove all uploaded file
                                    //@unlink($_FILES['fileToUpload']);		
                    }		
                    echo "{";
                    echo				"error: '" . $error . "',\n";
                    echo				"msg: '" . $msg . "'\n";
                    echo "}";
              
              
              
              
              
          }
          else
          {
              

            $nClients  = array();


            $Skyline               = $this->loadModel('Skyline');
            $brands_model          = $this->loadModel('Brands');
            $networks              = $Skyline->getNetworks();
            $allNetworkClients     = $Skyline->getNetworkClients();

            $SMSModel              = $this->loadModel('SMS');
            $smsMessages           = $SMSModel->fetchAll();

            $this->smarty->assign('smsMessages', $smsMessages);
            $this->smarty->assign('statuses', $this->statuses);
            $this->smarty->assign('allNetworkClients', $allNetworkClients);
            $this->smarty->assign('networks', $networks);
            $this->smarty->assign('UploadedBrandLogo', rand(100,10000));
            $this->smarty->assign('brandLogosPath', $this->config['Path']['brandLogosPath']);
            
            

            $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin);//this value should be from user calss
            $this->smarty->assign('userPermissions', $this->user->Permissions);//this value should be from user calss
            $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));

            $accessErrorFlag = false;



                $this->page =  $this->messages->getPage('brands', $this->lang);
                
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('brands');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
        
                
                $this->smarty->assign('page', $this->page);






                switch( $functionAction ) {

                    case 'insert':

                            //We are fetching data for the selected row where user clicks on copy button
                            $datarow = array(
                                
                                'NewBrandID'=>$brands_model->getNextBrandID(),
                                'BrandID'=>'',
                                'ClientID'=>'', 
                                'NetworkID'=>'', 
                                'BrandName'=>'',
                                'Acronym'=>'',
                                'BrandLogo'=>'', 
                                'AutoSendEmails'=>1,
//                                'SendRepairCompleteTextMessage'=>'No',
//                                'SMSID'=>'',
                                'EmailType'=>'Generic',  
                                'TrackerURL'=>'',
                                'Status'=>'Active'

                                );


                            //Checking user permissons to display the page.
                            if($this->user->SuperAdmin)
                            {
                                $accessErrorFlag = false;
                            }
                            else if(isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002']))
                            {

                                $accessErrorFlag = true;

                            }

                            $this->smarty->assign('datarow', $datarow);
                            $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                            $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
                            $this->smarty->assign('nClients', $nClients);


                            $htmlCode = $this->smarty->fetch('brandsForm.tpl');

                            echo $htmlCode;

                        break;


                    case 'update':

                            $args['BrandID']  = $selectedRowId;
                            $model            = $this->loadModel('Brands');

                            $datarow  = $model->fetchRow($args);
                            $datarow['NewBrandID']  = $selectedRowId;

                            $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

                            $this->smarty->assign('datarow', $datarow);

                            //Checking user permissons to display the page.
                            if($this->user->SuperAdmin)
                            {
                                $accessErrorFlag = false;
                            }
                            else if(isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002']))
                            {
                                $accessErrorFlag = true;
                            }    


                            $this->smarty->assign('accessErrorFlag', $accessErrorFlag);

                            if(isset($datarow['NetworkID']))
                            {
                                if(isset($allNetworkClients[$datarow['NetworkID']]))
                                {
                                    $nClients =  $allNetworkClients[$datarow['NetworkID']];
                                }
                            }
                            $this->smarty->assign('nClients', $nClients);    
                            $htmlCode = $this->smarty->fetch('brandsForm.tpl');

                        echo $htmlCode;

                        break;

                    default:


                        if($functionAction)
                        {
                            if(isset($allNetworkClients[$functionAction]))
                            {
                            $nClients =  $allNetworkClients[$functionAction];
                            }
                        }



                        $this->smarty->assign('nClients', $nClients);
                        $this->smarty->assign('nId', $functionAction);
                        $this->smarty->assign('cId', $selectedRowId);
                        $this->smarty->display('brands.tpl'); 

                    }
            }
       }
       
       
       
    /**
    * Description
    * 
    * This method is used for to manuplate data of Branches Page.
    * 
    * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
    * @return void It prints data to the loaded template.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
    */
       
    public function branchesAction($args) { 
       
	$functionAction = isset($args[0]) ? $args[0] : "";
	$selectedRowId = isset($args[1]) ? $args[1] : "";
          
	if ($functionAction == "getBrands") {
          
	    $BranchID = $selectedRowId;
            
	    if ($BranchID) {
		$Skyline = $this->loadModel("Skyline");
		$allBranchs = $Skyline->getBranchBrands($BranchID);
		echo json_encode($allBranchs);
	    }
            
	} else if ($functionAction == "getBrandSMS") {
	    
	    $BrandID = $selectedRowId;
            
	    if($BrandID == "") {
		$BrandID = false;
	    }    

	    $SMSModel = $this->loadModel("SMS");
	    $smsMessages = $SMSModel->fetchAll($BrandID);

	    echo json_encode( $smsMessages );
              
	} else {
          
	    $selectedRowId = isset($args[1]) ? $args[1] : false;
	    $thirdArg = isset($args[2]) ? $args[2] : false;
            
            $nClients = [];
            $cBrands = [];

            $Skyline = $this->loadModel("Skyline");
            $networks = $Skyline->getNetworks();
            $allNetworkClients = $Skyline->getNetworkClients();
            $countries = $Skyline->getCountriesCounties();
            $allClientBrands = $Skyline->getClientBrands();

            $branchTypes = [
		0 => ["BranchTypeID" => "Store", "Name" => "Store"],
		1 => ["BranchTypeID" => "Extra", "Name" => "Extra"],
		2 => ["BranchTypeID" => "Call Centre", "Name" => "Call Centre"],
		3 => ["BranchTypeID" => "Website", "Name" => "Website"]
            ];
            asort($branchTypes);
            $product_location_rows = $Skyline->getProductLocations(true);
            $this->smarty->assign("product_location_rows", $product_location_rows);
            
            $this->smarty->assign("statuses", $this->statuses);
            $this->smarty->assign("countries", $countries);
            $this->smarty->assign("branchTypes", $branchTypes);
            $this->smarty->assign("allClientBrands", $allClientBrands);
            $this->smarty->assign("allNetworkClients", $allNetworkClients);
            $this->smarty->assign("networks", $networks);

            $this->smarty->assign("SupderAdmin", $this->user->SuperAdmin);
            $this->smarty->assign("userPermissions", $this->user->Permissions);
            $this->smarty->assign("accessDeniedMsg", $this->messages->getError(1023, "default", $this->lang));

            $accessErrorFlag = false;

	    $spModel = $this->loadModel("ServiceProviders");
	    $serviceProviders = $spModel->getAllServiceProviders();
	    $this->smarty->assign("serviceProviders", $serviceProviders);
	    
            $this->page = $this->messages->getPage("branches", $this->lang);
            
            //Getting Page title & Description from database
            $model = $this->loadModel("GroupHeadings");
            $groupHedingResults = $model->getHeadingPageDetails("branches");

            //Over write the page title & description
            $page["Text"]["page_title"] = isset($groupHedingResults[0]) ? $groupHedingResults[0] : $page["Text"]["page_title"];
            $page["Text"]["description"] = isset($groupHedingResults[1]) ? $groupHedingResults[1] : $page["Text"]["description"];
        
            $this->smarty->assign("page", $this->page);

            $this->smarty->assign("User", $this->user);


	    switch($functionAction) {

		case "insert": {

		    //We are fetching data for the selected row where user clicks on copy button
		    $datarow = [
			"BranchID" => "", 
			"ClientID" => ($this->user->ClientID) ? $this->user->ClientID : $thirdArg, 
			"NetworkID" => ($this->user->NetworkID) ? $this->user->NetworkID : $selectedRowId, 
			"BranchName" => "", 
			"BuildingNameNumber" => "",
			"Street" => "",
			"LocalArea" => "", 
			"TownCity" => "", 
			"CountyID" => "", 
			"CountryID" => "", 
			"PostalCode" => "",
			"ServiceManager" => "",
			"ContactEmail" => "",
			"ContactPhoneExt" => "",
			"ContactPhone" => "",
			"ContactFax" => "",
			"BranchNumber" => "",
			"AccountNo" => "",
			"BranchType" => "",
			"BrandID" => [],
			"Status" => "Active",
			"CurrentLocationOfProduct" => "",
			"OriginalRetailerFormElement" => "FreeText",
			"SendRepairCompleteTextMessage" => "No",
			"SMSID" => "",
			"OpenJobsManagement" => "No"
		    ];


		    //Checking user permissons to display the page.
		    /*if($this->user->SuperAdmin) {
			$accessErrorFlag = false;
		    } else if (isset($this->user->Permissions["AP7001"]) || isset($this->user->Permissions["AP7002"])) {
			$accessErrorFlag = true;
		    }*/
                        
		    if (isset($datarow["NetworkID"])) {
			if (isset($allNetworkClients[$datarow["NetworkID"]])) {
			    $nClients = $allNetworkClients[$datarow["NetworkID"]];
			}
		    }

		    if (isset($datarow["ClientID"])) {
			if(isset($allClientBrands[$datarow["ClientID"]])) {
			    $cBrands = $allClientBrands[$datarow["ClientID"]];
			}
		    }

		    $this->smarty->assign("datarow", $datarow);
		    $this->smarty->assign("form_legend", $this->page["Text"]["insert_page_legend"]);
		    $this->smarty->assign("accessErrorFlag", $accessErrorFlag);
		    $this->smarty->assign("nClients", $nClients);
		    $this->smarty->assign("cBrands", $cBrands);
		    $this->smarty->assign("smsMessages", []);
                            
		    $htmlCode = $this->smarty->fetch("branchesForm.tpl");

		    echo $htmlCode;

		    break;
		}

		case "update": {

		    $args["BranchID"] = $selectedRowId;
		    $model = $this->loadModel("Branches");

		    $datarow = $model->fetchRow($args);

		    $this->smarty->assign("form_legend", $this->page["Text"]["update_page_legend"]);
		    $this->smarty->assign("datarow", $datarow);

		    //Checking user permissons to display the page.
		    /*if($this->user->SuperAdmin) {
			$accessErrorFlag = false;
		    } else if (isset($this->user->Permissions["AP7001"]) || isset($this->user->Permissions["AP7002"])) {
			$accessErrorFlag = true;
		    }    
		    */

		    $this->smarty->assign("accessErrorFlag", $accessErrorFlag);

		    if (isset($datarow["NetworkID"])) {
			if (isset($allNetworkClients[$datarow["NetworkID"]])) {
			    $nClients = $allNetworkClients[$datarow["NetworkID"]];
			}
		    }

		    if (isset($datarow["ClientID"])) {
			if (isset($allClientBrands[$datarow["ClientID"]])) {
			    $cBrands = $allClientBrands[$datarow["ClientID"]];
			}
		    }
                            
		    $SMSModel = $this->loadModel("SMS");
		    $smsMessages = [];

		    if ($datarow["BranchType"] == "Call Centre") {    
			$smsMessages = $SMSModel->fetchAll();
		    } else {
			if (count($cBrands)) {    
			    $smsMessages = $SMSModel->fetchAll($cBrands[0]["BrandID"]);
			}
		    }
 
		    $this->smarty->assign("nClients", $nClients);
		    $this->smarty->assign("cBrands", $cBrands);
                            
		    $this->smarty->assign("smsMessages", $smsMessages);
                            
		    $htmlCode = $this->smarty->fetch("branchesForm.tpl");

		    echo $htmlCode;

		    break;
		}
			
		default: {

		    if ($this->user->NetworkID) {
			$functionAction = $this->user->NetworkID;
		    }    

		    if ($this->user->ClientID) {    
			$selectedRowId = $this->user->ClientID;
		    }    
                           
                        
		    if ($functionAction) {
			if (isset($allNetworkClients[$functionAction])) {
			    $nClients = $allNetworkClients[$functionAction];
			}
		    }

		    $this->smarty->assign("nClients", $nClients);

		    $this->smarty->assign("cBrands", $cBrands);
		    $this->smarty->assign("nId", $functionAction);
		    $this->smarty->assign("cId", $selectedRowId);
                        
		    if (isset($args["jobPage"])) {
			$_SESSION["newBranchAddedMailFlag"] = 1;   
		    }    

		    $this->smarty->display("branches.tpl");
			
		}
			
	    }
	    
	}
	
    }
       
       
       
    /*
    * Description
    * 
    * This method is used for to manuplate data of Service Providers Page.
    * 
    * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
    * @return void It prints data to the loaded template.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
    */
       
    public function serviceProvidersAction($args) { 
        
        
     
       $functionAction   = isset($args[0])?$args[0]:'';
          $selectedRowId     = isset($args[1])?$args[1]:'';
          
          
          if($functionAction=="uploadImage")//Uploading image...
          {
               $this->log($_FILES,"img_________");
              
              //Uploading image...

       

        $error = "No error";
        $msg = "";
        $fileElementName = 'DocLogo';
//            echo"<pre>";
//            print_r($_FILES);
//            echo"</pre>";
     
        if (!empty($_FILES[$fileElementName]['error'])) {
  
            switch ($_FILES[$fileElementName]['error']) {

                case '1':
                    $error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
                    break;
                case '2':
                    $error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                    break;
                case '3':
                    $error = 'The uploaded file was only partially uploaded';
                    break;
                case '4':
                    $error = 'No file was uploaded.';
                    break;

                case '6':
                    $error = 'Missing a temporary folder';
                    break;
                case '7':
                    $error = 'Failed to write file to disk';
                    break;
                case '8':
                    $error = 'File upload stopped by extension';
                    break;
                case '999':
                default:
                    $error = 'No error code avaiable';
            }
        } else if (empty($_FILES[$fileElementName]['tmp_name']) || $_FILES[$fileElementName]['tmp_name'] == 'none') {

            $error = 'No file was uploaded..';
        } else {

            $LogosDirPath = APPLICATION_PATH . "/../images/serviceProvidersLogos/";

            $tmp_name = $_FILES[$fileElementName]["tmp_name"];
            $imgName = $_FILES[$fileElementName]["name"];
               
            move_uploaded_file($tmp_name, $LogosDirPath . $imgName);
 
            $imgNameArray = explode(".", $_FILES[$fileElementName]["name"]);
            $imgExt = strtolower($imgNameArray[count($imgNameArray) - 1]);

            $imgNewName =   $args[1]. "." . $imgExt;

            @rename($LogosDirPath . $imgName, $LogosDirPath . $imgNewName);

            $msg .= $imgName;
        
            // $msg .= " File Name: " . $_FILES[$fileElementName]['name'] . ", ";
            // $msg .= " File Size: " . @filesize($_FILES[$fileElementName]['tmp_name']);
            //for security reason, we force to remove all uploaded file
            //@unlink($_FILES['fileToUpload']);		
          
             $sp_model = $this->loadModel('ServiceProviders');
             $sp_model->updateSPDocLogo($args[1],$imgNewName);
            
        }
        
       
        exit();
              
          }
	$firstArg = isset($args[0]) ? $args[0] : '';
	$secondArg = isset($args[1]) ? $args[1] : '';
	$thirdArg = isset($args[2]) ? $args[2] : '';
	$fourthArg = isset($args[3]) ? $args[3] : '';
          
	if($firstArg == "getAssigned") {    //This is used for to get network id status for selected Service Provider.
	    
	    $ServiceProviderID = $secondArg;
	    $NetworkID = $thirdArg;
            
	    if($ServiceProviderID) {
		$Skyline = $this->loadModel('Skyline');
		$SPNWStatus = $Skyline->getServiceProviderNetworkStatus($ServiceProviderID, $NetworkID);
		echo json_encode($SPNWStatus);
	    }
              
	} else if($firstArg == "saveChanges") {	//This is used for to assigned checked Service Providers to selected Network .

	    $NetworkID = $secondArg;
            
	    if($NetworkID) {

		$assignedSPs = isset($_POST['assignedSPs']) ? $_POST['assignedSPs'] : ''; 
		$sltSPs = isset($_POST['sltSPs']) ? $_POST['sltSPs'] : '';

		$ServiceProviders = $this->loadModel('ServiceProviders');
		$result = $ServiceProviders->updateNetworkServiceProviders($NetworkID, $assignedSPs, $sltSPs);

		if($result) {
		    echo json_encode( "OK" );
		} else {
		    echo json_encode( "FAIL" );
		}

	    }
              
	} else if($firstArg == "sendSettings") {    //This is used for to send Settings of service provider.
                
	    $ServiceProviderID = $secondArg;
            
	    if($ServiceProviderID) {
                    
		//Api calls should be called here..
		$APISettingsModel = $this->loadModel('APISettings');
		$result = $APISettingsModel->putSettings($ServiceProviderID);

		//$result = array(
		//0=>array("ClientAccountNo"=>11111, "ResponseCode"=>"ssss1", "ResponseDescription"=> "sssssdf sdfsdfs"),
		//1=>array("ClientAccountNo"=>222, "ResponseCode"=>"33331", "ResponseDescription"=> "323232 sdfsdfs")
		//);

		$message = '';

		if(is_array($result)) {    
		    foreach($result as $rKey=>$rVal) {
			$message .=  $rVal['ClientAccountNo'] . " - " . $rVal['ResponseDescription'] . " (" . $rVal['ResponseCode'] . ") \n";
		    }
		}

		if($message!='') {
		    echo json_encode( array("status"=>"OK", "message"=>$message) );
		} else {
		    echo json_encode( array("status"=>"FAIL", "message"=>"") );
		}
                   
	    }
              
	} else {
          
	    $Skyline = $this->loadModel('Skyline');
	    $vat_rates_model = $this->loadModel('VATRates');
            $manufacturer_model = $this->loadModel('Manufacturers');
            
            $countries = $Skyline->getCountriesCounties();
            $vatRates = $vat_rates_model->getCountryVatRates();
            $networks = $Skyline->getNetworks();
            $platforms = $Skyline->getPlotforms();           

            $this->smarty->assign('statuses', $this->statuses);
            $this->smarty->assign('countries', $countries);
            $this->smarty->assign('vatRates', $vatRates);
            $this->smarty->assign('networks', $networks);
            $this->smarty->assign('platforms', $platforms);

            $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin);	//this value should be from user calss
            $this->smarty->assign('userPermissions', $this->user->Permissions);	//this value should be from user calss
            $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));

            $accessErrorFlag = false;

	    $this->page = $this->messages->getPage('serviceProviders', $this->lang);
            
                        //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('serviceProviders');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
        
            
	    $this->smarty->assign('page', $this->page);
              $currcencyModel=$this->loadModel('Currency');
                    $currcencyList=$currcencyModel->getCurrencyList();
		    $this->smarty->assign('currcencyList', $currcencyList);

	    switch($firstArg) {

		case 'insert': {

		    //We are fetching data for the selected row where user clicks on copy button
		    $datarow = [
			'ServiceProviderID' => '', 
			'NetworkID' => $secondArg, 
			'NetworkName' => $Skyline->getNetworkName($secondArg),
			'CompanyName' => '', 
			'BuildingNameNumber' => '', 
			'PostalCode' => '', 
			'Street' => '', 
			'LocalArea' => '',
			'TownCity' => '',
			'CountyID' => '',
			'CountryID' => '',
			'ContactPhone' => '',
			'ContactPhoneExt' => '',
			'ContactEmail' => '',
			'WeekdaysOpenTime' => '',
			'WeekdaysCloseTime' => '',
			'SaturdayOpenTime' => '',
			'SaturdayCloseTime' => '',
                        'SundayOpenTime' => '',
			'SundayCloseTime' => '',
                        'ServiceProvided' => '',
			'SynopsisOfBusiness' => '',
			'VATRateID' => '',
			'VATNumber' => '',
			'Platform' => '',
			'IPAddress' => '',
			'Port' => '',
			'InvoiceToCompanyName' => '',
			'SageSalesAccountNo' => '',
			'SagePurchaseAccountNo' => '',
			'TrackingUsername' => '',
			'TrackingPassword' => '',
			'WarrantyUsername' => '',
			'WarrantyPassword' => '',
			'WarrantyAccountNumber' => '',
			'Area' => '',
			'WalkinJobsUpload' => '',
			'CompanyCode' => '',
			'AccountNo' => '',
			'ReplyEmail' => '',  
			'DefaultTravelTime' => '120',
			'DefaultTravelSpeed' => '120',
			'CourierStatus' => 'Active',
                        'OnlineDiary' => 'In-Active',
			'Status' => 'Active',
			"ServiceManagerForename" => "",
			"ServiceManagerSurname" => "",
			"AdminSupervisorForename" => "",
			"AdminSupervisorSurname" => "",
			"ServiceManagerEmail" => "",
			"AdminSupervisorEmail" => "",
			"SendASCReport" => "No",
                        "EmailServiceInstruction" => "In-Active",
                        "Acronym" => "",
                        "DiaryType" => "FullViamente",
                        'PublicWebsiteAddress'=>'',
                        "GeneralManagerForename" => "",
			"GeneralManagerSurname" => "",
                        "GeneralManagerEmail" => "",
                        "ServiceProviderType" => "Standard",
                        "AutoReorderStock" => "No",
                        "SBRASync" => "",
			"DocumentCustomColorScheme" => "Skyline"
		    ];

		    //Checking user permissons to display the page.
		    if($this->user->SuperAdmin) {
			$accessErrorFlag = false;
		    } else {
			$accessErrorFlag = true;
		    }
                    
                    $manufacturers = $manufacturer_model->authManufacturersList();
                  
		    $this->smarty->assign('datarow', $datarow);
		    $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
		    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
                    $this->smarty->assign('manufacturers', $manufacturers);

		    $htmlCode = $this->smarty->fetch('serviceProvidersForm.tpl');

		    echo $htmlCode;

		    break;
		    
		}

		case 'update': {

		    $args['NetworkID'] = $secondArg;
		    $args['ServiceProviderID'] = $thirdArg;

		    $model = $this->loadModel('ServiceProviders');

		    $datarow = $model->fetchRow($args);

		    if($secondArg && !$datarow['NetworkID']) {
			$datarow['NetworkID'] = $secondArg;
			$datarow['NetworkName'] = $Skyline->getNetworkName($secondArg);
			$datarow['InvoiceToCompanyName'] = '';
			$datarow['SageSalesAccountNo'] = '';
			$datarow['SagePurchaseAccountNo'] = '';
			$datarow['TrackingUsername'] = '';
			$datarow['TrackingPassword'] = '';
			$datarow['WarrantyUsername'] = '';
			$datarow['WarrantyPassword'] = '';
			$datarow['WarrantyAccountNumber'] = '';
			$datarow['Area'] = '';
			$datarow['WalkinJobsUpload'] = '';
			$datarow['CompanyCode'] = '';
			$datarow['AccountNo'] = '';
		    }
                    
		    $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

		    $this->smarty->assign('datarow', $datarow);

		    //Checking user permissons to display the page.
		    if($this->user->SuperAdmin) {
			$accessErrorFlag = false;
		    } else {
			$accessErrorFlag = true;
		    }    
                            
		    $nClients = $model->getServiceProviderClients($args['ServiceProviderID']);
		    $clientContacts = $model->getServiceProviderClientContacts($args['ServiceProviderID']);
                    $manufacturers = $manufacturer_model->authManufacturersList($args['ServiceProviderID']);

		    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
		    $this->smarty->assign('nClients', $nClients);
		    $this->smarty->assign('clientContacts', $clientContacts);
                    $this->smarty->assign('manufacturers', $manufacturers);

		    $htmlCode = $this->smarty->fetch('serviceProvidersForm.tpl');

		    echo $htmlCode;

		    break;
		    
		}
		
		default: {

		    $this->smarty->assign('nId', $firstArg);
		    $this->smarty->assign('showAssigned', $secondArg);
		    $this->smarty->display('serviceProviders.tpl'); 
		}
		    
	    }
	    
	}         
           
    }
       
    
    
    /**
     * systemUsersAction
     * 
     * Action the request from the System users admin screen
     *  
     * @author Simon Tsang <s.tsang@pccsuk.com>
     * 
     * Fix Issue 13
     * Added code to activate filter drop down boxes
     * @author Andrew Williams <a.williams@pccsuk.com>
     *  
     **************************************************************************/
    public function systemUsersAction( $args ){
        $functionAction = isset($args[0])?$args[0]:'';                          /* Action (update / create / delete or Network to filter list by */
        $selectedRowId = isset($args[1])?$args[1]:'';                           /* Item to edit / delete or Client to filter by */
        $thirdArg         = isset($args[2])?$args[2]:'';                        /* Branch to filter by */

        
        
        $nClients  = array();                                                   /* Array for clients belonging to seleted network filter */
        $cBranches  = array();                                                  /* Array for branches belonging to selected client filter */

        $Skyline     = $this->loadModel('Skyline');
        
        $allNetworkClients = $Skyline->getNetworkClients();
        $networks = $Skyline->getNetworks();
        $allClientBranches = $Skyline->GetClientBranches();
        
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('countries', $Skyline->getCountriesCounties());
        $this->smarty->assign('SecurityQuestions', $Skyline->getSecurityQuestions() );
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin);//this value should be from user calss
        $this->smarty->assign('userPermissions', $this->user->Permissions);//this value should be from user calss
        $this->smarty->assign('allNetworkClients', $allNetworkClients);
        $this->smarty->assign('networks', $networks);
        $this->smarty->assign('clients', $Skyline->getClients());
        $this->smarty->assign('branches', $Skyline->getBranches());
        $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));
        $this->smarty->assign('nId', $functionAction);

        $this->smarty->assign('SecurityQuestions', $Skyline->getSecurityQuestions());
        $this->smarty->assign('Titles', $Skyline->getTitles());
        $this->smarty->assign('SearchScopes', array ('Branch Only','Brand','Client'));
        $accessErrorFlag = false;

        $this->page =  $this->messages->getPage('systemUsers', $this->lang);
        
           //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('systemUsers');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
        
        
        
        $this->smarty->assign('page', $this->page);

        switch( $functionAction ) {

            case 'insert':

                #$this->log('here');
                    //We are fetching data for the selected row where user clicks on copy button
                    $datarow = array(  
                        
                        'UserID' => '',
                        'Username'=>'',
                        'Password'=>'',
                        'CustomerTitleID'=>'',
                        'ContactFirstName'=>'',
                        'ContactLastName'=>'',
                        'ContactEmail'=>'',
                        'ContactMobile'=>'',
                        'UserType'=>'',
                        'ClientID'=>'',
                        'NetworkID'=>'',
                        'BranchID'=>'',
                        'CompanyID'=>'',
                        'Status'=>'',
                        'SecurityQuestionID'=>'',
                        'Answer' => '',
                        'DefaultBrandID'=>'',
                        'AccessLevels'=>'',
                        'SuperAdmin'=>'',
                        'BranchJobSearchScope'=>'',
                        'Status'=>'Active'

                        );


                    //Checking user permissons to display the page.
                    if($this->user->SuperAdmin) {
                        $accessErrorFlag = false;
                    } else if(isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {
                        $accessErrorFlag = true;
                    }

                    $this->smarty->assign('datarow', $datarow);
                    $this->smarty->assign('CompanyID', '');
                    $this->smarty->assign('UserType', '');
                    $this->smarty->assign('UserTypes', $Skyline->getUserType());
                    $this->smarty->assign('nClients', array());
                    $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);

                    $htmlCode = $this->smarty->fetch('systemUsersForm.tpl');

                    echo $htmlCode;

                break;


            case 'update':

                $args['UserID'] = $selectedRowId;
                $model          = $this->loadModel('SystemUsers');

                $datarow  = $model->fetchRow($args);

                #$this->log(''.var_export($datarow, true));
                $UserType='';
                $CompanyID='';
                if($datarow['BranchID'] != "")
                {
                    $UserType  = "Branch";
                    $CompanyID = $datarow['BranchID'];
                    if( $datarow['NetworkID'] ) {
                        if(isset($allNetworkClients[$datarow['NetworkID']])) {
                            $nClients =  $allNetworkClients[$datarow['NetworkID']];
                        }
                    }
                }
                else if($datarow['ClientID'] != "")
                {
                    $UserType  = "Client";
                    $CompanyID = $datarow['ClientID'];
                }
                else if($datarow['NetworkID'] != "")
                {
                    $UserType  = "Network";
                    $CompanyID = $datarow['NetworkID'];
                }
                else
                {
                    foreach($Skyline->getUserType() as $type){
                        if($datarow[ $type['Name'] . 'ID' ] != null){
                            $UserType  = $type['Name'];
                            $CompanyID = $datarow[ $type['Name'] . 'ID' ];
                        }
                    }
                }
                $this->smarty->assign('UserType', $UserType);
                $this->smarty->assign('CompanyID', $CompanyID);
                $this->smarty->assign('nClients', $nClients);
                $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

                $this->smarty->assign('datarow', $datarow);
                $this->smarty->assign('UserTypes', $Skyline->getUserType());


                //Checking user permissons to display the page..
                if($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else if(isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {
                    $accessErrorFlag = true;
                }    

                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);

                $htmlCode = $this->smarty->fetch('systemUsersForm.tpl');

                echo $htmlCode;

                break;

		
            default:

		if( $functionAction ) {
                    if(isset($allNetworkClients[$functionAction])) {
                            $nClients =  $allNetworkClients[$functionAction];
                    }
                }
                
                if ($selectedRowId) {
                    if(isset($allClientBranches[$selectedRowId])) {
                        $cBranches =  $allClientBranches[$selectedRowId];
                    }
                }
                
                $this->smarty->assign('nClients', $nClients);
                $this->smarty->assign('cBranches', $cBranches);
                $this->smarty->assign('nId', $functionAction);
                $this->smarty->assign('cId', $selectedRowId);
                $this->smarty->assign('bId', $thirdArg);
                $this->smarty->display('systemUsers.tpl'); 

            }

    }
    
    
    
    
     /**
     * Description
     * 
     * This method is used for to manuplate data of Extended Warrantor Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
       
      public function extendedWarrantorAction(  $args  ) {
          
            
          
           $functionAction       = isset($args[0])?$args[0]:'';
           $selectedRowId        = isset($args[1])?$args[1]:'';
          
           
          
          $this->smarty->assign('statuses', $this->statuses);
          $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin);//this value should be from user calss
          $this->smarty->assign('userPermissions', $this->user->Permissions);//this value should be from user calss
          $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));
                
          $accessErrorFlag = false;
          
          $this->page      = $this->messages->getPage('extendedWarrantor', $this->lang);
          
                     //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('extendedWarrantor');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
        
          
          
          $this->smarty->assign('page', $this->page);



            switch( $functionAction) {

                case 'insert':

                        //We are fetching data for the selected row where user clicks on copy button
                        $args['ExtendedWarrantorID']  = $selectedRowId;
                        if($args['ExtendedWarrantorID']!='')
                        {
                            $model    = $this->loadModel('ExtendedWarrantor'); 
                            $datarow  = $model->fetchRow($args);
                            $datarow['ExtendedWarrantorID'] = '';
                            $datarow['Status'] = 'Active';
                        }
                        else
                        {

                            $datarow = array('ExtendedWarrantorID'=>'', 'ExtendedWarrantorName'=>'', 'Status'=>'Active');
                        }

                        //Checking user permissons to display the page.
                        if($this->user->SuperAdmin)
                        {
                            $accessErrorFlag = false;
                        }
                        else 
                        {  
                            $accessErrorFlag = true;
                        }

                        $this->smarty->assign('datarow', $datarow);
                        $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                        $this->smarty->assign('accessErrorFlag', $accessErrorFlag);



                        $htmlCode = $this->smarty->fetch('extendedWarrantorForm.tpl');

                        echo $htmlCode;

                    break;


                case 'update':


                        $args['ExtendedWarrantorID'] = $selectedRowId;
                        $model     = $this->loadModel('ExtendedWarrantor');

                        $datarow   = $model->fetchRow($args);


                        $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

                       
                        $this->smarty->assign('datarow', $datarow);

                        //Checking user permissons to display the page.
                        if($this->user->SuperAdmin)
                        {
                            $accessErrorFlag = false;
                        }
                        else 
                        {    
                            $accessErrorFlag = true;
                        }    

                        $this->smarty->assign('accessErrorFlag', $accessErrorFlag);


                        $htmlCode = $this->smarty->fetch('extendedWarrantorForm.tpl');

                    echo $htmlCode;    

                    break;

                default:

                    $this->smarty->display('extendedWarrantor.tpl'); 

                }  
         
      }
    
    
       
       /*
    * Description
    * 
    * This method is used for to manuplate data of Suppliers Page.
    * 
    * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
    * @return void It prints data to the loaded template.
    * @author Andris Polnikovs <a.polnikovs@gmail.com>
    */
       
    public function suppliersAction($args) { 
//        echo"<pre>";
//        print_r($this->user->SuperAdmin);
//        echo"</pre>";
        
        //set table and user based on permissions
        if($this->user->SuperAdmin==0){
            if(array_key_exists("AP8001",$this->user->Permissions)){
                $this->session->activeUser="ServiveProvider";
                $this->session->mainTable="service_provider_supplier";
                $this->session->mainPage="suppliers";
                $this->session->secondaryTable="supplier";
                
             
            }
        }else{
                $this->session->activeUser="SuperAdmin";
                $this->session->mainTable="supplier";
                   $this->session->mainPage="suppliers";
                   $this->session->secondaryTable="service_provider_supplier";
        }
        
        
        $this->smarty->assign('activeUser', $this->session->activeUser);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin);
         $this->page =  $this->messages->getPage($this->session->mainPage, $this->lang);
         
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('suppliers');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
        
         
         
        $this->smarty->assign('page', $this->page);
        $model= $this->loadModel('Suppliers');
        $datatable= $this->loadModel('DataTable');
      
       
        $keys_data=$datatable->getAllFieldNames($this->session->mainTable,$this->user->UserID,$this->page['config']['pageID']);
        if(isset($keys_data[1])){
        $keys=$keys_data[1];
        }else{
        $keys=array();
        }
  
        $this->smarty->assign('data_keys', $keys);
      
         //to use this function we need provide pageid, it can be set in language config file
      
       
        
        if($this->session->activeUser=="SuperAdmin"){
            $pendingNo=$datatable->getPendingCount($this->session->mainTable);
            $this->smarty->assign('pendingNo', $pendingNo);
           $ServiceProviders = $this->loadModel('ServiceProviders');
        $splist=$ServiceProviders->getAllActiveServiceProviders();
         $this->smarty->assign('splist', $splist);
        }
       
        
        $this->smarty->display('systemAdmin/suppliers.tpl');  
    
    }
    
    public function tableDisplayPreferenceSetupAction($args){
         $this->page =  $this->messages->getPage($this->session->mainPage, $this->lang);
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin);//this value should be from user calss
   
        $datatable= $this->loadModel('DataTable');
        
        $columnStrings=$datatable->getColumnStrings($this->user->UserID,$this->page['config']['pageID'],'user');
        $columnStringsSA=$datatable->getColumnStrings(1,$this->page['config']['pageID'],'sa');
        
        
       $keys=$datatable->getAllFieldNames($this->session->mainTable);
   if(isset($keys[1])){
        $this->smarty->assign('data_keys', $keys[1]);
       }else{
           $this->smarty->assign('data_keys', $keys);  
       }
       
        if($columnStrings){
        $columnDisplayString=  explode(",", $columnStrings['ColumnDisplayString']);
        $columnOrderString=explode(',',$columnStrings['ColumnOrderString']);
        $columnNameString=explode(",", $columnStrings['ColumnNameString']);
      
        
        }else{
           $columnDisplayString=array(); 
           $columnOrderString="";
           $columnNameString=array();
           
        }
        
         if($columnStringsSA){
        $columnDisplayStringSA=  explode(",", $columnStringsSA['ColumnDisplayString']);
        $columnOrderStringSA=explode(',',$columnStringsSA['ColumnOrderString']);
        $columnNameStringSA=explode(",", $columnStringsSA['ColumnNameString']);
        $columnStatusStringSA=explode(",", $columnStringsSA['ColumnStatusString']);
         $this->smarty->assign('columnStatusStringSA', $columnStatusStringSA);
        
        }else{
           $columnDisplayStringSA=array(); 
           $columnOrderStringSA="";
           $columnNameStringSA=array();
           
        }
       
        $this->smarty->assign('columnDisplayString', $columnDisplayString);
        $this->smarty->assign('columnOrderString', $columnOrderString);
        $this->smarty->assign('columnNameString', $columnNameString);
        $this->smarty->assign('columnDisplayStringSA', $columnDisplayStringSA);
        $this->smarty->assign('columnOrderStringSA', $columnOrderStringSA);
        $this->smarty->assign('columnNameStringSA', $columnNameStringSA);
       $this->smarty->assign('controller', "OrganisationSetup");
     
        $this->smarty->assign('typeAction', $args['page']);
       
         $this->smarty->display('systemAdmin/tableDisplayPreferences.tpl');  
    }
    
    public function saveDisplayPreferencesAction($args){
       
         $this->page =  $this->messages->getPage($this->session->mainPage, $this->lang);
          $model= $this->loadModel('DataTable');
       
        $model->saveDisplayPreferences($this->user->UserID,$this->page['config']['pageID'],$_POST);
         $this->redirect('OrganisationSetupController','suppliersAction');
    }
    
    public function processSupplierAction($args){
        $Skyline = $this->loadModel('Skyline');
        $Currency = $this->loadModel('Currency');
        $ServiceProviders = $this->loadModel('ServiceProviders');
	$countries = $Skyline->getCountriesCounties();
	$currencylist = $Currency->getCurrencyList();
        $this->page =  $this->messages->getPage($this->session->mainPage, $this->lang);
         $this->smarty->assign('page', $this->page);
         $this->smarty->assign('countries', $countries);
         $this->smarty->assign('currencylist', $currencylist);
         $this->smarty->assign('statuses', $this->statuses);
         $this->smarty->assign('ServiceProviderID', $this->user->ServiceProviderID);
        
        
         //edit,copy
         if(isset($args['id'])){
               $model= $this->loadModel('Suppliers');
             if(isset($args['copy'])){
                 
                 $this->smarty->assign('mode', "copyNew");
                 $data=$model->getData($args['id'],$this->session->secondaryTable);
                 }
                 else
                 {$this->smarty->assign('mode', "update");
                 
                 $data=$model->getData($args['id'],$this->session->mainTable);
                 }
                
              
              $this->smarty->assign('data', $data);
               if($this->user->ServiceProviderID==""&&isset($data)){
                  
                     $spid=$model->getIDFromMain($data['SupplierID']);
                    
                 if($spid){
             $ancronym= $ServiceProviders->getServiceCentreAncronym($spid);
         $this->smarty->assign('ancronym', $ancronym);
                 }else{
                     $this->smarty->assign('ancronym', "");
                 }
               }
         $this->smarty->assign('activeUser',$this->session->activeUser);
            
              $this->smarty->display('systemAdmin/supplierCard.tpl');
         }else{
             if($this->session->activeUser!="SuperAdmin"){
                 if(isset($args['new'])){
                      $this->smarty->assign('mode', "New");
                    
                       $this->smarty->display('systemAdmin/supplierCard.tpl');
                 }else{
                       $this->smarty->assign('controller', "OrganisationSetup");
                  $this->smarty->display('systemAdmin/insertNewSelector.tpl');
                 }
             }else{
             $this->smarty->assign('mode', "insert");
              $this->smarty->display('systemAdmin/supplierCard.tpl');
             }
         }
         
         
       
    }
    
    
    public function saveSupplierAction($args){
         $model= $this->loadModel('Suppliers');
      $model->saveSupplier($_POST,$this->session->mainTable,$this->session->secondaryTable);
       $this->redirect('OrganisationSetupController','suppliersAction');
        
    }
    public function deleteSupplierAction($args){
      $model= $this->loadModel('Suppliers');
      $model->deleteSupplier($args['id'],$this->session->mainTable);
      $this->redirect('OrganisationSetupController','suppliersAction');
        
    }
    
    public function resetDisplayPreferencesAction($args)
    {
         $typeAction=$args['typeAction'];
         
         $this->page =  $this->messages->getPage( $typeAction, $this->lang);
          $model= $this->loadModel('DataTable');
          
          $model->resetDisplayPreferences($this->user->UserID,$this->page['config']['pageID']);
           $this->redirect('OrganisationSetupController',$typeAction."Action");
    }
    
    
    public function loadSupplierTableAction($args)
            {
     
       $dd=$_GET;
      $this->page =  $this->messages->getPage('suppliers', $this->lang);
      
      
        $datatable= $this->loadModel('DataTable');
        if(isset($args['inactive'])){
           $inactive=true;
       }else{
           $inactive=false;
       }
       $table=$this->session->mainTable;
       if($this->user->ServiceProviderID!=""){
           $sp=$this->user->ServiceProviderID;
           $aproved="";
       }else{
           $sp=false;
           $aproved="Approved";
       }
       if(isset($args['unaproved'])){
           $aproved="NotApproved";
       }
       if(isset($args['pending'])){
           $aproved="Pending";
       }
       if(isset($args['supplierid'])){
           $sp=$args['supplierid'];
           $table=$this->session->secondaryTable;
       }
      
        $keys_data=$datatable->getAllFieldNames($table,$this->user->UserID,$this->page['config']['pageID']);
        $columns=$keys_data[0];
       
      
     // $columns=$datatable->getAllFieldNames('currency');
       //$columns="";
    
       
       $joins=" left join country on  country.CountryID=".$table.".CountryID
                left join currency cu on cu.CurrencyID=".$table.".DefaultCurrencyID";
       //$joins="";
       
       
        $data=$datatable->datatableSS($table,$columns,$dd,$joins,$extraColumns=array('<input type="checkbox">'),0,false,$inactive,$sp,$aproved);
      $this->log($data,"datatableData");
        echo json_encode($data);
    }
    
    //this is used only for new supplier insert
     public function loadSupplierListAction($args)
            {
     
       $dd=$_GET;
      $this->page =  $this->messages->getPage($this->session->mainPage, $this->lang);
        $datatable= $this->loadModel('DataTable');
       
       
      
    
       $columns=array("SupplierID","CompanyName");
    
       
       
       //$joins=""; 
      
       
        $data=$datatable->datatableSS($this->session->secondaryTable,$columns,$dd,"",array(),0,false,false,false,"Approved");
      $this->log($data,"datatableData");
        echo json_encode($data);
    }
    
    
    //generates pdf for Requisition
      //args  service provider id
    public function showSampleDocAction($args){
         $spModel = $this->loadModel("ServiceProviders");
	    $data = $spModel->getDocColours($args[0]);
        $this->smarty->assign("data",$data);
          $html = $this->smarty->fetch('stockControl/pdf/samplePrewview.tpl');

        include(APPLICATION_PATH . '/../libraries/MPDF56/mpdf.php');
        $mpdf = new mPDF();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
        exit;
      }
    
    
       
      
    
}

?>
