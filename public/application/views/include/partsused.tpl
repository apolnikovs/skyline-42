<style>
 
</style>
<script>
    
    function setRowColours(nRow, aData){
   
    //var myarr = aData[11].split(" ");
    // $(nRow,'td').css({ "color":"#"+myarr[0]});
    
    $('td:eq(9)',nRow).html("<div style='width:100%;height:100%;background:#"+aData[10]+"'>&nbsp;</div>");
    }
    $(document).ready(function(){
//Parts Table ***********************************************************************
        
	var partsTableObj = {
            aoColumns: [ 
                /* PartID */	    { bVisible: false },
                /* Description */   { sWidth: "10%" },
                /* PartNo */	    { sWidth: "10%" },
                /* Quantity */	    { sWidth: "5%" },
                /* Supplier */	    { sWidth: "10%" },
                /* OrderDate */	    { sWidth: "9%" },
                /* OrderNo */	    { sWidth: "8%" },
                /* ExpectedDate */  { sWidth: "8%" },
                /* ReceivedDate */  { sWidth: "8%" },
                /* ChargeType */  { sWidth: "4%" },
            /*Colours*/          { sWidth: "4%"  },
                /* OrderStatus */   { sWidth: "20%" }
                
            ],
            bAutoWidth:		false,
            bDestroy:           true,
            bStateSave:         false,
            bServerSide:        false,
            bProcessing:        false,
            htmlTableId:        "PartsUsed",
            sDom:               "ft<'#dataTables_child'>Trpli",
            sPaginationType:    "full_numbers",
            bPaginate:          true,
            bSearch:            false,
            aaSorting:		[[ 0, 'desc' ], [ 1, 'desc' ]],
            iDisplayLength:     10,
            sAjaxSource:        '{$_subdomain}/Data/partsused/{$JobID}/chargeType='+$('input[name=ptRadio]:checked').val()+"/",
	    oLanguage: {
		sSearch: "<span id='searchLabel' style='float:left; top:10px; position:relative;'>Search within results:</span>&nbsp;"
	    },
	    fnInitComplete: function() {
		var html = '<a href="#" class="delSearch" style="top:14px; right:4px; position:absolute;">\
				<img style="position:relative; zoom:1; bottom:4px;" src="{$_subdomain}/css/Skins/skyline/images/close.png">\
			    </a>';
		$(html).insertAfter("#EQ3 .dataTables_filter input");
		$(document).on("click", "#EQ3 .delSearch", function() {
		    $(".dataTables_filter input").val("");
		    partsTable.fnFilter("");
		    return false;
		});
	    },
            fnRowCallback:  function(nRow, aData){
               setRowColours(nRow, aData);
                
                }
	};
       
	{if isset($loggedin_user->Permissions["AP7046"]) || $loggedin_user->Username == "sa"}

           

 $('#PartsUsed tbody tr').live('click', function (event) { 
 table = TableTools.fnGetInstance("PartsUsed");
 var selected = table.fnGetSelected();
 data=partsTable.fnGetData(selected[0]);
// color=data[11].split(' ');
// if(color[1]=="White"){
// textcolor="#fff";
 
// }else{
// textcolor="#000";
// }
// $(".customCl").css("background-color","");
// $(".customCl").css("color","");
// $(".customCl").removeClass("customCl");
// $("table tr.part_selected td").addClass("customCl");
// $(".customCl").css({ "background-color": "#"+color[0]+"!important"});
// $(".customCl").css({ "color": textcolor+"!important"});
 
  
 //show fit parts button
 
 
    if(data[9]=="04 DELIVERED"){ $('#ToolTables_PartsUsed_4').removeClass('hidden')}else{ $('#ToolTables_PartsUsed_4').addClass('hidden')}
    });

	    partsTableObj.oTableTools = {
		sRowSelect: "single",
                sSelectedClass: "part_selected",
		aButtons: [
		    {
			    sExtends:	    "text",
			    sButtonText:    "Insert",
			    sButtonClass:   "marginRight10 btnStandard",
			fnClick: function() { 
			    createPart(); 
			    return false; 
			}
		    },
		    {
			    sExtends:	    "text",
			    sButtonText:    "Fault Code Only",
			    sButtonClass:   "marginRight10 btnStandard largeButton",
			fnClick: function() { 
				createPart("codeOnly"); 
				return false; 
			    }
			},
			{
			    sExtends:	    "text",
			    sButtonText:    "Edit",
			    sButtonClass:   "marginRight10 btnStandard",
			    fnClick: function() { 
			    //updatePart(null);  //need to be disabled till spect
			    return false; 
			}
		    },
		    {
			    sExtends:	    "text",
			sButtonText:    "Delete",
			    sButtonClass:   "marginRight10 btnStandard",
			fnClick: function() { 
			    deletePart(); 
			    return false; 
			}
		    },
                        {
			    sExtends:	    "text",
			    sButtonText:    "Fit Part",
			    sButtonClass:   "marginRight10 btnStandard hidden",
			    fnClick: function() { 
				fitPartConfirm(); 
				return false; 
		    }
			}
		]
	    };
	    
	{else}

	    partsTableObj.sDom = "ft<'#dataTables_child'>rpli";
	    
	{/if}

	partsTable = $("#PartsUsed").dataTable(partsTableObj);
        		
	$(document).on("dblclick", "#PartsUsed tr", function() {
	    updatePart(this);
	});
	function updateJobModel(t){
             $.post("{$_subdomain}/Job/changeJobModel", { ServiceProviderModelID: $('#modelFilter').val(),JobID:{$JobID} }, function(data) {
               
		 $( t ).dialog( "close" );
                $('#JobModelNumberSpan').html($('#modelFilter option:selected').text());
                $('#ModelNumber').html($('#modelFilter option:selected').text());
                $('input[name^=ModelNo]').val($('#modelFilter option:selected').text());
                
                     $('#modelChnageDivConf').dialog({
                               
                                                    title:"Success ",
                                             resizable:false,
                                          modal: true,
                            buttons: {
                                 
                                Ok:{ text:"Ok", "class":"gplus-blue", click:function() {
                                  $( this ).dialog( "close" );
                                    }}
                                      }
    
                               });
                     

                            
	    });
        }

	function createPart(mode) {
		
		$.colorbox({
			href :	"{$_subdomain}/Job/getUpdatePartModal/partID=/jobID={$JobID}/mode="+mode, 
			
		    scrolling:  false,
                    escKey: false,
                        overlayClose: false,
		    onComplete: function() {
			$("#OrderDate, #DueDate, #ReceivedDate").datepicker({ dateFormat: 'dd/mm/yy' });
                            $("#SectionCodeID").combobox();
                            $("#RepairCodeID").combobox();
                            $("#DefectCodeID").combobox();
                            $("#modelFilter").combobox(
                        { selected: function() {  
                               $('#modelChnageDiv').dialog({
                               
                                                    title:"Warning",
                                             resizable:false,
                                          modal: true,
                            buttons: {
                                 UpdateJob:{ text:"Update Job", "class":"gplus-blue", click:function() {
                                  updateJobModel(this);
                                    }},
                                Cancel:{ text:"Cancel", "class":"gplus-blue", click:function() {
                                  $( this ).dialog( "close" );
                                    }}
                                      }
    
                               });
                          updateModelFilter();

                                                    } }
                                );
                             $('#cboxClose').hide();
                           
		    },
                        onClosed:function(){
                      $("#updatePartModalContainer").remove().empty();
		    $("#stockSearchContainer").remove().empty();
                        $.colorbox.remove();
                        }
		});
		return false;
		
	}
	
function fitPartConfirm(){

var html="<fieldset>\
             <legend>Update Part Status</legend>\
             <p style='color:6b6b6b'>Please confirm you have fitted the selected part to this job?</p>\
             <br>\
            <button id='fitparconfirm'   type=button class='btnStandard'  onclick='' style='width:100px;float:left'><span class='label'>Yes</span></button>\
           <button   type=button class='btnStandard' onclick='$.colorbox.close()' style='width:100px;float:right'><span class='label'>No</span></button>\
          </fieldset>\
            ";
      
            $.colorbox({
			html :	html, 
			
			scrolling:  false,
                        escKey: false,
                        overlayClose: false,
			onComplete: function() {
			  
			},
                        onClosed:function(){
                    
                        }
		    });
		    return false;
            }
 $(document).on("click", "#fitparconfirm", function() {
                        fitPart();
                        });
	 function updatePart(the) {

		if(the) {
		    var partID = partsTable.fnGetData(the)[0];
		} else {
		    var table = TableTools.fnGetInstance("PartsUsed");
		    var selected = table.fnGetSelected();
		    if(selected.length == 0) {
			alert("You must select a part first.");
			return false;
		    }
		    var partData = partsTable.fnGetData(selected[0]);
		    var partID = partData[0];
		}
                    
                  
		    $.colorbox({
			href :	"{$_subdomain}/Job/getUpdatePartModal/partID="+partID+"/jobID={$JobID}/", 
			width:	"1000px",
			scrolling:  false,
                        escKey: false,
                        overlayClose: false,
			onComplete: function() {
			    $("#OrderDate, #DueDate, #ReceivedDate").datepicker({ dateFormat: 'dd/mm/yy' });
                            $("#SectionCodeID").combobox();
                            $("#RepairCodeID").combobox();
                            $("#DefectCodeID").combobox();
                             $('#cboxClose').hide();
             
}
});
		
			}
                       
	 function fitPart(the) {
		
		if(the) {
		    var partID = partsTable.fnGetData(the)[0];
		} else {
		    var table = TableTools.fnGetInstance("PartsUsed");
		    var selected = table.fnGetSelected();
		    if(selected.length == 0) {
			alert("You must select a part first.");
			return false;
	    }
		    var partData = partsTable.fnGetData(selected[0]);
		    var partID = partData[0];
		}


		     $.post("{$_subdomain}/Job/fitPart", { partID: partID,JobID:{$JobID} }, function(data) {
                
		partsTable.fnReloadAjax(null,null,null,true);
		$.colorbox.close();
		return false;
	    });	    
	    }
            
         


	function deletePart() {
	    var table = TableTools.fnGetInstance("PartsUsed");
	    var selected = table.fnGetSelected();
	    if(selected.length == 0) {
		alert("You must select a part first.");
		return false;
	    }
	    var partData = partsTable.fnGetData(selected[0]);
	    var partID = partData[0];
	    
	    var html = "<fieldset style='text-align:center;'>\
			    <legend align='center'>{$page['Labels']['part_delete_legend']|escape:'html'}</legend>\
			    <p style='margin-top:15px;'>{$page['Text']['part_delete_confirmation']|escape:'html'}</p>\
			    <a href='#' id='partDeleteYes' class='DTTT_button' style='margin-right:10px; width: 30px;'>Yes</a>\
			    <a href='#' id='cancel' class='DTTT_button' style='width: 30px;'>No</a>\
			</fieldset>\
		       ";
		    
	    $.colorbox({
		html :	    html, 
		width:	    "400px",
		scrolling:  false
	    });
	}


	
	$(document).on("click", "#partDeleteYes", function() {
	    var table = TableTools.fnGetInstance("PartsUsed");
	    var selected = table.fnGetSelected();
	    if(selected.length == 0) {
		alert("You must select a part first.");
		return false;
	    }
	    var partData = partsTable.fnGetData(selected[0]);
	    var partID = partData[0];
	    $.post("{$_subdomain}/Job/deletePart", { partID: partID,JobID:{$JobID} }, function() {
		partsTable.fnReloadAjax(null,null,null,true);
		$.colorbox.close();
		return false;
	    });	    
	    return false;
	});
	

	$(document).on("click", "#cancel", function() {
	    $.colorbox.close();
	    return false;
	});
        
	$(document).on("click", "#colourKey", function() {
	      $.colorbox({
			href :	"{$_subdomain}/stockControl/getColourKey/", 
			width:	"450px",
			scrolling:  false,
                        escKey: false,
                        overlayClose: false,
			onComplete: function() {
			 
                        }
});
	});

});

</script>

{if $AP7026}
	<div id="EQ3" class="span-24">
	    <div class="head"><div class="icon_c">&nbsp;</div>Parts Used (<span id="PartsUsedCount">{$NumPartsUsed}</span>)</div>
	    <div class="show">
                {if $jobChargeType!="m"}{$display="none"}{else}{$display="block"}{/if}
                <div style="float:left;margin-top:10px;display:{$display}">Part Type: 
                  <input {if $jobChargeType=="w"}checked=checked{/if} type="radio" name="ptRadio" value="Warranty">Warranty
                  <input {if $jobChargeType=="c"}checked=checked{/if} type="radio" name="ptRadio" value="Chargable">Chargeable
                  <input {if $jobChargeType=="m"}checked=checked{/if} type="radio" name="ptRadio" value="Both">Both
                </div>
		<table id="PartsUsed" class="browse">
		    <thead>
			<tr>
			    <th></th>
			    <th>Description</th>
			    <th>Stock Code</th>
			    <th>Qty.</th>
			    <th>Supplier</th>
			    <th>Order No.</th>
			    <th>Ordered</th>
			    <th>Expected</th>
			    <th>Received</th>
                            <th title="W = Warranty  	C = Chargeable	E = Estimate" >Type</th>
                            <th>Colour</th>
			    <th>Status</th>
			    
			</tr>
		    </thead>
		    <tbody>
			<tr> <td colspan="9"> No records found </td> </tr>
		    </tbody>
		</table> 
                <div class="DTTT_container">
                <!--<a id="colourKey" class="DTTT_button marginRight10 btnStandard largeButton"><span>Colour Key</span></a>-->
                </div>
	    </div>
	</div>
    {/if}