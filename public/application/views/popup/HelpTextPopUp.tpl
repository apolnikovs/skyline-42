<script type="text/javascript" >
 $(document).ready(function() {  
 
 $('#popUpContainerMainDiv').hide();
 $('#helpContainer').show();
 $.colorbox.resize();
 
                    $(document).on('click', '.helpClose', 
                    
                        function() {
                            
                            
                            $('#popUpContainerMainDiv').show();
                            $('#helpContainer').hide();
                              $('#popUpContainerMainDiv').show();
                            $.colorbox.resize();
                            
                        
                        });
                       
                       
                    {if $SuperAdmin}   
                        
                        $(document).on('click', '#help_edit_btn', 

                            function() {

                                $("#HelpTopDiv").hide("fast");
                                $("#HelpBottomDiv").show("fast");



                                $.colorbox.resize({ height:450 });
                                return false;
                            });    


                        $(document).on('click', '#help_save_btn', 



                            function() {


                                if($("label.fieldError:visible").length>0)
                                {

                                }
                                else
                                {
                                    $("#processDisplayHelpText").show();
                                    $("#help_save_btn").hide();
                                    $("#help_cancel_btn").hide();
                                }


                                $('#HelpTextForm').validate({

                                            ignore: '',

                                            rules:  {
                                                        HelpTextTitle:
                                                        {
                                                                required: true

                                                        },
                                                        HelpText:
                                                        {
                                                               required: true
                                                        }

                                            },
                                            messages: {
                                                        HelpTextTitle:
                                                        {
                                                                required: "{$page['Errors']['title']|escape:'html'}"
                                                        },
                                                        HelpText:
                                                        {
                                                                required: "{$page['Errors']['description']|escape:'html'}"
                                                        }
                                            },

                                            errorPlacement: function(error, element) {

                                                    error.insertAfter( element );

                                                    $("#processDisplayHelpText").hide();

                                                    $("#help_save_btn").show();
                                                    $("#help_cancel_btn").show();

                                            },
                                            errorClass: 'fieldError',
                                            onkeyup: false,
                                            onblur: false,
                                            errorElement: 'label',
                                            submitHandler: function() {

                                                    $("#processDisplayHelpText").show();
                                                    $("#help_save_btn").hide();
                                                    $("#help_cancel_btn").hide();


                                                    $.post("{$_subdomain}/Popup/helpText/",        

                                                    $("#HelpTextForm").serialize(),      

                                                    function(data) {

                                                        var p = eval("(" + data + ")");



                                                        if(p['status']=="OK") {


                                                             $.colorbox.close();
                                                        } 
                                                        else 
                                                        {


                                                           $("#processDisplayHelpText").hide();

                                                           $("#help_save_btn").show();
                                                           $("#help_cancel_btn").show();

                                                           $("#suggestText").html(p['message']);

                                                        }

                                                    });    

                                                }

                                            });




                            });
                
                   {/if}
            });
     

</script>

<div id="HelpPageDiv" >
    
    <div id="HelpTopDiv" >
    <form action="#" id="HelpTextTopForm" name="HelpTextTopForm"  >
        <fieldset>
            <legend title="">{$HelpTextTitle|escape:'html'}</legend>
            <p>

                {$HelpText|nl2br}

            </p>
            <p>
                {if $SuperAdmin}
                <input type="submit" name="help_edit_btn" style="float:left;" class="textSubmitButton" id="help_edit_btn"   value="{$page['Buttons']['edit']|escape:'html'}" >
                {/if}
                <input type="button" name="helpClose" style="float:right;" class="textSubmitButton helpClose" id="help_close_btn"   value="{$page['Buttons']['close']|escape:'html'}" >

            </p>  
        </fieldset>
    </form>
    </div>

    <div id="HelpBottomDiv"  class="SystemAdminFormPanel" style="display:none;" >
    {if $SuperAdmin}
        
    <form action="{$_subdomain}/Popup/helpText/" id="HelpTextForm" name="HelpTextForm" method="post" >
        <fieldset>
            <legend title="">{$form_legend|escape:'html'}</legend>

                 <p><label id="suggestText" ></label></p>
                <p>
                   <label class="fieldLabel" for="HelpTextTitle" >{$page['Labels']['title']|escape:'html'}:<sup>*</sup></label>

                    &nbsp;&nbsp; <input  type="text" class="text"  name="HelpTextTitle" value="{$datarow.HelpTextTitle|escape:'html'}" id="HelpTextTitle" >

                </p>


                <p>
                   <label class="fieldLabel" for="HelpText" >{$page['Labels']['description']|escape:'html'}:<sup>*</sup></label>

                   &nbsp;&nbsp; <textarea name="HelpText"  cols="20" rows="6" class="text" id="HelpText"  >{$datarow.HelpText|escape:'html'}</textarea>

                </p>



                <p>

                   <span class= "bottomButtons" >
                        <input type="hidden" name="Status"  value="Active" >
                        <input type="hidden" name="HelpTextID"  value="{$datarow.HelpTextID|escape:'html'}" >
                        <input type="hidden" name="HelpTextCode"  value="{$datarow.HelpTextCode|escape:'html'}" >

                        {if $datarow.HelpTextID neq '' && $datarow.HelpTextID neq '0'}

                            <input type="submit" name="help_save_btn" class="textSubmitButton" id="help_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >

                        {else}

                            <input type="submit" name="help_save_btn" class="textSubmitButton" id="help_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >

                        {/if}

                            &nbsp;&nbsp;
                            <input type="button" name="help_cancel_btn" class="textSubmitButton helpClose" id="help_cancel_btn"  value="{$page['Buttons']['cancel']|escape:'html'}" >

                            <span id="processDisplayHelpText" style="color:red;display:none;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >&nbsp;&nbsp;{$page['Text']['process_record']|escape:'html'}</span> 
                     </span>
                                           
                </p>


        </fieldset>
    </form>
                     
     {/if}
     
    </div>  
    
    
</div>
                      
