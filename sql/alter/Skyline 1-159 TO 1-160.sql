# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.159');


# ---------------------------------------------------------------------- #
# Modify table "service_provider_engineer"                                                     #
# ---------------------------------------------------------------------- #
ALTER TABLE `service_provider_engineer` ADD COLUMN `EngineerImportance` INT(11) NOT NULL DEFAULT '50';



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.160');




