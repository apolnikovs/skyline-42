# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.165');

# ---------------------------------------------------------------------- #
# Modify table "service_provider_engineer"                               #
# ---------------------------------------------------------------------- #
ALTER TABLE `service_provider_engineer` ADD `SetupType` ENUM( 'Unique', 'Replicate' ) NULL DEFAULT 'Unique'; 
ALTER TABLE `service_provider_engineer` ADD `ReplicateType` ENUM( 'FourWeeksOnly', 'WeeklyCycle', 'MonthlyCycle' ) NULL DEFAULT 'FourWeeksOnly';
ALTER TABLE `service_provider_engineer` ADD `ReplicateStatusFlag` ENUM( 'No', 'Yes' ) NULL DEFAULT 'No';


# ---------------------------------------------------------------------- #
# Modify table "service_provider"                                        #
# ---------------------------------------------------------------------- #
ALTER TABLE `service_provider` ADD COLUMN `DiaryAllocationType` ENUM('Postcode','GridMapping') NOT NULL DEFAULT 'Postcode';



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.166');
