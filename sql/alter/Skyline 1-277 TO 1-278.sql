# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.277');

# ---------------------------------------------------------------------- #
# Andris Stock Changes                                                   #
# ---------------------------------------------------------------------- # 

CREATE TABLE repair_type ( RepairTypeID INT(10) NOT NULL AUTO_INCREMENT, RepairType VARCHAR(20) NULL DEFAULT NULL, ManufacturerID INT(11) NOT NULL, JobWeighting INT(11) NULL DEFAULT NULL, RepairIndex INT(11) NULL DEFAULT NULL, Chargeable ENUM('Yes','No') NOT NULL DEFAULT 'No', Warranty ENUM('Yes','No') NOT NULL DEFAULT 'No', WarrantyCode VARCHAR(5) NULL DEFAULT NULL, CompulsoryFaultCoding ENUM('Yes','No') NOT NULL DEFAULT 'No', ExcludeFromEDI ENUM('Yes','No') NOT NULL DEFAULT 'No', ForceAdjustmentIfNoPartsUsed ENUM('Yes','No') NOT NULL DEFAULT 'No', ExcludeFromInvoicing ENUM('Yes','No') NOT NULL DEFAULT 'No', PromptForExchange ENUM('Yes','No') NOT NULL DEFAULT 'No', NoParts ENUM('Yes','No') NOT NULL DEFAULT 'No', ScrapExchange ENUM('Yes','No') NOT NULL DEFAULT 'No', ExcludeRRCHandlingFee ENUM('Yes','No') NOT NULL DEFAULT 'No', Unavailable ENUM('Yes','No') NOT NULL DEFAULT 'No', Type ENUM('Repair','Modification','Exchange','LiquidDamage','NFF','BER','RTM','RNR','Excluded','Exchange','Estimate','Exchange Fee','Handling Fee') NULL DEFAULT NULL, Status ENUM('Active','In-active') NOT NULL DEFAULT 'Active', ModifiedUserID INT(11) NULL DEFAULT NULL, ModifiedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, CreatedDate TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00', CreatedUserID INT(11) NULL DEFAULT NULL, PRIMARY KEY (RepairTypeID) ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

CREATE TABLE service_provider_repair_type ( ServiceProviderRepairTypeID INT(10) NOT NULL AUTO_INCREMENT, RepairTypeID INT(10) NULL DEFAULT NULL, RepairType VARCHAR(20) NULL DEFAULT NULL, ServiceProviderManufacturerID INT(11) NULL DEFAULT NULL, JobWeighting INT(11) NULL DEFAULT NULL, RepairIndex INT(11) NULL DEFAULT NULL, Chargeable ENUM('Yes','No') NOT NULL DEFAULT 'No', Warranty ENUM('Yes','No') NOT NULL DEFAULT 'No', WarrantyCode VARCHAR(5) NULL DEFAULT NULL, CompulsoryFaultCoding ENUM('Yes','No') NOT NULL DEFAULT 'No', ExcludeFromEDI ENUM('Yes','No') NOT NULL DEFAULT 'No', ForceAdjustmentIfNoPartsUsed ENUM('Yes','No') NOT NULL DEFAULT 'No', ExcludeFromInvoicing ENUM('Yes','No') NOT NULL DEFAULT 'No', PromptForExchange ENUM('Yes','No') NOT NULL DEFAULT 'No', NoParts ENUM('Yes','No') NOT NULL DEFAULT 'No', ScrapExchange ENUM('Yes','No') NOT NULL DEFAULT 'No', ExcludeRRCHandlingFee ENUM('Yes','No') NOT NULL DEFAULT 'No', Unavailable ENUM('Yes','No') NOT NULL DEFAULT 'No', Type ENUM('Repair','Modification','Exchange','LiquidDamage','NFF','BER','RTM','RNR','Excluded','Exchange','Estimate','Exchange Fee','Handling Fee') NULL DEFAULT NULL, ServiceProviderID INT(11) NULL DEFAULT NULL, Status ENUM('Active','In-active') NOT NULL DEFAULT 'Active', PRIMARY KEY (ServiceProviderRepairTypeID) ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

CREATE TABLE service_provider_supplier_to_sp_part_stock_template ( ServiceProviderSupplierToSPPartStockTemplateID INT(10) NOT NULL AUTO_INCREMENT, ServiceProviderSupplierID INT(10) NOT NULL DEFAULT '0', SpPartStockTemplateID INT(10) NOT NULL DEFAULT '0', PRIMARY KEY (ServiceProviderSupplierToSPPartStockTemplateID) ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

ALTER TABLE service_provider ADD COLUMN CurrencyID INT(11) NULL DEFAULT NULL AFTER KeepEngInPCArea;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.278');
