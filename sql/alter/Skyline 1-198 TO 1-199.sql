# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.198');

# ---------------------------------------------------------------------- #
# Modify table "service_provider_engineer"                               #
# ---------------------------------------------------------------------- #
ALTER TABLE service_provider_engineer ADD COLUMN ExcludeFromAppTable ENUM('Yes','No') NOT NULL DEFAULT 'No' AFTER ReplicatePostcodeAllocation;

# ---------------------------------------------------------------------- #
# Modify table "service_provider"                                         #
# ---------------------------------------------------------------------- #
ALTER TABLE service_provider CHANGE COLUMN AutoChangeTimeSlotLabel AutoChangeTimeSlotLabel ENUM('Yes','No') NOT NULL DEFAULT 'Yes' AFTER DefaultTravelSpeed,
							ADD COLUMN DiaryShowSlotNumbers ENUM('Yes','No') NOT NULL DEFAULT 'No' AFTER DiaryType, 
							ADD COLUMN ViamenteStartTime ENUM('Fixed','Flexible') NOT NULL DEFAULT 'Flexible' AFTER DiaryShowSlotNumbers, 
							ADD COLUMN EmailAppBooking ENUM('Yes','No') NOT NULL DEFAULT 'No' AFTER ViamenteStartTime;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.199');
