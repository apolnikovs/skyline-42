# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.294');

# ---------------------------------------------------------------------- #
# Modify job_fault_code table											 #
# ---------------------------------------------------------------------- # 
ALTER TABLE job_fault_code CHANGE UseAsGenericDescription UseAsGenericFaultDescription ENUM('Yes', 'No') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'No'; 
ALTER TABLE job_fault_code CHANGE LengthFrom MinLength INT( 11 ) NULL DEFAULT NULL,
						   CHANGE LengthTo MaxLength INT( 11 ) NULL DEFAULT NULL; 
ALTER TABLE job_fault_code CHANGE Format RequiredFormat VARCHAR( 15 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL; 
ALTER TABLE job_fault_code CHANGE HideWhenRelatedFaultCodeBlank HideWhenRelatedCodeIsBlank ENUM( 'Yes', 'No' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'No'; 
ALTER TABLE job_fault_code CHANGE ReqAtBookingForChargeable ReqAtBookingForChargeableJobs ENUM( 'Yes', 'No' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'No', 
						   CHANGE ReqAtCompletionForChargeable ReqAtCompletionForChargeableJobs ENUM( 'Yes', 'No' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'No', 
						   CHANGE ReqAtBookingForWarranty ReqAtBookingForWarrantyJobs ENUM( 'Yes', 'No' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'No', 
						   CHANGE ReqAtCompletionForWarranty ReqAtCompletionForWarrantyJobs ENUM( 'Yes', 'No' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'No'; 
ALTER TABLE job_fault_code ADD isDeleted ENUM( 'Yes', 'No' ) NOT NULL DEFAULT 'No' AFTER CreatedUserID;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.295');
