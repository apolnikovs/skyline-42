# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.300');

# ---------------------------------------------------------------------- #
# Raju Changes 															 #
# ---------------------------------------------------------------------- # 
update email set Message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>[PAGE_TITLE]</title>
</head>

<body>
<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:\'Calibri\';">
	<tbody><tr>
		<td bgcolor="#ffffff" align="center">
        	<table width="640" cellspacing="0" cellpadding="0" border="0" style="margin:0 10px;font-family:\'Calibri\'">
            	<tbody>
                
                <!-- #HEADER
        		================================================== -->
            	<tr>
                	<td width="640"><!-- End top bar table -->
                    </td>
                </tr>
                <tr>
                <td width="640" bgcolor="#52aae0" align="center">
    
    <table width="640" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
        <td style="color:#ffffff; font-size:24px; padding-left:10px; text-transform:capitalize;" width="98" height="60px;" valign="bottom">YOUR</td>
        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;font-family:\'Calibri\'" height="60px" colspan="2" valign="bottom">REMINDER</td>
        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;padding-bottom:2px;padding-right:5px; font-family:\'Calibri\'" height="30" colspan="2" valign="bottom"><p align="right" style="font-size:13px; color:#ffffff; margin:2px;font-family:\'Calibri\'">Email us: <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a></p></td>
        </tr>
        
    </tbody></table>
    
    <!-- End header -->
</td>
                </tr>
                <!-- #CONTENT
        		================================================== -->
                <tr><td width="640" height="10" bgcolor="#ffffff"></td></tr>
                <tr>
                <td width="640" bgcolor="#ffffff">
    <table width="640" cellspacing="6" cellpadding="6" border="0">
        <tbody><tr>
            <td width="30"></td>
            <td width="580">
                <!-- #TEXT ONLY
                ================================================== -->
                <table width="580" cellspacing="6" cellpadding="6" border="0">
                    <tbody><tr>
                        <td width="405">
                            <p style="font-size:13px;font-family:\'Calibri\';color:#444444;">Dear [CUSTOMER_TITLE_SURNAME]</p>
                            
							
				                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\';">We are pleased to confirm the site visit on behalf of [BRAND_NAME] will take place on [APPOINTMENT_DATE]. The time window allocated for your appointment is [TIME_SLOT].</p>
								[PRE_VISIT_INFO]
                                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'"></p>
                                

                        </td>
                        <td width="175">
						                        
                          <img width="[BRAND_LOGO_WIDTH]" height="[BRAND_LOGO_HEIGHT]" alt="[BRAND_NAME]"  title="[BRAND_NAME]" src="[BRAND_LOGO]" />
                       
					    </td>
                    </tr>
                   
                </tbody></table>
                <table>
                <tbody>
                 <tr><td width="580" height="5"></td></tr>
                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>
                    <tr><td width="580" height="5"></td></tr>
                    </tbody>
                    </table>
                                        
                <!-- #TEXT WITH ADDRESS
                ================================================== -->
                <table width="580" cellspacing="0" cellpadding="2" border="0">
                    <tbody>
                    <tr>
                        <td width="186" style="padding-bottom:25px;vertical-align:top;"  >
                          <p><table cellspacing="0" cellpadding="5" border="0">
						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>
                          <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'" height="5" width="150" colspan="2" valign="bottom">ADDRESS DETAILS</td>
                          </table></p>
                       </td>
                        <td width="394" style="padding-bottom:25px;vertical-align:top;"  >
                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_TITLE_FIRST_LAST_NAME], <br>[CUSTOMER_ADDRESS]</p>
                        </td>
                    </tr>
					
					
					
                    <tr>
                        <td width="186" style="padding-bottom:25px;vertical-align:top;" >
                          <p> <table cellspacing="0" cellpadding="5" border="0">
                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>
                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">CONTACT DETAILS</td>
                          </table></p>
                       </td>
                        <td width="394" style="padding-bottom:25px;vertical-align:top;" >
                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_PHONE_NO][CUSTOMER_MOBILE_NO][CUSTOMER_EMAIL_PREFIX]<a href="mailto:[CUSTOMER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[CUSTOMER_EMAIL]</a></p>
                        </td>
                    </tr>
					
                    <tr>
                        <td width="186" style="padding-bottom:25px;vertical-align:top;" >
                           <p><table cellspacing="0" cellpadding="5" border="0">
                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>
                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;font-size:13px;font-family:\'Calibri\'">SERVICE CENTRE</td>
                          </table></p>
                       </td>
                        <td width="394" style="padding-bottom:25px;vertical-align:top;" >
                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[SERVICE_CENTRE_ADDRESS].<br>[SERVICE_PROVIDER_TELNO_HTML][SERVICE_CENTRE_EMAIL_PREFIX]<a href="mailto:[SERVICE_PROVIDER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[SERVICE_PROVIDER_EMAIL]</a></p>
                        </td>
                    </tr>
					
                    <tr>
                        <td width="186" style="padding-bottom:25px;vertical-align:top;" >
                          <p><table cellspacing="0" cellpadding="5" border="0">
						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>
                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">PRODUCT DETAILS</td>
                          </table></p>
                       </td>
                        <td width="394" style="padding-bottom:25px;vertical-align:top;" >
                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Job Number: [SC_JOB_NUMBER]&nbsp;&nbsp;&nbsp;&nbsp;Job Booked: [BOOKED_DATE] <br>Manufacturer: [MANUFACTURER_NAME]&nbsp;&nbsp;&nbsp;&nbsp;<br>Product: [UNIT_TYPE_NAME]&nbsp;&nbsp;&nbsp;&nbsp; <br>Model No: [MODEL_NO] <br>Service Type: [SERVICE_TYPE]&nbsp;&nbsp;&nbsp;&nbsp;Location: [PRODUCT_LOCATION]</p>
                        </td>
                    </tr>
			    </tbody></table>
                                        
           
                <table width="580" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                        
                    </tr>
                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>
                </tbody></table>
                                        
                <!-- #TEXT FOOTER
                ================================================== -->
                <table width="580" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                        
                    </tr>
                    <tr><td width="580" height="100">
                    

<p style="font-size:13px;color:#444444;margin-bottom:0px;margin-top:15px;line-height:115%;font-family:\'Calibri\'">Please email <a style="font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a> immediately if there are any discrepancies within the information. Alternatively, if you prefer to talk to a person please call [SERVICE_PROVIDER_NAME] 
on [SERVICE_PROVIDER_TELNO] and quote [SC_JOB_NUMBER]    
<br><br></p>
<p style="font-size:13px;color:#444444;margin-top:0px;line-height:115%;font-family:\'Calibri\'">Assuring you of our best intention at all times.</p>

<p style="font-size:13px;color:#444444;line-height:115%;font-family:\'Calibri\'">Yours Sincerely, <br />[BRAND_NAME] Customer Service Team</p>
                    </td>
                    </tr>
                </tbody></table>
                                                                                
                <!-- #SIDE-BY-SIDE ARTICLE SECTIONS WITH TEXT ONLY
                ================================================== -->
               <!-- <table width="580" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                        <td valign="top">
                          
                        </td>
                       
                    </tr>
                </tbody></table>-->
                                        
                <!-- #ADDITIONAL IMAGE GALLERY ROW WITH NO TITLE
                ================================================== -->
                <!--<table width="580" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                        <td width="180" valign="top">
                          
                        </td>
                        <td width="20"></td>
                        <td width="180" valign="top">
                       
                        </td>
                        <td width="20"></td>
                        <td width="180" valign="top">
                            
                        </td>
                    </tr>
                </tbody></table>-->
            </td>
            <td width="30"></td>
        </tr>
    </tbody></table>
</td>
                </tr>
                <!--<tr><td width="640" height="15" bgcolor="#ffffff"></td></tr>-->
                <!-- #FOOTER
   				 ================================================== -->
                <tr>
                <td width="640">
    <table width="640" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
        <td width="640" colspan="3" rowspan="3" height="20px" style="background:#3576bc;font-family:\'Calibri\'">
 <p style="font-size:13px;color:#ffffff; text-align:center;font-family:\'Calibri\'">If you have any queries contact us on <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a></p>
            </td>
            </tr>

    </tbody></table>
</td>
                </tr>
               
            </tbody></table>
        </td>
	</tr>
</tbody></table>
</body>
</html>'
where EmailCode = 'appointment_reminder';

update email set Message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>[PAGE_TITLE]</title>
</head>

<body>
<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:\'Calibri\';">
	<tbody><tr>
		<td bgcolor="#ffffff" align="center">
        	<table width="640" cellspacing="0" cellpadding="0" border="0" style="margin:0 10px;font-family:\'Calibri\'">
            	<tbody>
                
                <!-- #HEADER
        		================================================== -->
            	<tr>
                	<td width="640"><!-- End top bar table -->
                    </td>
                </tr>
                <tr>
                <td width="640" bgcolor="#52aae0" align="center">
    
    <table width="640" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
        <td style="color:#ffffff; font-size:24px; padding-left:10px; text-transform:capitalize;" width="98" height="60px;" valign="bottom">YOUR</td>
        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;font-family:\'Calibri\'" height="60px" colspan="2" valign="bottom">REMINDER</td>
        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;padding-bottom:2px;padding-right:5px; font-family:\'Calibri\'" height="30" colspan="2" valign="bottom"><p align="right" style="font-size:13px; color:#ffffff; margin:2px;font-family:\'Calibri\'">Email us: <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a></p></td>
        </tr>
        
    </tbody></table>
    
    <!-- End header -->
</td>
                </tr>
                <!-- #CONTENT
        		================================================== -->
                <tr><td width="640" height="10" bgcolor="#ffffff"></td></tr>
                <tr>
                <td width="640" bgcolor="#ffffff">
    <table width="640" cellspacing="6" cellpadding="6" border="0">
        <tbody><tr>
            <td width="30"></td>
            <td width="580">
                <!-- #TEXT ONLY
                ================================================== -->
                <table width="580" cellspacing="6" cellpadding="6" border="0">
                    <tbody><tr>
                        <td width="405">
                            <p style="font-size:13px;font-family:\'Calibri\';color:#444444;">Dear [CUSTOMER_TITLE_SURNAME]</p>
                            
							
				                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\';">We are pleased to confirm the site visit on behalf of [BRAND_NAME] will take place on [APPOINTMENT_DATE]. The time window allocated for your appointment is [TIME_SLOT].</p>
								[PRE_VISIT_INFO]
                                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Please be aware that you can access <a style="font-family:\'Calibri\'" href="[CUSTOMER_WEBSITE_URL]" target="_blank" >[CUSTOMER_WEBSITE_NAME]</a> to view a web page specifically created to allow you to manage your service life cycle on-line and receive real time progress reports.  You will be able to monitor your engineers progress when he is on route to your location and you can also login to <a style="font-family:\'Calibri\'" href="[CUSTOMER_WEBSITE_URL]" target="_blank" >[CUSTOMER_WEBSITE_NAME]</a> if you wish to cancel or re-schedule your appointment.</p>
                                

                        </td>
                        <td width="175">
						                        
                          <img width="[BRAND_LOGO_WIDTH]" height="[BRAND_LOGO_HEIGHT]" alt="[BRAND_NAME]"  title="[BRAND_NAME]" src="[BRAND_LOGO]" />
                       
					    </td>
                    </tr>
                   
                </tbody></table>
                <table>
                <tbody>
                 <tr><td width="580" height="5"></td></tr>
                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>
                    <tr><td width="580" height="5"></td></tr>
                    </tbody>
                    </table>
                                        
                <!-- #TEXT WITH ADDRESS
                ================================================== -->
                <table width="580" cellspacing="0" cellpadding="2" border="0">
                    <tbody>
                    <tr>
                        <td width="186" style="padding-bottom:25px;"  >
                          <table cellspacing="0" cellpadding="5" border="0">
						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>
                          <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'" height="5" width="150" colspan="2" valign="bottom">ADDRESS DETAILS</td>
                          </table>
                       </td>
                        <td width="394" style="padding-bottom:25px;"  >
                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_TITLE_FIRST_LAST_NAME], <br>[CUSTOMER_ADDRESS]</p>
                        </td>
                    </tr>
					
					
					
                    <tr>
                        <td width="186" style="padding-bottom:25px;" >
                           <table cellspacing="0" cellpadding="5" border="0">
                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>
                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">CONTACT DETAILS</td>
                          </table>
                       </td>
                        <td width="394" style="padding-bottom:25px;" >
                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_PHONE_NO][CUSTOMER_MOBILE_NO][CUSTOMER_EMAIL_PREFIX]<a href="mailto:[CUSTOMER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[CUSTOMER_EMAIL]</a></p>
                        </td>
                    </tr>
					
                    <tr>
                        <td width="186" style="padding-bottom:25px;" >
                           <table cellspacing="0" cellpadding="5" border="0">
                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>
                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;font-size:13px;font-family:\'Calibri\'">SERVICE CENTRE</td>
                          </table>
                       </td>
                        <td width="394" style="padding-bottom:25px;" >
                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[SERVICE_CENTRE_ADDRESS].<br>[SERVICE_PROVIDER_TELNO_HTML][SERVICE_CENTRE_EMAIL_PREFIX]<a href="mailto:[SERVICE_PROVIDER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[SERVICE_PROVIDER_EMAIL]</a></p>
                        </td>
                    </tr>
					
                    <tr>
                        <td width="186" style="padding-bottom:25px;" >
                          <table cellspacing="0" cellpadding="5" border="0">
						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>
                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">PRODUCT DETAILS</td>
                          </table>
                       </td>
                        <td width="394" style="padding-bottom:25px;" >
                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Job Number: [SC_JOB_NUMBER]&nbsp;&nbsp;&nbsp;&nbsp;Job Booked: [BOOKED_DATE] <br>Manufacturer: [MANUFACTURER_NAME]&nbsp;&nbsp;&nbsp;&nbsp;<br>Product: [UNIT_TYPE_NAME]&nbsp;&nbsp;&nbsp;&nbsp; <br>Model No: [MODEL_NO] <br>Service Type: [SERVICE_TYPE]&nbsp;&nbsp;&nbsp;&nbsp;Location: [PRODUCT_LOCATION]</p>
                        </td>
                    </tr>
			    </tbody></table>
                                        
           
                <table width="580" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                        
                    </tr>
                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>
                </tbody></table>
                                        
                <!-- #TEXT FOOTER
                ================================================== -->
                <table width="580" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                        
                    </tr>
                    <tr><td width="580" height="100">
                    

<p style="font-size:13px;color:#444444;margin-bottom:0px;line-height:115%;font-family:\'Calibri\'">Please email <a style="font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a> immediately if there are any discrepancies within the information. Alternatively, if you prefer to talk to a person please call [SERVICE_PROVIDER_NAME] 
on [SERVICE_PROVIDER_TELNO] and quote [SC_JOB_NUMBER]    
<br><br></p>
<p style="font-size:13px;color:#444444;margin-top:0px;line-height:115%;font-family:\'Calibri\'">Assuring you of our best intention at all times.</p>

<p style="font-size:13px;color:#444444;line-height:115%;font-family:\'Calibri\'">Yours Sincerely, <br />[BRAND_NAME] Customer Service Team</p>
                    </td>
                    </tr>
                </tbody></table>
                                                                                
                <!-- #SIDE-BY-SIDE ARTICLE SECTIONS WITH TEXT ONLY
                ================================================== -->
               <!-- <table width="580" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                        <td valign="top">
                          
                        </td>
                       
                    </tr>
                </tbody></table>-->
                                        
                <!-- #ADDITIONAL IMAGE GALLERY ROW WITH NO TITLE
                ================================================== -->
                <!--<table width="580" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                        <td width="180" valign="top">
                          
                        </td>
                        <td width="20"></td>
                        <td width="180" valign="top">
                       
                        </td>
                        <td width="20"></td>
                        <td width="180" valign="top">
                            
                        </td>
                    </tr>
                </tbody></table>-->
            </td>
            <td width="30"></td>
        </tr>
    </tbody></table>
</td>
                </tr>
                <!--<tr><td width="640" height="15" bgcolor="#ffffff"></td></tr>-->
                <!-- #FOOTER
   				 ================================================== -->
                <tr>
                <td width="640">
    <table width="640" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
        <td width="640" colspan="3" rowspan="3" height="20px" style="background:#3576bc;font-family:\'Calibri\'">
 <p style="font-size:13px;color:#ffffff; text-align:center;font-family:\'Calibri\'">If you have any queries contact us on <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a></p>
            </td>
            </tr>

    </tbody></table>
</td>
                </tr>
               
            </tbody></table>
        </td>
	</tr>
</tbody></table>
</body>
</html>'
where EmailCode = 'appointment_reminder_crm';

update email set Message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>[PAGE_TITLE]</title>
</head>

<body>
<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:\'Calibri\';">
	<tbody><tr>
		<td bgcolor="#ffffff" align="center">
        	<table width="640" cellspacing="0" cellpadding="0" border="0" style="margin:0 10px;font-family:\'Calibri\'">
            	<tbody>
                
                <!-- #HEADER
        		================================================== -->
            	<tr>
                	<td width="640"><!-- End top bar table -->
                    </td>
                </tr>
                <tr>
                <td width="640" bgcolor="#52aae0" align="center">
    
    <table width="640" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
        <td style="color:#ffffff; font-size:24px; padding-left:10px; text-transform:capitalize;" width="98" height="60px;" valign="bottom">YOUR</td>
        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;font-family:\'Calibri\'" height="60px" colspan="2" valign="bottom">REMINDER</td>
        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;padding-bottom:2px;padding-right:5px; font-family:\'Calibri\'" height="30" colspan="2" valign="bottom"><p align="right" style="font-size:13px; color:#ffffff; margin:2px;font-family:\'Calibri\'">Email us: <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a></p></td>
        </tr>
        
    </tbody></table>
    
    <!-- End header -->
</td>
                </tr>
                <!-- #CONTENT
        		================================================== -->
                <tr><td width="640" height="10" bgcolor="#ffffff"></td></tr>
                <tr>
                <td width="640" bgcolor="#ffffff">
    <table width="640" cellspacing="6" cellpadding="6" border="0">
        <tbody><tr>
            <td width="30"></td>
            <td width="580">
                <!-- #TEXT ONLY
                ================================================== -->
                <table width="580" cellspacing="6" cellpadding="6" border="0">
                    <tbody><tr>
                        <td width="405">
                            <p style="font-size:13px;font-family:\'Calibri\';color:#444444;">Dear [CUSTOMER_TITLE_SURNAME]</p>
                            
							
				                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\';">We are pleased to confirm the site visit on behalf of [BRAND_NAME] will take place on [APPOINTMENT_DATE]. The time window allocated for your appointment is [TIME_SLOT].</p>
								[PRE_VISIT_INFO]
                                <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Although, we will communicate regularly confirming each stage of your service lifecycle you can check progress anytime at <a style="font-family:\'Calibri\'" href="[CUSTOMER_WEBSITE_URL]" target="_blank" >[CUSTOMER_WEBSITE_NAME]</a></p>
                                

                        </td>
                        <td width="175">
						                        
                          <img width="[BRAND_LOGO_WIDTH]" height="[BRAND_LOGO_HEIGHT]" alt="[BRAND_NAME]"  title="[BRAND_NAME]" src="[BRAND_LOGO]" />
                       
					    </td>
                    </tr>
                   
                </tbody></table>
                <table>
                <tbody>
                 <tr><td width="580" height="5"></td></tr>
                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>
                    <tr><td width="580" height="5"></td></tr>
                    </tbody>
                    </table>
                                        
                <!-- #TEXT WITH ADDRESS
                ================================================== -->
                <table width="580" cellspacing="0" cellpadding="2" border="0">
                    <tbody>
                    <tr>
                        <td width="186" style="padding-bottom:25px;vertical-align:top;"  >
                          <p><table cellspacing="0" cellpadding="5" border="0">
						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>
                          <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'" height="5" width="150" colspan="2" valign="bottom">ADDRESS DETAILS</td>
                          </table></p>
                       </td>
                        <td width="394" style="padding-bottom:25px;vertical-align:top;"  >
                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_TITLE_FIRST_LAST_NAME], <br>[CUSTOMER_ADDRESS]</p>
                        </td>
                    </tr>
					
					
					
                    <tr>
                        <td width="186" style="padding-bottom:25px;vertical-align:top;" >
                           <p><table cellspacing="0" cellpadding="5" border="0">
                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>
                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">CONTACT DETAILS</td>
                          </table></p>
                       </td>
                        <td width="394" style="padding-bottom:25px;vertical-align:top;" >
                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[CUSTOMER_PHONE_NO][CUSTOMER_MOBILE_NO][CUSTOMER_EMAIL_PREFIX]<a href="mailto:[CUSTOMER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[CUSTOMER_EMAIL]</a></p>
                        </td>
                    </tr>
					
                    <tr>
                        <td width="186" style="padding-bottom:25px;vertical-align:top;" >
                           <p><table cellspacing="0" cellpadding="5" border="0">
                          <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>
                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;font-size:13px;font-family:\'Calibri\'">SERVICE CENTRE</td>
                          </table></p>
                       </td>
                        <td width="394" style="padding-bottom:25px;vertical-align:top;" >
                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">[SERVICE_CENTRE_ADDRESS].<br>[SERVICE_PROVIDER_TELNO_HTML][SERVICE_CENTRE_EMAIL_PREFIX]<a href="mailto:[SERVICE_PROVIDER_EMAIL]" style="font-family:\'Calibri\'" target="_balnk" >[SERVICE_PROVIDER_EMAIL]</a></p>
                        </td>
                    </tr>
					
                    <tr>
                        <td width="186" style="padding-bottom:25px;vertical-align:top;" >
                          <p><table cellspacing="0" cellpadding="5" border="0">
						  <td style="background:#52aae0;color:#ffffff; font-size:11px;" width="31" height="5px;" valign="bottom">YOUR</td>
                          <td width="150" height="5" colspan="2" valign="bottom" style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:13px;font-family:\'Calibri\'">PRODUCT DETAILS</td>
                          </table></p>
                       </td>
                        <td width="394" style="padding-bottom:25px;vertical-align:top;" >
                        <p style="font-size:13px; color:#444444;line-height:115%;font-family:\'Calibri\'">Job Number: [SC_JOB_NUMBER]&nbsp;&nbsp;&nbsp;&nbsp;Job Booked: [BOOKED_DATE] <br>Manufacturer: [MANUFACTURER_NAME]&nbsp;&nbsp;&nbsp;&nbsp;<br>Product: [UNIT_TYPE_NAME]&nbsp;&nbsp;&nbsp;&nbsp; <br>Model No: [MODEL_NO] <br>Service Type: [SERVICE_TYPE]&nbsp;&nbsp;&nbsp;&nbsp;Location: [PRODUCT_LOCATION]</p>
                        </td>
                    </tr>
			    </tbody></table>
                                        
           
                <table width="580" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                        
                    </tr>
                    <tr><td width="580" height="10" style="background:#3576bc;font-family:\'Calibri\'"></td></tr>
                </tbody></table>
                                        
                <!-- #TEXT FOOTER
                ================================================== -->
                <table width="580" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                        
                    </tr>
                    <tr><td width="580" height="100">
                    

<p style="font-size:13px;color:#444444;margin-bottom:0px;margin-top:15px;line-height:115%;font-family:\'Calibri\'">Please email <a style="font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a> immediately if there are any discrepancies within the information. Alternatively, if you prefer to talk to a person please call [SERVICE_PROVIDER_NAME] 
on [SERVICE_PROVIDER_TELNO] and quote [SC_JOB_NUMBER]    
<br><br></p>
<p style="font-size:13px;color:#444444;margin-top:0px;line-height:115%;font-family:\'Calibri\'">Assuring you of our best intention at all times.</p>

<p style="font-size:13px;color:#444444;line-height:115%;font-family:\'Calibri\'">Yours Sincerely, <br/>[BRAND_NAME] Customer Service Team</p>
                    </td>
                    </tr>
                </tbody></table>
                                                                                
                <!-- #SIDE-BY-SIDE ARTICLE SECTIONS WITH TEXT ONLY
                ================================================== -->
               <!-- <table width="580" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                        <td valign="top">
                          
                        </td>
                       
                    </tr>
                </tbody></table>-->
                                        
                <!-- #ADDITIONAL IMAGE GALLERY ROW WITH NO TITLE
                ================================================== -->
                <!--<table width="580" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                        <td width="180" valign="top">
                          
                        </td>
                        <td width="20"></td>
                        <td width="180" valign="top">
                       
                        </td>
                        <td width="20"></td>
                        <td width="180" valign="top">
                            
                        </td>
                    </tr>
                </tbody></table>-->
            </td>
            <td width="30"></td>
        </tr>
    </tbody></table>
</td>
                </tr>
                <!--<tr><td width="640" height="15" bgcolor="#ffffff"></td></tr>-->
                <!-- #FOOTER
   				 ================================================== -->
                <tr>
                <td width="640">
    <table width="640" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
        <td width="640" colspan="3" rowspan="3" height="20px" style="background:#3576bc;font-family:\'Calibri\'">
 <p style="font-size:13px;color:#ffffff; text-align:center;font-family:\'Calibri\'">If you have any queries contact us on <a style="color:#ffffff;font-family:\'Calibri\'" href="mailto:[SERVICE_PROVIDER_EMAIL]" >[SERVICE_PROVIDER_EMAIL]</a></p>
            </td>
            </tr>

    </tbody></table>
</td>
                </tr>
               
            </tbody></table>
        </td>
	</tr>
</tbody></table>
</body>
</html>'
where EmailCode = 'appointment_reminder_smart_service_tracker';

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.301');



